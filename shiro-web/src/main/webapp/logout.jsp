<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="include.jsp" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="<c:url value="/style.css"/>"/>
</head>
<body onload="document.getElementById('logout_form').submit();return false;">

<h2>如果没有重定向, 点击退出按钮进行退出.</h2>

<form id="logout_form" name="logout_form" action="<c:url value="/logout"/>" method="post">
    <input type="submit" value="退出登录">
</form>

</body>
</html>
