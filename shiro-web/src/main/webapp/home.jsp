<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="include.jsp" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="<c:url value="/style.css"/>"/>
    <title>快速开始示例</title>
</head>
<body>

<h1>快速开始示例</h1>

<p>你好 <shiro:guest>访客</shiro:guest><shiro:user><shiro:principal/></shiro:user>!
    ( <shiro:user>  <a href="<c:url value="/logout"/>" onclick="document.getElementById('logout_form').submit();return false;">退出</a> </shiro:user>
    <shiro:guest><a href="<c:url value="/login.jsp"/>">登录</a> (提供的示例账户)</shiro:guest> )
</p>

<p>欢迎来到Apache Shiro Quickstart示例应用程序。
    此页面表示web应用程序的主页。</p>

<shiro:user><p>访问你的 <a href="<c:url value="/account"/>">账户页面</a>.</p></shiro:user>
<shiro:guest><p>如果你只想访问 <a href="<c:url value="/account"/>">用户页面</a>,
    你需要先登录.</p></shiro:guest>

<h2>Roles</h2>

<p>为了展示一些标签，下面是您拥有和没有的角色。在不同的用户下注销和重新登录
    帐户可以看到不同的角色。</p>

<h3>你的角色</h3>
<hr/>
<p>
    <shiro:hasRole name="admin">admin<br/></shiro:hasRole>
    <shiro:hasRole name="president">president<br/></shiro:hasRole>
    <shiro:hasRole name="darklord">darklord<br/></shiro:hasRole>
    <shiro:hasRole name="goodguy">goodguy<br/></shiro:hasRole>
    <shiro:hasRole name="schwartz">schwartz<br/></shiro:hasRole>
</p>

<h3>你没有的角色</h3>
<hr/>

<p>
    <shiro:lacksRole name="admin">admin<br/></shiro:lacksRole>
    <shiro:lacksRole name="president">president<br/></shiro:lacksRole>
    <shiro:lacksRole name="darklord">darklord<br/></shiro:lacksRole>
    <shiro:lacksRole name="goodguy">goodguy<br/></shiro:lacksRole>
    <shiro:lacksRole name="schwartz">schwartz<br/></shiro:lacksRole>
</p>

<form id="logout_form" action="<c:url value="/logout"/>" method="post"></form>
</body>
</html>
