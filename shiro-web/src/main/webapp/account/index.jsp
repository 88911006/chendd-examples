<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../include.jsp" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="<c:url value="/style.css"/>"/>
</head>
<body>

<h2>Users only</h2>

<p>您当前正在登录.</p>

<p><a href="<c:url value="/home.jsp"/>">返回到主页.</a></p>

<p><a href="<c:url value="/logout"/>" onclick="document.getElementById('logout_form').submit();return false;">退出.</a></p>
<form id="logout_form" action="<c:url value="/logout"/>" method="post"></form>
</body>
</html>