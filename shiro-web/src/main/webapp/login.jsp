<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="include.jsp" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="<c:url value="/style.css"/>"/>
</head>
<body>

<h2>请登录</h2>

<shiro:guest>
    <p>下面列表是登录相关的用户名与密码，他们表示不同的角色信息，也有保护记住我的复选框.</p>


    <style type="text/css">
        table.sample {
            border-width: 1px;
            border-style: outset;
            border-color: blue;
            border-collapse: separate;
            background-color: rgb(255, 255, 240);
        }

        table.sample th {
            border-width: 1px;
            padding: 5px;
            border-style: none;
            border-color: blue;
            background-color: rgb(255, 255, 240);
        }

        table.sample td {
            border-width: 1px;
            padding: 5px;
            border-style: none;
            border-color: blue;
            background-color: rgb(255, 255, 240);
        }
    </style>


    <table class="sample">
        <thead>
        <tr>
            <th>用户名</th>
            <th>密码</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>root</td>
            <td>secret</td>
        </tr>
        <tr>
            <td>presidentskroob</td>
            <td>12345</td>
        </tr>
        <tr>
            <td>darkhelmet</td>
            <td>ludicrousspeed</td>
        </tr>
        <tr>
            <td>lonestarr</td>
            <td>vespa</td>
        </tr>
        </tbody>
    </table>
    <br/><br/>
</shiro:guest>

<form name="loginform" action="" method="post">
    <table align="left" class="sample" border="1" cellspacing="0" cellpadding="1" style="border-collapse: collapse">
        <tr>
            <td>用户名:</td>
            <td><input type="text" name="username" maxlength="30"></td>
        </tr>
        <tr>
            <td>密码:</td>
            <td><input type="password" name="password" maxlength="30"></td>
        </tr>
        <tr>
            <td colspan="2" align="left"><input type="checkbox" name="rememberMe"><font size="2">记住我</font></td>
        </tr>
        <tr>
            <td colspan="2" align="right"><input type="submit" name="submit" value="登录"></td>
        </tr>
    </table>
</form>

</body>
</html>
