package cn.chendd.redis.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * SpringBean组件获取工具类
 *
 * @author chendd
 * @date 2023/5/19 15:11
 */
@Component
public class SpringBeanFactory implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringBeanFactory.applicationContext = applicationContext;
    }

    public static <T> T getBean(String beanName , Class<T> clazz){
        return applicationContext.getBean(beanName , clazz);
    }

    public static <T> Map<String , T> getBeans(Class<T> clazz){
        return applicationContext.getBeansOfType(clazz);
    }

}
