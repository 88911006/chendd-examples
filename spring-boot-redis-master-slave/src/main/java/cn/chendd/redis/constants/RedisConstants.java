package cn.chendd.redis.constants;

/**
 * 常量类定义
 *
 * @author chendd
 * @date 2023/5/19 15:13
 */
public interface RedisConstants {

    /**
     * Redis Lettuce 写工厂组件名称
     */
    String WRITE_LETTUCE_CONNECTION_FACTORY = "writeLettuceConnectionFactory";

    /**
     * Redis Lettuce 读工厂组件名称
     */
    String READ1_LETTUCE_CONNECTION_FACTORY = "read1LettuceConnectionFactory";

    /**
     * Redis Lettuce 读工厂组件名称
     */
    String READ2_LETTUCE_CONNECTION_FACTORY = "read2LettuceConnectionFactory";

    /**
     * 写 StringRedisTemplate
     */
    String WRITE_STRING_REDIS_TEMPLATE = "writeStringRedisTemplate";

    /**
     * 写 writeRedisTemplate
     */
    String WRITE_REDIS_TEMPLATE = "writeRedisTemplate";

    /**
     * 读 read1StringRedisTemplate
     */
    String READ1_STRING_REDIS_TEMPLATE = "read1StringRedisTemplate";

    /**
     * 读 read1RedisTemplate
     */
    String READ1_REDIS_TEMPLATE = "read1RedisTemplate";

    /**
     * 读 read2StringRedisTemplate
     */
    String READ2_STRING_REDIS_TEMPLATE = "read2StringRedisTemplate";

    /**
     * 读 read2RedisTemplate
     */
    String READ2_REDIS_TEMPLATE = "read2RedisTemplate";

}
