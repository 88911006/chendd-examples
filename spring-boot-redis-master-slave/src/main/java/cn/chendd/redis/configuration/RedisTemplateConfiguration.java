package cn.chendd.redis.configuration;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import static cn.chendd.redis.constants.RedisConstants.*;

/**
 * RedisTemplate 配置
 *
 * @author chendd
 * @date 2023/5/19 9:42
 */
@Configuration
@DependsOn("redisConfiguration")
public class RedisTemplateConfiguration {

    @Bean(WRITE_STRING_REDIS_TEMPLATE)
    public StringRedisTemplate writeStringRedisTemplate(@Autowired @Qualifier(WRITE_LETTUCE_CONNECTION_FACTORY) RedisConnectionFactory factory) {
        return this.createStringRedisTemplate(factory);
    }

    @Bean(WRITE_REDIS_TEMPLATE)
    public RedisTemplate<String, Object> writeRedisTemplate(@Autowired @Qualifier(WRITE_LETTUCE_CONNECTION_FACTORY) RedisConnectionFactory factory) {
        return this.createRedisTemplate(factory);
    }

    @Bean(READ1_STRING_REDIS_TEMPLATE)
    public StringRedisTemplate read1StringRedisTemplate(@Autowired @Qualifier(READ1_LETTUCE_CONNECTION_FACTORY) RedisConnectionFactory factory) {
        return this.createStringRedisTemplate(factory);
    }

    @Bean(READ1_REDIS_TEMPLATE)
    public RedisTemplate<String, Object> read1RedisTemplate(@Autowired @Qualifier(READ1_LETTUCE_CONNECTION_FACTORY) RedisConnectionFactory factory) {
        return this.createRedisTemplate(factory);
    }

    @Bean(READ2_STRING_REDIS_TEMPLATE)
    public StringRedisTemplate read2StringRedisTemplate(@Autowired @Qualifier(READ2_LETTUCE_CONNECTION_FACTORY) RedisConnectionFactory factory) {
        return this.createStringRedisTemplate(factory);
    }

    @Bean(READ2_REDIS_TEMPLATE)
    public RedisTemplate<String, Object> read2RedisTemplate(@Autowired @Qualifier(READ2_LETTUCE_CONNECTION_FACTORY) RedisConnectionFactory factory) {
        return this.createRedisTemplate(factory);
    }

    private <T> RedisTemplate<String, Object> createRedisTemplate(RedisConnectionFactory factory) {

        GenericFastJsonRedisSerializer fastjson = new GenericFastJsonRedisSerializer();

        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());

        template.setValueSerializer(fastjson);
        template.setHashValueSerializer(fastjson);
        template.afterPropertiesSet();
        return template;
    }

    private StringRedisTemplate createStringRedisTemplate(RedisConnectionFactory factory) {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(factory);
        return stringRedisTemplate;
    }

}
