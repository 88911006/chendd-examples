package cn.chendd.redis;

import cn.chendd.redis.utils.RedisUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.*;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Redis写入数据类型测试【用write环境写，用read环境读】
 *
 * @author chendd
 * @date 2023/5/20 12:07
 */
public class RedisWriteDataTypeTest extends BaseTest {

    private RedisTemplate<String , Object> writeRedisTemplate;
    private RedisTemplate<String , Object> readRedisTemplate;

    @Before
    public void init() {
        this.writeRedisTemplate = RedisUtils.getWriteRedisTemplate();
        this.readRedisTemplate = RedisUtils.getReadRedisTemplate();
    }

    /**
     * 字符串的存取
     */
    @Test
    public void string() {
        System.out.println("两种方式存取，应使用StringRedisTemplate");
        this.writeRedisTemplate.boundValueOps("name").set("陈冬冬-" + getDatetime());
        String name1 = (String) this.readRedisTemplate.boundValueOps("name").get();
        System.out.println(MessageFormat.format("【boundValueOps】 name = {0}" , name1));
        String name2 = (String) this.readRedisTemplate.opsForValue().get("name");
        System.out.println(MessageFormat.format("【opsForValue】 name = {0}" , name2));
        System.out.println("删除key：" + this.writeRedisTemplate.delete("name"));
        System.out.println("删除后再获取：" + this.readRedisTemplate.boundValueOps("name").get());
        System.out.println("删除后再获取：" + this.readRedisTemplate.opsForValue().get("name"));
    }

    /**
     * List集合的增删改查
     */
    @Test
    public void list() {
        BoundListOperations<String, Object> write = this.writeRedisTemplate.boundListOps("list");
        //按顺序追加元素
        write.rightPush("Java");
        write.rightPush("C++");
        write.rightPush("C#");
        //设置第 0 个元素的值
        write.set(0 , "Java chendd");
        //追加一个数组
        write.rightPushAll(ArrayUtils.toArray("a" , "b" , "c" , "d" , "C++" , null , "" , "SQL"));

        BoundListOperations<String, Object> read = this.readRedisTemplate.boundListOps("list");
        long size = read.size();
        //获取数据，根据范围全量获取
        System.out.println("根据范围获取数据：" + read.range(0 , size));
        //按照下标循环获取
        for (int i = 0 ; i < size ; i++) {
            System.out.println(MessageFormat.format("循环获取数据 index = {0} , value = {1}" , i , read.index(i)));
        }
        //删除所有值等于C++的数据
        write.remove(0 , "C++");
        System.out.println("获取删除后的数据：" + read.range(0 , size));
    }

    @Test
    public void set() {
        //创建一个set，赋值有包含null和重复值
        BoundSetOperations<String, Object> write = this.writeRedisTemplate.boundSetOps("set");
        write.add("c" , "h" , "e" , "n" , "d" , "d" , null , "" , "666");
        //删除值为 666
        write.remove("666");
        //读取集合
        BoundSetOperations<String, Object> read = this.readRedisTemplate.boundSetOps("set");
        long size = read.size();
        Set<Object> values = read.members();
        System.out.println("数据条数：" + size + "，数据：" + values);
    }

    @Test
    public void zset() {
        //构造zset并存储数据
        BoundZSetOperations<String, Object> write = this.writeRedisTemplate.boundZSetOps("zset");
        write.add("a" , 1);
        write.add("b" , 1);
        write.add("c" , 2);
        write.add("d" , 3);
        write.add("" , 4);
        write.add(null , 5);
        write.add("666" , 6);
        //删除不存在的数据
        write.remove("temp");
        //根据范围删除数据
        write.removeRange(2 , 3);
        //获取zset数据
        BoundZSetOperations<String, Object> read = this.readRedisTemplate.boundZSetOps("zset");
        System.out.println("根据不存在的值获取序号：" + read.score("temp"));
        System.out.println("根据存在的值获取序号：" + read.score("666"));
        long size = read.size();
        Set<Object> values = read.range(0, size);
        System.out.println("数据：" + values);
    }

    @Test
    public void hash() {
        //构造hash结构并存储多种数据类型的数据
        BoundHashOperations<String, String, Object> write = this.writeRedisTemplate.boundHashOps("hash");
        write.put("a" , "chendd");
        write.put("b" , "男");
        write.put("c" , true);
        write.put("d" , 35987.55);
        write.put("f" , null);
        write.put("g" , "");
        write.put("h" , "temp");
        System.out.println("keys = " + write.keys());
        System.out.println("values = " + write.values());
        System.out.println("删除不存在的key：" + write.delete("t"));
        System.out.println("删除存在的key：" + write.delete("h"));
        //或者hash数据
        BoundHashOperations<String, Object, Object> read = this.readRedisTemplate.boundHashOps("hash");
        System.out.println("数据：" + read.entries());
    }

    /**
     * 高德地图坐标提取：https://lbs.amap.com/tools/picker
     */
    @Test
    public void geo() {
        //地图经纬度坐标
        BoundGeoOperations<String, Object> write = this.writeRedisTemplate.boundGeoOps("geo");
        write.add(new Point(116.40,39.91) , "北京");
        write.add(new RedisGeoCommands.GeoLocation<>("老河口" , new Point(111.68,32.36)));
        System.out.println(write.position("北京"));
        System.out.println(write.hash("老河口"));
    }

    @Test
    public void stream() {
        BoundStreamOperations<String, String, Object> write = this.writeRedisTemplate.boundStreamOps("stream");
        Map<String , Object> data = new HashMap<>();
        data.put("aaa" , 111);
        data.put("bbb" , "222");
        write.add(data);
        //看起来挺复杂，就先这么着吧，今后有缘再深挖。。。
    }

    private String getDatetime() {
        return DateFormatUtils.format(new Date() , DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.getPattern());
    }

}
