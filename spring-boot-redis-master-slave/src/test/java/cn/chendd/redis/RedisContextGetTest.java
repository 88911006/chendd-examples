package cn.chendd.redis;

import cn.chendd.redis.utils.RedisUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.text.MessageFormat;

/**
 * 使用工具类获取的方式来测试redis的实例，关注各个实例输出的服务器参数
 *
 * @author chendd
 * @date 2023/5/19 21:30
 */
public class RedisContextGetTest extends BaseTest {

    private RedisTemplate readRedisTemplate;
    private RedisTemplate writeRedisTemplate;

    private StringRedisTemplate writeStringRedisTemplate;
    private StringRedisTemplate readStringRedisTemplate;

    @Before
    public void init() {
        this.writeRedisTemplate = RedisUtils.getWriteRedisTemplate();
        this.writeStringRedisTemplate = RedisUtils.getWriteStringRedisTemplate();

        this.readRedisTemplate = RedisUtils.getReadRedisTemplate();
        this.readStringRedisTemplate = RedisUtils.getReadStringRedisTemplate();
    }

    @Test
    public void contextRedisTemplate() {
        LettuceConnectionFactory write = (LettuceConnectionFactory) this.writeRedisTemplate.getConnectionFactory();
        assert write != null;
        System.out.println(MessageFormat.format("write host = {0} , port = {1}", write.getHostName(), write.getPort()));
        LettuceConnectionFactory read = (LettuceConnectionFactory) this.readRedisTemplate.getConnectionFactory();
        assert read != null;
        System.out.println(MessageFormat.format("read1 host = {0} , port = {1}", read.getHostName(), read.getPort()));
    }

    @Test
    public void contextStringRedisTemplate() {
        LettuceConnectionFactory write = (LettuceConnectionFactory) this.writeStringRedisTemplate.getConnectionFactory();
        assert write != null;
        System.out.println(MessageFormat.format("write host = {0} , port = {1}", write.getHostName(), write.getPort()));
        LettuceConnectionFactory read = (LettuceConnectionFactory) this.readStringRedisTemplate.getConnectionFactory();
        assert read != null;
        System.out.println(MessageFormat.format("read1 host = {0} , port = {1}", read.getHostName(), read.getPort()));
    }

}
