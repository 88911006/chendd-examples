package cn.chendd.redis;

import cn.chendd.redis.utils.RedisUtils;
import org.junit.Test;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.text.MessageFormat;

/**
 * Redis读负载均衡测试
 *
 * @author chendd
 * @date 2023/5/19 16:39
 */
public class ReadisReadLoadBalanceTest extends BaseTest {

    /**
     * 循环10次，观察每次获取到的参数信息
     */
    @Test
    public void lbRedisTemplate() {
        for (int i = 1 ; i <= 10 ; i++) {
            RedisTemplate redisTemplate = RedisUtils.getReadRedisTemplate();
            LettuceConnectionFactory read = (LettuceConnectionFactory) redisTemplate.getConnectionFactory();
            assert read != null;
            System.out.println(MessageFormat.format("read redisTemplate {0} host = {1} , port = {2}" , i , read.getHostName(), read.getPort()));
        }
    }

    /**
     * 循环10次，观察每次获取到的参数信息
     */
    @Test
    public void lbStringRedisTemplate() {
        for (int i = 1 ; i <= 10 ; i++) {
            StringRedisTemplate stringRedisTemplate = RedisUtils.getReadStringRedisTemplate();
            LettuceConnectionFactory read = (LettuceConnectionFactory) stringRedisTemplate.getConnectionFactory();
            assert read != null;
            System.out.println(MessageFormat.format("read stringRedisTemplate {0} host = {1} , port = {2}" , i , read.getHostName(), read.getPort()));
        }

    }

}
