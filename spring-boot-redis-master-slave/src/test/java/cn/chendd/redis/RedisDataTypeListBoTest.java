package cn.chendd.redis;

import cn.chendd.redis.utils.RedisUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.awt.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * List对象类型存储测试【读写分离】
 *
 * @author chendd
 * @date 2023/5/21 10:15
 */
public class RedisDataTypeListBoTest extends BaseTest {

    private RedisTemplate<String , User> writeRedisTemplate;
    private RedisTemplate<String , User> readRedisTemplate;

    @Before
    public void init() {
        this.writeRedisTemplate = RedisUtils.getWriteRedisTemplate();
        this.readRedisTemplate = RedisUtils.getReadRedisTemplate();
    }

    @Test
    public void listBo() {
        BoundListOperations<String, User> write = this.writeRedisTemplate.boundListOps("listBo");
        {
            User user = new User();
            user.setId(Long.MAX_VALUE - Integer.MAX_VALUE);
            user.setName("陈冬冬");
            user.setAge(Short.MAX_VALUE);
            user.setPi(Math.PI);
            user.setAmount(BigDecimal.TEN);
            user.setFlag(Boolean.TRUE);
            user.setSex(SexEnum.Boy);
            List<Point> points = new ArrayList<>();
            points.add(new Point(1 , 2));
            points.add(new Point(11 , 22));
            user.setPoints(points);
            write.rightPush(user);
        }
        {
            User user = new User();
            user.setId(Long.MIN_VALUE + Integer.MAX_VALUE);
            user.setName("redis");
            user.setAge(Short.MIN_VALUE);
            user.setPi(Math.PI);
            user.setAmount(new BigDecimal("123456.789"));
            user.setFlag(Boolean.FALSE);
            user.setSex(SexEnum.Gril);
            List<Point> points = new ArrayList<>();
            points.add(new Point(111 , 222));
            points.add(new Point(333 , 444));
            user.setPoints(points);
            write.rightPush(user);
        }
        //从读服务中获取数据
        BoundListOperations<String, User> read = this.readRedisTemplate.boundListOps("listBo");
        Long size = read.size();
        List<User> lists = read.range(0, size);
        for (User user : lists) {
            System.out.println(user);
        }
    }

    /**
     * 定义User对象，使用多种数据类型
     */
    public static class User {

        private Long id;

        private String name;

        private Short age;

        private Double pi;

        private BigDecimal amount;

        private Boolean flag;

        private SexEnum sex;

        private List<Point> points;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Short getAge() {
            return age;
        }

        public void setAge(Short age) {
            this.age = age;
        }

        public Double getPi() {
            return pi;
        }

        public void setPi(Double pi) {
            this.pi = pi;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public Boolean getFlag() {
            return flag;
        }

        public void setFlag(Boolean flag) {
            this.flag = flag;
        }

        public SexEnum getSex() {
            return sex;
        }

        public void setSex(SexEnum sex) {
            this.sex = sex;
        }

        public List<Point> getPoints() {
            return points;
        }

        public void setPoints(List<Point> points) {
            this.points = points;
        }
    }

    public enum SexEnum {

        Boy("男"),

        Gril("女"),
        ;

        private String text;

        SexEnum(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

}
