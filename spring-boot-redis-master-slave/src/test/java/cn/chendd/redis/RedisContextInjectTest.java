package cn.chendd.redis;

import cn.chendd.redis.constants.RedisConstants;
import org.junit.Test;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.text.MessageFormat;

/**
 * 使用@Resource注入的方式来测试redis的6个实现类，关注各个实例输出的服务器参数
 *
 * @author chendd
 * @date 2023/5/16 21:40
 */
public class RedisContextInjectTest extends BaseTest {

    @Resource(name = RedisConstants.READ1_REDIS_TEMPLATE)
    private RedisTemplate read1RedisTemplate;
    @Resource(name = RedisConstants.READ2_REDIS_TEMPLATE)
    private RedisTemplate read2RedisTemplate;
    @Resource(name = RedisConstants.WRITE_REDIS_TEMPLATE)
    private RedisTemplate writeRedisTemplate;

    @Resource(name = RedisConstants.READ1_STRING_REDIS_TEMPLATE)
    private StringRedisTemplate read1StringRedisTemplate;
    @Resource(name = RedisConstants.READ2_STRING_REDIS_TEMPLATE)
    private StringRedisTemplate read2StringRedisTemplate;
    @Resource(name = RedisConstants.WRITE_STRING_REDIS_TEMPLATE)
    private StringRedisTemplate writeStringRedisTemplate;
    
    @Test
    public void contextRedisTemplate() {
        LettuceConnectionFactory write = (LettuceConnectionFactory) this.writeRedisTemplate.getConnectionFactory();
        assert write != null;
        System.out.println(MessageFormat.format("write host = {0} , port = {1}", write.getHostName(), write.getPort()));
        LettuceConnectionFactory read1 = (LettuceConnectionFactory) this.read1RedisTemplate.getConnectionFactory();
        assert read1 != null;
        System.out.println(MessageFormat.format("read1 host = {0} , port = {1}", read1.getHostName(), read1.getPort()));
        LettuceConnectionFactory read2 = (LettuceConnectionFactory) this.read2RedisTemplate.getConnectionFactory();
        assert read2 != null;
        System.out.println(MessageFormat.format("read2 host = {0} , port = {1}", read2.getHostName(), read2.getPort()));
    }

    @Test
    public void contextStringRedisTemplate() {
        LettuceConnectionFactory write = (LettuceConnectionFactory) this.writeStringRedisTemplate.getConnectionFactory();
        assert write != null;
        System.out.println(MessageFormat.format("write host = {0} , port = {1}", write.getHostName(), write.getPort()));
        LettuceConnectionFactory read1 = (LettuceConnectionFactory) this.read1StringRedisTemplate.getConnectionFactory();
        assert read1 != null;
        System.out.println(MessageFormat.format("read1 host = {0} , port = {1}", read1.getHostName(), read1.getPort()));
        LettuceConnectionFactory read2 = (LettuceConnectionFactory) this.read2StringRedisTemplate.getConnectionFactory();
        assert read2 != null;
        System.out.println(MessageFormat.format("read2 host = {0} , port = {1}", read2.getHostName(), read2.getPort()));
    }

}
