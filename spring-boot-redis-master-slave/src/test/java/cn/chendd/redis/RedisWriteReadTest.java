package cn.chendd.redis;

import cn.chendd.redis.constants.RedisConstants;
import cn.chendd.redis.utils.RedisUtils;
import org.junit.Test;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.UUID;

/**
 * Redis读写分离测试
 * 知识点
 * （1）；
 *
 * @author chendd
 * @date 2023/5/20 9:36
 */
public class RedisWriteReadTest extends BaseTest {

    /**
     * 写 客户端
     */
    @Resource(name = RedisConstants.WRITE_STRING_REDIS_TEMPLATE)
    private StringRedisTemplate writeStringRedisTemplate;
    /**
     * 读 1 客户端
     */
    @Resource(name = RedisConstants.READ1_STRING_REDIS_TEMPLATE)
    private StringRedisTemplate read1StringRedisTemplate;
    /**
     * 读 2 客户端
     */
    @Resource(name = RedisConstants.READ2_STRING_REDIS_TEMPLATE)
    private StringRedisTemplate read2StringRedisTemplate;

    @Test
    public void writeAndReadInject() {
        for (int i= 0 ; i <= 20 ; i++) {
            String value = "chendd_" + UUID.randomUUID().toString().substring(22);
            String name = "name_" + value;
            this.writeStringRedisTemplate.boundValueOps(name).set(value);
            String read1 = this.read1StringRedisTemplate.boundValueOps(name).get();
            String read2 = this.read2StringRedisTemplate.boundValueOps(name).get();
            System.out.println(MessageFormat.format("写入值 = {0}， 读1客户端 = {1} ，读2客户端 = {2}", value, read1, read2));
        }
    }

    @Test
    public void writeAndReadGet() {
        for (int i= 0 ; i <= 20 ; i++) {
            String value = "chendd_" + UUID.randomUUID().toString().substring(22);;
            String name = "name_" + value;
            RedisUtils.getWriteStringRedisTemplate().boundValueOps(name).set(value);
            String read1 = RedisUtils.getReadStringRedisTemplate().boundValueOps(name).get();
            String read2 = RedisUtils.getReadStringRedisTemplate().boundValueOps(name).get();
            System.out.println(MessageFormat.format("写入值 = {0}， 读1客户端 = {1} ，读2客户端 = {2}", value, read1, read2));
        }
    }

}
