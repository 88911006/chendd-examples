package cn.chendd.redis;

import cn.chendd.redis.utils.RedisUtils;
import org.junit.Test;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.types.RedisClientInfo;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Redis连接池测试，验证使用RedisTemplate的连接池工作机制
 * 知识点
 * （1）使用@Resource注入的对象存在连接池策略，使用RedisUtils工具类也同样存在；
 * （2）受LettuceConnectionFactory的setShareNativeConnection设置影响，决定是否使用线程池；
 * （3）只有在线程较多的场景多线程才会启用，单独的应用一般只会使用一个客户端连接；
 * （4）getClientList获取的是当前Redis的客户端连接个数，等同于redis命令“client list”或“info clients”；
 * （5）示例获取到的Redis客户端连接个数要多于此处线程池配置的个数，因为它获取到的并不是当前程序进程内的连接个数，而是Redis服务器上的全部连接个数，
 *     受“redis-cli”命令连接影响，同时若开启了主从策略也会多占用一个连接。
 *     即：getClientList = 当前进程启用个数（<=max-active） + redis-cli打开的连接个数 + redis-server主从或集群内部的连接；
 *
 * @author chendd
 * @date 2023/5/19 16:39
 */
public class ReadisConnectionPoolTest extends BaseTest {

    /*@Resource(name = RedisConstants.READ1_STRING_REDIS_TEMPLATE)
    private StringRedisTemplate stringRedisTemplate;*/

    /**
     * 累计循环10000个线程，观察getClientList的参数值
     */
    @Test
    public void poolStringRedisTemplate() throws InterruptedException {
        int count = 100;
        int number = 100;
        CountDownLatch countDownLatch = new CountDownLatch(count * number);
        AtomicInteger total = new AtomicInteger();
        for (int j = 1 ; j <= count ; j++) {
            Runnable runnable = () -> {
                StringRedisTemplate stringRedisTemplate = RedisUtils.getReadStringRedisTemplate();
                List<RedisClientInfo> clientList;
                String message;
                for (int i = 1; i <= number; i++) {
                    clientList = stringRedisTemplate.getClientList();
                    message = MessageFormat.format("第 {0} 个线程，共有 {1} 个客户端连接" , total.addAndGet(1) , clientList.size());
                    System.out.println(message);
                    countDownLatch.countDown();
                }
            };
            new Thread(runnable).start();
        }
        System.out.println("主线程准备等待...");
        countDownLatch.await();
        System.out.println("主线程执行完毕...");
    }

}
