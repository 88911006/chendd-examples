package cn.chendd.arthas.exmples.getstatic;

import java.awt.*;
import java.io.IOException;

/**
 * 演示获取静态字段
 *
 * @author chendd
 * @date 2023/9/25 13:16
 */
public class GetStatic extends Chendd {

    private static String USER_NAME = "chendongdong";

    private static final String WEBSITE = "https://www.chendd.cn";

    private static Point point = new Point(9 , 11);

    public static void main(String[] args) throws IOException {
        System.out.println("按任意键回车后退出...");
        System.in.read();
    }

}
