package cn.chendd.arthas.exmples.sysprop;

import java.util.concurrent.TimeUnit;

/**
 * 演示系统自定义属性获取
 *
 * @author chendd
 * @date 2023/9/24 19:54
 */
public class SysProperty {

    public static void main(String[] args) throws Exception {
        System.setProperty("chendd.name" , "chendongdong");
        for (int i = 0; i < 100; i++) {
            System.out.println(System.getProperty("chendd.name" , "chendd.name 未获取到"));
            System.out.println(System.getProperty("chendd.blog" , "chendd.blog 未获取到"));
            TimeUnit.SECONDS.sleep(1L);
        }

    }

}
