package cn.chendd.arthas.exmples.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志输出器
 *
 * @author chendd
 * @date 2023/9/24 22:04
 */
public class WebLogger {

    public static final Logger LOGGER = LoggerFactory.getLogger("WebLog");
    
    public static Logger getLogger() {
        return LOGGER;
    }

}
