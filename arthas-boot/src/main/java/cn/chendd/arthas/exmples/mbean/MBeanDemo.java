package cn.chendd.arthas.exmples.mbean;

import org.springframework.jmx.export.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 演示MBean
 *
 * @author chendd
 * @date 2023/9/25 20:55
 */
@Component
@ManagedResource(objectName = "cn.chendd.arthas:name=MBeanDemo" , description = "示例演示")
public class MBeanDemo {

    @ManagedAttribute(description = "获取系统名称")
    public String getName() {
        return "chendd";
    }

    @ManagedOperation(description = "输出Hello")
    @ManagedOperationParameters(value = {
            @ManagedOperationParameter(name = "name", description = "名称")
    })
    public String sayHello(String name) {
        return String.format("hello , %s" , name);
    }

    @ManagedOperation(description = "关闭服务器")
    public void shutdown() {
        System.exit(0);
    }

}
