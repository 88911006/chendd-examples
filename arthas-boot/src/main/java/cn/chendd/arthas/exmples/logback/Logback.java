package cn.chendd.arthas.exmples.logback;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * 演示日志文件
 * @author chendd
 * @date 2023/9/24 21:50
 */
public class Logback {

    public static void main(String[] args) throws Exception {
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(lc);
        lc.reset(); // 清除现有的默认配置
        // 指定配置文件的路径
        try {
            configurator.doConfigure(Logback.class.getResource("logback.xml"));
        } catch (JoranException e) {
            // 如果配置文件加载失败，则输出错误信息
            e.printStackTrace();
        }
        // 确认配置是否加载成功
        StatusPrinter.printInCaseOfErrorsOrWarnings(lc);
        // 使用 logger 输出日志
        for (int i = 0; i < 1000; i++) {
            AdminLogger.getLogger().debug("AdminLogger debug i = {}" , i);
            AdminLogger.getLogger().info("AdminLogger info i = {}" , i);
            AdminLogger.getLogger().warn("AdminLogger warn i = {}" , i);
            WebLogger.getLogger().debug("WebLogger debug i = {}" , i);
            WebLogger.getLogger().info("WebLogger info i = {}" , i);
            WebLogger.getLogger().warn("WebLogger warn i = {}" , i);
            TimeUnit.SECONDS.sleep(1);
        }
    }
}
