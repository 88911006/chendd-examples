package cn.chendd.arthas.exmples.vmtool;

/**
 * 演示vmtool获取
 *
 * @author chendd
 * @date 2023/9/24 21:03
 */
public class VmTool {

    private static User u1 = new User("chendd-1");
    private static User u2 = new User("chendd-2");

    private User u3 = new User("chendd-3");

    private String name = "chendongdong";
    private String website = "http://www.chendd.cn";

    public static void main(String[] args) throws Exception {
        String name1 = new String("cdd-1");
        String name2 = "cdd-2";
        User u4 = new User("chendd-3");
        new Thread(() -> new User("cdd-5")).start();
        System.out.println("按任意键回车后退出...");
        System.in.read();
    }

    public static class User {

        private String name;

        public User(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
