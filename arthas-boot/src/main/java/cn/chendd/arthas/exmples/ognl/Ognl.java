package cn.chendd.arthas.exmples.ognl;

import java.awt.*;
import java.io.IOException;

/**
 * 演示Ognl
 *
 * @author chendd
 * @date 2023/9/25 15:06
 */
public class Ognl {

    public static void main(String[] args) throws IOException {
        System.out.println("按任意键回车后退出...");
        System.in.read();
    }

    public String getJavaVersion() {
        return System.getProperty("java.version");
    }

    public String sayHello(String name) {
        return "hello , " + name;
    }

    public static String create(Point point , String value) {
        return "x = " + point.x + ", y = " + point.y + " , value = " + value;
    }

    public static void stop() {
        System.exit(0);
    }

}
