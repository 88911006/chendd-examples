package cn.chendd.arthas.exmples.sysenv;

import java.util.concurrent.TimeUnit;

/**
 * 演示系统环境变量获取
 *
 * @author chendd
 * @date 2023/9/24 20:12
 */
public class SysEnvironement {

    public static void main(String[] args) throws Exception {

        System.load("E:/app/chendd-env/jacob-1.20-x86.dll");
        String library = System.getProperty("java.library.path");
        System.setProperty("java.library.path" , library + ";E:/app/chendd-env");
        for (int i = 0; i < 1000; i++) {
            TimeUnit.SECONDS.sleep(1L);
        }
    }

}
