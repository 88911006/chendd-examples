package cn.chendd.arthas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 启动类
 *
 * @author chendd
 * @date 2023/9/22 14:08
 */
@SpringBootApplication
@RestController
public class BootstrapArthas {

    public static void main(String[] args) {

        SpringApplication.run(BootstrapArthas.class , args);
    }

    @GetMapping
    public String sayHello() {
        /*try {
            TimeUnit.SECONDS.sleep(2L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        System.out.println("问题已解决，第 [5] 个版本");
        return "hello, chendd 第 [5] 个版本";
    }

}
