package cn.chendd.jjwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.security.Keys;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.crypto.SecretKey;
import java.util.UUID;

/**
 * 对称加密简单示例
 *
 * @author chendd
 * @date 2022/11/6 9:02
 */
@RunWith(JUnit4.class)
public class JjwtShaTest {

    @Test
    public void test() {
        //生成随机key
        String jwtTokenRandomKey = Jwts.builder().setPayload("chendd").signWith(Keys.secretKeyFor(SignatureAlgorithm.HS256)).compact();
        System.out.println("随机密钥key生成Token：" + jwtTokenRandomKey);
        System.out.println("------------------------------------");

        //使用自定义密钥 签名 注意源码中限制了密钥存在的最小长度限制 【传递字节数组长度 * 8 必须大于定等于加密枚举实例中定义的最小大小】
        SecretKey key = Keys.hmacShaKeyFor("abcdefghijklmnopqrstuvwxyz123456".getBytes());

        //使用 payload 设置 content 数据
        {
            String jwtToken = Jwts.builder().setPayload("chendd").signWith(key).compact();
            System.out.println("固定密钥key和Payload生成Token：" + jwtToken);
            //解密
            Jwt parse = Jwts.parserBuilder().setSigningKey(key).build().parse(jwtToken);
            System.out.println("固定密钥key解析Token Header：" + parse.getHeader());
            System.out.println("固定密钥key解析Token Body：" + parse.getBody());
        }
        System.out.println("------------------------------------");

        //使用 chaim 设置 header 和 content 数据
        {
            String jwtToken = Jwts.builder()
                    //设置header
                    .setHeaderParam(JwsHeader.TYPE , JwsHeader.JWT_TYPE)
                    .setHeaderParam(JwsHeader.ALGORITHM , SignatureAlgorithm.HS256.name())
                    //设置 claim 标准参数（另有更多）
                    .setClaims(new DefaultClaims()
                            //jwt Id
                            .setId(UUID.randomUUID().toString())
                            //jwt 主题
                            .setSubject("chendd-examples-jjwt")
                    )
                    //设置 自定义 参数
                    .claim("userId" , "88911006")
                    .claim("userName" , "chendd")
                    //设置 密钥 签名
                    .signWith(key)
                    .compact();
            System.out.println("固定密钥key和Chaim生成Token：" + jwtToken);
            //解密
            Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwtToken);
            System.out.println("Header：" + claimsJws.getHeader());
            System.out.println("Body：" + claimsJws.getBody());
            System.out.println("Signature：" + claimsJws.getSignature());
        }
    }

}
