package cn.chendd.jjwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.UUID;

/**
 * Jwt RSA 非对称加密
 *
 * @author chendd
 * @date 2022/11/6 10:20
 */
@RunWith(JUnit4.class)
public class JwtRsaTest {

    /**
     * 私钥
     */
    private Key privateKey;
    /**
     * 公钥
     */
    private Key publicKey;

    @Before
    public void initKey() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048);
        KeyPair keyPair = generator.genKeyPair();
        this.privateKey = keyPair.getPrivate();
        this.publicKey = keyPair.getPublic();
        System.out.println("私钥：" + Base64.getEncoder().encodeToString(this.privateKey.getEncoded()));
        System.out.println("公钥：" + Base64.getEncoder().encodeToString(this.publicKey.getEncoded()));
    }

    @Test
    public void test() {
        String jwtToken = Jwts.builder()
                //设置header
                .setHeaderParam(JwsHeader.TYPE , JwsHeader.JWT_TYPE)
                .setHeaderParam(JwsHeader.ALGORITHM , SignatureAlgorithm.HS256.name())
                //设置 claim 标准参数（另有更多）
                .setClaims(new DefaultClaims()
                        //jwt Id
                        .setId(UUID.randomUUID().toString())
                        //jwt 主题
                        .setSubject("chendd-examples-jjwt")
                )
                //设置 自定义 参数
                .claim("userId" , "88911006")
                .claim("userName" , "chendd")
                //设置 密钥 签名
                .signWith(this.privateKey)
                .compact();
        System.out.println("非对称加密key和Chaim生成Token：" + jwtToken);
        System.out.println("------------------------------------");
        //解密
        Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(this.publicKey).build().parseClaimsJws(jwtToken);
        System.out.println("Header：" + claimsJws.getHeader());
        System.out.println("Body：" + claimsJws.getBody());
        System.out.println("Signature：" + claimsJws.getSignature());
    }

}
