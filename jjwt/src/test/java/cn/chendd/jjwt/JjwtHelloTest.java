package cn.chendd.jjwt;

import io.jsonwebtoken.Jwts;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

/**
 * Jjwt的基本应用测试实践
 *
 * @author chendd
 * @date 2022/10/22 22:03
 */
@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JjwtHelloTest {

    String jwtTokenHello;

    @Before
    public void generateToken() {
        this.jwtTokenHello = Jwts.builder().setPayload("Hello Wolrd").compact();
    }

    @Test
    public void hello() {
        System.out.println("默认无加密的token签名：" + this.jwtTokenHello);
    }

    @Test
    public void helloParse() {
        System.out.println("解析无加密的token签名：" + Jwts.parserBuilder().build().parse(this.jwtTokenHello));
    }


}
