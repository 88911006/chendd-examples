package cn.chendd.jjwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.security.Keys;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.UUID;

/**
 * Jjwt基本使用测试，
 *
 * @author chendd
 * @date 2022/10/23 10:03
 */
@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JjwtBasicTest {

    private static String JWT_TOKEN;
    private static SecretKey KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    @Test
    public void generateToken() {
        long current = System.currentTimeMillis();
        JWT_TOKEN = Jwts.builder()
                //设置 header 部分头信息
                .setHeaderParam(JwsHeader.TYPE, JwsHeader.JWT_TYPE)
                .setHeaderParam(JwsHeader.ALGORITHM, SignatureAlgorithm.HS256.getValue())
                //设置 payload 部分有效载荷信息（标准的Token属性）
                .setClaims(new DefaultClaims()
                        //唯一身份标识，用于生成一次性Token，避免重放攻击
                        .setId(UUID.randomUUID().toString())
                        //设置Token主题
                        .setSubject("chendd-jwt-example")
                        //过期时间，指定从现在开始到什么时候过期
                        .setExpiration(new Date(current + 1000 * 60 * 60 * 12))
                        //设置Token签发者
                        .setIssuer("chendd")
                        //设置Token签发时间
                        .setIssuedAt(new Date(current))
                        //设置Token开始可用时间
                        .setNotBefore(new Date(current))
                        //设置Token接收方
                        .setAudience("example")
                )
                //设置加密信息
                .signWith(KEY)
                .compact();
        System.out.println("生成JwtToken：" + JWT_TOKEN);
    }

    @Test
    public void validatorToken() {
        //默认解析
        Jwt jwt = Jwts.parserBuilder().setSigningKey(KEY).build().parse(JWT_TOKEN);
        Header header = jwt.getHeader();
        System.out.println("解析JwtToken Header：" + header);
        DefaultClaims payload = (DefaultClaims) jwt.getBody();
        System.out.println("解析JwtToken Payload：" + payload);
        //可以拿到签名参数
        Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(KEY).build().parseClaimsJws(JWT_TOKEN);
        System.out.println(claimsJws);
    }

}
