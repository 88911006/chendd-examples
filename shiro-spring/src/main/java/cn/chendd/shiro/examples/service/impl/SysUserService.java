package cn.chendd.shiro.examples.service.impl;

import cn.chendd.shiro.examples.dao.SysUserDao;
import cn.chendd.shiro.examples.model.SysUser;
import cn.chendd.shiro.examples.service.ISysUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author chendd
 * @date 2019/6/16 21:53
 */
@Service
public class SysUserService implements ISysUserService {

    @Resource
    private SysUserDao sysUserDao;

    @Override
    public SysUser getSysUserByUserName(String userName) {
        return sysUserDao.getSysUserByUserName(userName);
    }
}
