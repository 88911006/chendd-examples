package cn.chendd.shiro.examples.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Set;

/**
 * 权限管理Controller
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {

    @Resource
    private RequestMappingHandlerMapping rmhp;

    @RequestMapping("/reload")
    public String reload(HttpServletRequest request , HttpSession session) {
        /*System.out.println(request.getClass());
        System.out.println(request.getSession());
        System.out.println(session);
        System.out.println(request.getSession().getId());
        System.out.println(session.getId());
        System.out.println(UUID.randomUUID().toString());*/
        System.out.println("PermissionController.reload --- " + rmhp);
        System.out.println(SecurityUtils.getSecurityManager() instanceof RealmSecurityManager);
        System.out.println(SecurityUtils.getSecurityManager().getClass());
        System.out.println(SecurityUtils.getSubject());

        Map<RequestMappingInfo, HandlerMethod> handlerMethods = rmhp.getHandlerMethods();
        System.out.println(handlerMethods.size());
        Set<Map.Entry<RequestMappingInfo , HandlerMethod>> entrySet = handlerMethods.entrySet();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> entry : entrySet) {
            System.out.println(entry.getKey() + "---" + entry.getValue());

        }

        return "success";
    }

}
