package cn.chendd.shiro.examples.model;

import java.io.Serializable;
import java.security.Principal;

/**
 * @author chendd
 * @date 2019/6/16 21:56
 * 系统用户实体对象
 */
public class SysUser implements Serializable , Principal {

    private static final long serialVersionUID = -9157414070344516006L;
    private Integer userId;//用户ID
    private String userName;//用户名
    private String passWord;//用户密码
    private String realName;//真实姓名
    private String status;//DISABLED表示锁定，ENABLE为可用

    public SysUser() {
    }

    public SysUser(Integer userId, String userName, String passWord, String realName , String status) {
        this.userId = userId;
        this.userName = userName;
        this.passWord = passWord;
        this.realName = realName;
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getName() {
        return "chendd-shiro";
    }
}
