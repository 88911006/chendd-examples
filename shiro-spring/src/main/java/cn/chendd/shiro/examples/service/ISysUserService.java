package cn.chendd.shiro.examples.service;

import cn.chendd.shiro.examples.model.SysUser;

/**
 * @author chendd
 * @date 2019/6/16 21:53
 * 系统用户相关Service接口定义
 */
public interface ISysUserService {
    SysUser getSysUserByUserName(String userName);
}
