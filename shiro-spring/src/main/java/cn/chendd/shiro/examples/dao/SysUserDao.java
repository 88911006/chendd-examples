package cn.chendd.shiro.examples.dao;

import cn.chendd.shiro.examples.model.SysUser;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chendd
 * @date 2019/6/16 21:55
 * 系统用户相关Dao
 */
@Repository
public class SysUserDao {

    //根据用户名获取用户，判断用户名是否存在
    public SysUser getSysUserByUserName(String userName){
        SysUser user = null;
        List<SysUser> dataList = getAllUsers();
        for (SysUser sysUser : dataList) {
            if(sysUser.getUserName().equals(userName)){
                user = sysUser;
                break;
            }
        }
        return user;
    }

    //所有用户
    public List<SysUser> getAllUsers(){

        List<SysUser> dataList = new ArrayList<SysUser>();
        //密码为用户名+123，且以用户名为盐，加密7次，如chendd的密文为：(chendd123 + chendd为盐) * 7次
        dataList.add(new SysUser(1001 , "chendd" , "304029978ded47a4dd0fdf9454db2e04" , "陈dd" , "ENABLE"));
        dataList.add(new SysUser(1002 , "jiajt" , "xxxxxxx" , "贾jt" , "DISABLED"));
        dataList.add(new SysUser(1003 , "sunm" , "d0c974c25431cc8978fcbaf8bb65afb4" , "孙m" , "ENABLE"));
        dataList.add(new SysUser(1004 , "lisl" , "xxxxxxx" , "李sl" , "DISABLED"));
        return dataList;
    }

}
