package cn.chendd.shiro.examples.enums;

/**
 * @author chendd
 * @date 2019/6/16 20:42
 * 登录失败时的异常定义
 */
public enum EnumLoginFail {

    UnknownAccountException("org.apache.shiro.authc.UnknownAccountException" , "用户名不存在！"),
    IncorrectCredentialsException("org.apache.shiro.authc.IncorrectCredentialsException" , "用户名或密码不匹配！"),
    AccountException("org.apache.shiro.authc.LockedAccountException" , "用户被锁定！"),
    ValidCodeError("ValidCodeError" , "验证码错误！"),
    UnknowException("UnknowException" , "未知异常"),
    ;

    private String key , text;

    private EnumLoginFail(String key , String text){
        this.key = key;
        this.text = text;
    }

    public static String getLogFail(String loginFail) {
        EnumLoginFail fail = UnknowException;
        for (EnumLoginFail enumFail : EnumLoginFail.values()){
            if(enumFail.getKey().equalsIgnoreCase(loginFail)){
                fail = enumFail;
                break;
            }
        }
        return fail.getText();
    }

    public String getKey() {
        return key;
    }

    public String getText() {
        return text;
    }
}
