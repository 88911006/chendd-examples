package cn.chendd.shiro.examples.controller;

import cn.chendd.shiro.examples.commponent.CustomLoginFormFilter;
import cn.chendd.shiro.examples.enums.EnumLoginFail;
import cn.chendd.shiro.examples.model.SysUser;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;

/**
 * @author chendd
 * @date 2019/6/15 0:44
 * 提供登录、退出等功能地址
 */
@Controller
public class IndexController {

    @GetMapping("/test")
    @ResponseBody
    public String test(){
        System.out.println("test...");
        Subject subject = SecurityUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        System.out.println(JSON.toJSONString(sysUser));
        return "success-test";
    }

    @RequestMapping("/login")
    public String login(HttpServletRequest request){
        String loginFail = (String) request.getAttribute(CustomLoginFormFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
        System.out.println(loginFail + "---loginFail");
        String method = request.getMethod();
        if(loginFail != null &&  "POST".equalsIgnoreCase(method)){
            String text = EnumLoginFail.getLogFail(loginFail);
            request.setAttribute("loginFail" , text);
        }
        //登录的地址被放行了，增加一个判定，防止登录后的输入登录地址出现重新登录页面
        HttpSession session = request.getSession();
        SimplePrincipalCollection simplePrincipal = (SimplePrincipalCollection) session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
        if(simplePrincipal != null){
            return this.index(request);
        }
        return "login";
    }

    @RequestMapping("/index")
    public String index(HttpServletRequest request){
        HttpSession session = request.getSession();
        Enumeration<String> names = session.getAttributeNames();
        while (names.hasMoreElements()){
            String name = names.nextElement();
            System.out.println(name + "--" + session.getAttribute(name));
        }

        Object sysUser = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
        if(sysUser == null){
            return this.login(request);
        }

        return "index";
    }

    @RequestMapping("/")
    public String i(HttpServletRequest request){
        System.out.println("/////////////////////////////////");
        return this.index(request);
    }


    /*@RequestMapping("/logout")
    public String logout(HttpServletRequest request){
        System.err.println("IndexController.logout");
        SecurityUtils.getSubject().logout();
        return this.i(request);
    }*/

    @RequestMapping("/unauth")
    @ResponseBody
    public String unauth(){

        System.out.println("----------------unauth");
        return "unauth";
    }

}
