package cn.chendd.shiro.examples.controller;

import com.alibaba.fastjson.JSON;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.awt.*;
import java.util.Random;

/**
 * 测试权限
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/queryList")
    @RequiresRoles("userManager")//根据角色验证
    @RequiresPermissions("user:queryList")//根据权限验证
    public String queryList(){
        System.out.println("UserController.queryList");
        return "success";
    }

    @RequestMapping("/setSessionData")
    public String setSessionData(HttpSession session){
        System.out.println("UserController.setSessionData");
        Integer x = new Random().nextInt(10000);
        Integer y = new Random().nextInt(10000);
        session.setAttribute("sessionData" , new Point(x , y));
        return "success";
    }

    @RequestMapping("/getSessionData")
    @ResponseBody
    public String fetSessionData(HttpSession session){
        return JSON.toJSONString(session.getAttribute("sessionData"));
    }

}
