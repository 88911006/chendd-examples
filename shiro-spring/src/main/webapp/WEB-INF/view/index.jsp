<%@page pageEncoding="utf-8" contentType="text/html; utf-8" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
<meta charset="utf-8" />
</head>
<body>
<%--property="userName"--%>
<h3>欢迎你：<shiro:principal></shiro:principal >&nbsp;<a href="/logout">退出</a></h3>
<h4><a href="/user/queryList">用户查询列表</a></h4>
<h4><a href="/user/setSessionData">设置session数据</a>&nbsp;<a href="/user/getSessionData">获取session数据</a></h4>

    角色：<shiro:lacksRole name="userManager"><span style="color: red;">无</span></shiro:lacksRole>
         <shiro:hasRole name="userManager"><span style="color: green;">有</span></shiro:hasRole>，
    权限：<shiro:lacksPermission name="user:queryList"><span style="color: red;">无</span></shiro:lacksPermission>
         <shiro:hasPermission name="user:queryList"><span style="color: green;">有</span></shiro:hasPermission>
</h4>
<h4><a href="/dynamicPermission/reloadPermission">当前用户权限加载（了解相关Api函数）</a></h4>
<h4><a href="/dynamicPermission/clearPermission">清空当前用户权限</a></h4>

</body>
</html>
