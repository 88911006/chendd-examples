<%@ page pageEncoding="utf-8" contentType="text/html; utf-8" isELIgnored="false" %>
<html>
<head>
<title>登录页面</title>
<style type="text/css">
    tr{
        height: 30px;
    }
</style>
<script type="text/javascript">
window.onload = function () {
    document.getElementById("validCode_id").value = document.getElementById("validCodeImage_id").innerText;
};
</script>
</head>
<body>

<form action="login" method="post">
    <table align="center">
        <tr>
            <td>用户名：</td>
            <td><input type="text" name="userName" /></td>
        </tr>
        <tr>
            <td>密码：</td>
            <td><input type="text" name="passWord" /></td>
        </tr>
        <tr>
            <td>验证码：</td>
            <td><input type="text" name="validCode" id="validCode_id" value="" placeholder="请输入验证码" style="width:140px;" />
                <span id="validCodeImage_id"><jsp:include page="validCode.jsp" /></span></td>
        </tr>
        <tr>
            <td>
                <label>记住我：<input type="checkbox" name="remMe" /></label>
            </td>
            <td align="center">
                <input type="submit" value="登录" />
            </td>
        </tr>
        <tr>
            <td colspan="2">${loginFail}</td>
        </tr>
    </table>
</form>
</body>
</html>
