package cn.chendd.vo;

import java.awt.Point;

import static cn.chendd.common.CommonConstants.*;
import cn.chendd.enums.EnumGameStatus;
import cn.chendd.enums.EnumLevelSpeed;

/**
 * 游戏上下文数据
 * 
 * @author chendd
 * 
 */
public class GameContext {

	private int next;// 下一个方片的预览
	private int typeCode;// 当前的方块编号，下标
	private Point actPoint[];// 当前的方块坐标集
	private boolean gameMap[][] = new boolean[MAX_X + 1][MAX_Y + 1];//游戏地图
	private int gameStatus = EnumGameStatus.UN_START.getStatus();//游戏状态
	private boolean isOver;//是否为已结束,此状态下还是需要绘制地图的
	private int level;//等级,1---7级
	private int score;//分数，每1000分升个级
	
	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public Point[] getActPoint() {
		return actPoint;
	}

	public void setActPoint(Point[] actPoint) {
		this.actPoint = actPoint;
	}

	public int getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(int typeCode) {
		this.typeCode = typeCode;
	}

	public boolean[][] getGameMap() {
		return gameMap;
	}

	public void setGameMap(boolean[][] gameMap) {
		this.gameMap = gameMap;
	}

	public int getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(int gameStatus) {
		this.gameStatus = gameStatus;
	}

	public int getLevel() {
		int currentLevel = this.score / 1000 + 1;
		if(currentLevel > EnumLevelSpeed.values().length){
			currentLevel = EnumLevelSpeed.values().length;
		}
		this.level = currentLevel;
		return level;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean isOver() {
		return isOver;
	}

	public void setOver(boolean isOver) {
		this.isOver = isOver;
	}

}
