package cn.chendd.main;

import cn.chendd.common.MyImageIcon;
import cn.chendd.service.GameService;
import cn.chendd.ui.GamePanel;
import cn.chendd.vo.GameContext;

import javax.swing.*;

/**
 * 游戏主窗口
 * @author chendd
 */
public class MainFrame extends JFrame{

	private static final long serialVersionUID = 1L;

	public MainFrame(){
		super.setSize(567, 620);
		super.setTitle("俄罗斯方块");
		super.setLocationRelativeTo(null);
		super.setIconImage(new MyImageIcon("graphics/logo.png").getImage());
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GameContext gameContext = new GameContext();//游戏上下文对象
		GameService gameService = new GameService(gameContext);//游戏业务对象
		GamePanel gamePanel = new GamePanel(gameService);
		gamePanel.setRequestFocusEnabled(true);//设置面板获得焦点，响应键盘事件
		super.setContentPane(gamePanel);
		super.setLayout(null);//自定义坐标布局
		super.setResizable(false);
		super.setContentPane(gamePanel);
		super.setVisible(true);
	}
	
	public static void main(String[] args) {
		
		new MainFrame();
	}
	
}
