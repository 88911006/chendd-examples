package cn.chendd.ui;

import static cn.chendd.common.CommonConstants.START_BUTTON;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;

import cn.chendd.common.CommonConstants;
import cn.chendd.enums.EnumGameStatus;
import cn.chendd.enums.EnumLevelSpeed;
import cn.chendd.service.GameService;
import cn.chendd.vo.GameContext;
/**
 * 游戏主控制面板
 * @author chendd
 */
public class GamePanel extends JPanel {
	
	JButton controlButton = null;//开始、暂停、继续按钮
	private Thread autoDownThread = null;//方块自动下落线程
	
	private GameService gameService;//游戏业务处理
	
	public GamePanel(GameService gameService){
		this.gameService = gameService;
		initComponent();
	}
	
	/**
	 * 初始化下落线程数据
	 */
	private void initDownThread() {
		this.autoDownThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				GamePanel.this.repaint();//线程启动之前先刷新一下方块，防止看到的新方块总是出现在第一行
				//如果游戏开始了就刷新方块
				GameContext gameContext = gameService.getGameContext();
				gameContext.setGameStatus(EnumGameStatus.RUNING.getStatus());
				while(true){
					try {
						int gameStatus = gameContext.getGameStatus();
						//是否暂停
						if(gameStatus == EnumGameStatus.PAUSE.getStatus()){
							Thread.sleep(200L);
							continue;
						}
						//是否正在运行
						if(gameStatus == EnumGameStatus.RUNING.getStatus()){
							int level = gameContext.getLevel();
							Long sleep = EnumLevelSpeed.getEnumDataByLevel(level).getSleep();
							Thread.sleep(sleep);
							gameService.keyDown();
							GamePanel.this.repaint();
							continue;
						}
						//是否已经结束
						if(gameStatus == EnumGameStatus.UN_START.getStatus()){
							//重置控制按钮
							controlButton.setIcon(CommonConstants.START_BUTTON);
							break;
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		});
	}
	
	/**
	 * 初始化相关元素
	 */
	public void initComponent(){
		controlButton = new JButton();
		//初始化相关开始、暂停按钮数据
		controlButton.setIcon(START_BUTTON);
		controlButton.setLayout(null);
		controlButton.setLocation(100, 100);
		controlButton.setBounds(385, 265, 120, 50);
		controlButton.addMouseListener(new MouseAdapter(){

			@Override
			public void mouseClicked(MouseEvent e) {
				GameContext gameContext = gameService.getGameContext();
				int gameStatus = gameContext.getGameStatus();
				if(gameStatus == EnumGameStatus.PAUSE.getStatus()){
					controlButton.setIcon(CommonConstants.PAUSE_BUTTON);
					gameContext.setGameStatus(EnumGameStatus.RUNING.getStatus());
				} else if (gameStatus == EnumGameStatus.RUNING.getStatus()) {
					gameContext.setGameStatus(EnumGameStatus.PAUSE.getStatus());
					controlButton.setIcon(CommonConstants.CONTINUE_BUTTON);
				} else if(gameStatus == EnumGameStatus.UN_START.getStatus()){
					gameService.resetGame();//重置游戏...
					initDownThread();//初始化下落线程
					autoDownThread.start();
					controlButton.setIcon(CommonConstants.PAUSE_BUTTON);
				}
			}
		});
		
		this.add(controlButton);
		
		this.addKeyListener(new GamePlayer(this));
		
	}
	
	private static final long serialVersionUID = 1L;

	/**
	 * 定义各个面板窗口
	 */
	public Layer lays[] = new Layer[]{
		new LayerBackground(0, 0, 0, 0),
		new LayerGame(0, 0, 320, 576),
		new LayerNext(340, 0, 200, 100),
		new LayerLevel(340, 117, 200, 100),
		new LayerAbout(340 , 352 , 200 , 150)
	};
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (Layer lay : lays) {
			lay.setGameService(gameService);
			lay.paint(g);
		}
		this.requestFocus();//有这个设置可以将焦点直接聚集在panel上，并且panel上添加的按钮可以立即显示
	}

	public GameService getGameService() {
		return gameService;
	}

	public void setGameService(GameService gameService) {
		this.gameService = gameService;
	}

}
