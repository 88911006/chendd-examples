package cn.chendd.ui;

import java.awt.Graphics;
import java.awt.Image;

import cn.chendd.common.MyImageIcon;
import cn.chendd.enums.EnumGameStatus;

/**
 * 下一个方块的预览
 * @author chendd
 *
 */
public class LayerNext extends Layer {
	
	private static final Image NEXT_ACT[];//所有方块的所有的图片绘制

	static{
		NEXT_ACT = new Image[7];
		for(int i=0 ; i < 7 ; i++){
			NEXT_ACT[i] = new MyImageIcon("graphics/game/" + i + ".png").getImage();
		}
	}
	
	public LayerNext(int locationX, int locationY, int winWidth, int winHeight) {
		super(locationX, locationY, winWidth, winHeight);
	}

	@Override
	public void paint(Graphics g) {
		super.drawWinow(g);
		
		int status = super.gameService.getGameContext().getGameStatus();
		if(status == EnumGameStatus.RUNING.getStatus() || status == EnumGameStatus.PAUSE.getStatus()){
			int nextIndex = super.gameService.getGameContext().getNext();
			Image img = NEXT_ACT[nextIndex];
			drawNextImage(g , img);//绘制居中的下一个方块提示
		}
		
	}

}
