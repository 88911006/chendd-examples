package cn.chendd.ui;

import static cn.chendd.common.CommonConstants.BORDER;

import java.awt.Graphics;
import java.awt.Image;

import cn.chendd.common.MyImageIcon;

/**
 * 关于游戏
 * @author chendd
 *
 */
public class LayerAbout extends Layer{

	Image aboutImage = new MyImageIcon("graphics/string/about.png").getImage();
	
	public LayerAbout(int locationX, int locationY, int winWidth, int winHeight) {
		super(locationX, locationY, winWidth, winHeight);
	}

	@Override
	public void paint(Graphics g) {
		//super.drawWinow(g);//无需显示边框
		g.drawImage(aboutImage, super.locationX + BORDER + 10,  super.locationY + BORDER + 10, null);
	}

}
