package cn.chendd.ui;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * 游戏按键控制的处理
 * @author chendd
 *
 */
public class GamePlayer extends KeyAdapter {
	
	private GamePanel gamePanel;//游戏主面板
	
	public GamePlayer(GamePanel gamePanel){
		this.gamePanel = gamePanel;
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		switch (key) {
			case KeyEvent.VK_UP:
				gamePanel.getGameService().keyUp();
				break;
			case KeyEvent.VK_DOWN:
				gamePanel.getGameService().keyDown();
				break;
			case KeyEvent.VK_LEFT:
				gamePanel.getGameService().keyLeft();
				break;
			case KeyEvent.VK_RIGHT:
				gamePanel.getGameService().keyRight();
				break;
			case KeyEvent.VK_SPACE:
				gamePanel.getGameService().quickDown();
				break;
			default:
				break;
		}
		gamePanel.repaint();
	}

	public GamePanel getGamePanel() {
		return gamePanel;
	}

}
