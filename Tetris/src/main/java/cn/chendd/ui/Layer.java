package cn.chendd.ui;

import static cn.chendd.common.CommonConstants.BORDER;
import static cn.chendd.common.CommonConstants.WINDOW_HEIGHT;
import static cn.chendd.common.CommonConstants.WINDOW_IMAGE;
import static cn.chendd.common.CommonConstants.WINDOW_WIDTH;

import java.awt.Graphics;
import java.awt.Image;

import cn.chendd.service.GameService;

/**
 * 显示绘图窗口以及一些公共的函数定义
 * @author chendd
 * 
 */
public abstract class Layer {

	protected int locationX, locationY, winWidth, winHeight;
	
	protected GameService gameService;//游戏业务处理

	public Layer(int locationX, int locationY, int winWidth, int winHeight) {
		this.locationX = locationX;
		this.locationY = locationY;
		this.winWidth = winWidth;
		this.winHeight = winHeight;
	}

	/**
	 * 定义绘图窗口，各自子类实现
	 * @param g 画笔
	 */
	public abstract void paint(Graphics g);
	
	/**
	 * 绘制区域窗口
	 * @param g 画笔
	 */
	public void drawWinow(Graphics g) {
		// 绘制左上角
		g.drawImage(WINDOW_IMAGE, locationX, locationY, locationX + BORDER,
				locationY + BORDER, 0, 0, BORDER, BORDER, null);
		// 左中
		g.drawImage(WINDOW_IMAGE, locationX + BORDER, locationY, locationX
				+ BORDER + winWidth, locationY + BORDER, BORDER, 0,
				WINDOW_WIDTH - BORDER, BORDER, null);
		// 左右
		g.drawImage(WINDOW_IMAGE, locationX + BORDER + winWidth, locationY,
				locationX + BORDER + winWidth + BORDER, locationY + BORDER,
				WINDOW_WIDTH - BORDER, 0, WINDOW_WIDTH, BORDER, null);
		// 中左
		g.drawImage(WINDOW_IMAGE, locationX, locationY + BORDER, locationX
				+ BORDER, locationY + BORDER + winHeight, 0, BORDER, BORDER,
				WINDOW_HEIGHT - BORDER, null);
		// 中右
		g.drawImage(WINDOW_IMAGE, locationX + BORDER + winWidth, locationY
				+ BORDER, locationX + BORDER + winWidth + BORDER, locationY
				+ BORDER + winHeight, WINDOW_WIDTH - BORDER, BORDER,
				WINDOW_WIDTH, WINDOW_HEIGHT - BORDER, null);
		// 左下
		g.drawImage(WINDOW_IMAGE, locationX, locationY + BORDER + winHeight,
				locationX + BORDER, locationY + BORDER + winHeight + BORDER, 0,
				WINDOW_HEIGHT - BORDER, BORDER, WINDOW_HEIGHT, null);
		// 中下
		g.drawImage(WINDOW_IMAGE, locationX + BORDER, locationY + BORDER
				+ winHeight, locationX + BORDER + winWidth, locationY + BORDER
				+ winHeight + BORDER, BORDER, WINDOW_HEIGHT - BORDER,
				WINDOW_WIDTH - BORDER, WINDOW_HEIGHT, null);
		// 右下
		g.drawImage(WINDOW_IMAGE, locationX + BORDER + winWidth, locationY
				+ BORDER + winHeight, locationX + BORDER + winWidth + BORDER,
				locationY + BORDER + winHeight + BORDER, WINDOW_WIDTH - BORDER,
				WINDOW_HEIGHT - BORDER, WINDOW_WIDTH, WINDOW_HEIGHT, null);
	}

	/**
	 * 根据当前的方块编号来绘制方块，区域内居中
	 * @param g 画笔
	 * @param img 图片
	 */
	public void drawNextImage(Graphics g , Image img) {
		//根据图片的宽高，将图片绘制在中间的区域，计算方法同窗口居中
		int imgW = img.getWidth(null);
		int imgH = img.getHeight(null);
		int top = (this.winHeight - imgH) / 2;
		int left = (this.winWidth - imgW) / 2;
		g.drawImage(img, this.locationX + left , this.locationY + BORDER + top, null);
	}
	
	/**
	 * 绘制图片的等级，将传递到number转换为图片显示
	 * @param g 画笔
	 * @param num 数字
	 * @param numImage 数字对应的图片
	 */
	public void drawNumberLevel(Graphics g, int num , Image numImage) {
		String ns = String.valueOf(num);
		int w = numImage.getWidth(null);
		int h = numImage.getHeight(null);
		int aWidth = w / 10;
		int width = ns.length() * aWidth;
		int left = (this.winWidth - width) / 2;
		int top = (this.winHeight - h) / 2;
		for(int i=0 ; i < ns.length() ; i++){
			int n = ns.charAt(i) - 48;
			int start = n * aWidth;
			int end = start + aWidth;
			g.drawImage(numImage, 
					this.locationX + left + i * aWidth, 
					this.locationY + top, 
					this.locationX + left + (i + 1) * aWidth,
					this.locationY + h + top, 
					start, 0, end, h, null);
		}
	}
	
	/**
	 * 将一组数值显示成一个图片，可自定义便宜位置
	 * TODO 此函数可以与上面的drawNumberLevel重构
	 * @param g 画笔
	 * @param num 数字
	 * @param numImage 数字图片
	 * @param left 左偏移
	 * @param top 右偏移
	 */
	public void drawNumberLevel(Graphics g, int num, Image numImage , int left , int top) {
		String ns = String.valueOf(num);
		int w = numImage.getWidth(null);
		int h = numImage.getHeight(null);
		int aWidth = w / 10;
		for(int i=0 ; i < ns.length() ; i++){
			int n = ns.charAt(i) - 48;
			int start = n * aWidth;
			int end = start + aWidth;
			g.drawImage(numImage, 
					this.locationX + left + i * aWidth, 
					this.locationY + top, 
					this.locationX + left + (i + 1) * aWidth,
					this.locationY + h + top, 
					start, 0, end, h, null);
		}
	}

	
	public GameService getGameService() {
		return gameService;
	}

	public void setGameService(GameService gameService) {
		this.gameService = gameService;
	}
}
