package cn.chendd.ui;

import static cn.chendd.common.CommonConstants.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import cn.chendd.enums.EnumGameStatus;

/**
 * 游戏显示方块的窗口
 * @author chendd
 *
 */
public class LayerGame extends Layer {

	public LayerGame(int locationX, int locationY, int winWidth, int winHeight) {
		super(locationX, locationY, winWidth, winHeight);
	}

	@Override
	public void paint(Graphics g) {
		super.drawWinow(g);//先画窗口

		int gameStatus = this.gameService.getGameContext().getGameStatus();
		boolean gameOver = gameStatus == EnumGameStatus.UN_START.getStatus();
		boolean isOver = this.gameService.getGameContext().isOver();
		if(gameOver && isOver == false) {
			return;
		}
		
		int typeCode = this.gameService.getGameContext().getTypeCode();
		Point points[] = this.gameService.getGameContext().getActPoint();
		//根据方块类型选择方块绘制的颜色
		int block = (typeCode + 1);//绘地图的格子
		if(gameOver){
			block = 8;
		}
		int start = block * 32 , end = start + 32;
		for (Point point : points) {
			g.drawImage(IMAGE_TEMPLATE, 
					super.locationX + point.x * 32 + BORDER, 
					super.locationY + point.y * 32 + BORDER, 
					super.locationX + point.x * 32 + 32 + BORDER, 
					super.locationY + point.y * 32 + 32  + BORDER, 
					start, 0, end, 32, null);
		}
			
		
		/**
		 * 打印阴影地图
		 */
		Point shadowPoints[] = this.gameService.getCanMoveDown();
		if(shadowPoints != null){
			g.setColor(new Color(255, 255, 0, 100));//设置矩形颜色已经透明度
			for (Point point : shadowPoints) {
				if(point == null){
					continue;
				}
				g.fillRect(point.x * 32 + BORDER, point.y * 32 + BORDER, 32, 32);
			}
		}
		
		//打印游戏地图，判断游戏是否结束，如果结束打印成渣渣
		boolean gameMap[][] = this.gameService.getGameContext().getGameMap();
		block = 0;//绘地图的格子
		if(gameOver){
			block = 8;
		}
		for (int x = 0; x < gameMap.length; x++) {
			for (int y = 0; y < gameMap[x].length; y++) {
				if(gameMap[x][y]){
					g.drawImage(IMAGE_TEMPLATE, 
							this.locationX + 32 * x + BORDER,
							this.locationY + 32 * y + BORDER,
							this.locationX + 32 * x + 32 + BORDER,
							this.locationY + 32 * y + 32 + BORDER,
							block * 32, 0, 32 + block * 32, 32, null);
				}
			}
		}
		//绘制的越在下，可越能显示在上层
		if(gameStatus == EnumGameStatus.PAUSE.getStatus()){
			//判断是否为暂停状态，如果为暂停则绘制暂停的图片
			super.drawNextImage(g, IMAGE_PAUSE);
		}
			
		
	}
	
}
