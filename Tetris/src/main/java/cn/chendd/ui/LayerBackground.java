package cn.chendd.ui;

import java.awt.Graphics;
import java.awt.Image;

import cn.chendd.common.MyImageIcon;
import cn.chendd.enums.EnumLevelSpeed;

/**
 * 按分数等级显示不同的背景图片
 * @author chendd
 *
 */
public class LayerBackground extends Layer {

	private static Image IMAGE_GB_TEMP[] = null;
	
	static{
		int lens = EnumLevelSpeed.values().length;
		IMAGE_GB_TEMP = new Image[lens];
		for(int i=0 ; i < lens ; i++){
			EnumLevelSpeed enumLevel = EnumLevelSpeed.values()[i];
			IMAGE_GB_TEMP[i] = new MyImageIcon("graphics/background/" + enumLevel.getBgName()).getImage();
		}
	}
	
	public LayerBackground(int locationX, int locationY, int winWidth,
			int winHeight) {
		super(locationX, locationY, winWidth, winHeight);
	}

	@Override
	public void paint(Graphics g) {
		//super.drawWinow(g);//无需显示边框
		int level = super.getGameService().getGameContext().getLevel();
		g.drawImage(IMAGE_GB_TEMP[level-1], 0, 0, null);
	}

}
