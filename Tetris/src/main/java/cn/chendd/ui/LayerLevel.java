package cn.chendd.ui;

import static cn.chendd.common.CommonConstants.BORDER;

import java.awt.Graphics;
import java.awt.Image;

import cn.chendd.common.MyImageIcon;

/**
 * 等级分数显示区域
 * @author chendd
 *
 */
public class LayerLevel extends Layer {

	Image levelImage = new MyImageIcon("graphics/string/level.png").getImage();//等级
	Image scoreImage = new MyImageIcon("graphics/string/score.png").getImage();//分数
	Image numImage = new MyImageIcon("graphics/string/num.png").getImage();//数字
	
	public LayerLevel(int locationX, int locationY, int winWidth, int winHeight) {
		super(locationX, locationY, winWidth, winHeight);
	}

	@Override
	public void paint(Graphics g) {
		super.drawWinow(g);
		//等级文本图
		g.drawImage(levelImage, this.locationX + 10 , this.locationY + BORDER + 15, null);
		//等级数值切片图
		int level = super.gameService.getGameContext().getLevel();
		int levelWidth = levelImage.getWidth(null) + 5;
		g.drawImage(numImage,
				super.locationX + BORDER + 0 + levelWidth, 
				super.locationY + BORDER - 5, 
				super.locationX + BORDER + 32 + levelWidth, 
				super.locationY + BORDER + 64 - 5, 
				level*32, 0, level*32 + 32, 64, null);
//		//分数文本
		g.drawImage(scoreImage, this.locationX + 10 , this.locationY + BORDER + 55, null);
		int score = super.gameService.getGameContext().getScore();
		super.drawNumberLevel(g, score, numImage , 75 , 45);
	}

}
