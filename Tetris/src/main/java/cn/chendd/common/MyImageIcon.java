package cn.chendd.common;

import javax.swing.ImageIcon;

/**
 * 打包时出现的文件路径找不见的问题，故全部替换成这种路径
 * @author chendd
 *
 */
public class MyImageIcon extends ImageIcon{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyImageIcon(String path){
		super(MyImageIcon.class.getResource("/" + path));
	}
	
}
