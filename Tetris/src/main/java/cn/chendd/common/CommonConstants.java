package cn.chendd.common;

import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

/**
 * 公共的字段定义
 * @author chendd
 *
 */
public class CommonConstants {

	/**
	 * 绘制区域的边框
	 */
	public static final Image WINDOW_IMAGE = new MyImageIcon("graphics/window/Window.png").getImage();
	/**
	 * 绘制原图片的宽度
	 */
	public static final int WINDOW_WIDTH = WINDOW_IMAGE.getWidth(null);
	/**
	 * 绘制原图片的高度
	 */
	public static final int WINDOW_HEIGHT = WINDOW_IMAGE.getHeight(null);
	/**
	 * 
	 */
	public static final int PADDING = 5;
	/**
	 * 绘制原图的边框厚度
	 */
	public static final int BORDER = 6;
	/**
	 * 开始按钮
	 */
	public static final ImageIcon START_BUTTON = new MyImageIcon("graphics/string/start.png");
	/**
	 * 暂停按钮
	 */
	public static final ImageIcon PAUSE_BUTTON = new MyImageIcon("graphics/string/pause.png");
	/**
	 * 继续按钮
	 */
	public static final ImageIcon CONTINUE_BUTTON = new MyImageIcon("graphics/string/continue.png");
	/**
	 * 存储方块的模型以及坐标
	 */
	public static final List<Point[]> TYPE_CONFIG;
	
	static{
		TYPE_CONFIG = new ArrayList<Point[]>();
		TYPE_CONFIG.add(new Point[]{new Point(4,0),new Point(3,0),new Point(5,0),new Point(6,0)});
		TYPE_CONFIG.add(new Point[]{new Point(4,0),new Point(3,0),new Point(5,0),new Point(4,1)});
		TYPE_CONFIG.add(new Point[]{new Point(4,0),new Point(3,0),new Point(5,0),new Point(3,1)});
		TYPE_CONFIG.add(new Point[]{new Point(4,0),new Point(5,0),new Point(3,1),new Point(4,1)});
		TYPE_CONFIG.add(new Point[]{new Point(4,0),new Point(5,0),new Point(4,1),new Point(5,1)});
		TYPE_CONFIG.add(new Point[]{new Point(4,0),new Point(3,0),new Point(5,0),new Point(5,1)});
		TYPE_CONFIG.add(new Point[]{new Point(4,0),new Point(3,0),new Point(4,1),new Point(5,1)});
	}
	/**
	 * 方块的绘制模板
	 */
	public static Image IMAGE_TEMPLATE = new MyImageIcon("graphics/game/rect.png").getImage();
	/**
	 * 游戏主面板的大暂停图片
	 */
	public static Image IMAGE_PAUSE = new MyImageIcon("graphics/game/pause.png").getImage();
	/**
	 * 地图的最左端坐标
	 */
	public static final int MIN_X = 0;
	/**
	 * 地图的最又端坐标
	 */
	public static final int MAX_X = 9;
	/**
	 * 地图的最上端坐标
	 */
	public static final int MIN_Y = 0;
	/**
	 * 地图的最下端坐标
	 */
	public static final int MAX_Y = 17;
}
