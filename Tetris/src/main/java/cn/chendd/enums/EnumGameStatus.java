package cn.chendd.enums;

/**
 * 游戏状态类型枚举
 * @author chendd
 *
 */
public enum EnumGameStatus {

	/**
	 * 未开始,或者已结束
	 */
	UN_START(1),
	/**
	 * 运行中
	 */
	RUNING(2),
	/**
	 * 暂停中
	 */
	PAUSE(3),
	;
	
	private int status;
	
	private EnumGameStatus(int status){
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
	
}
