package cn.chendd.enums;

/**
 * 消行的分数运算，根据消除的行数获取行对应的分数
 * @author chendd
 *
 */
public enum EnumLineScore {

	/**
	 * 消除一行时的分数
	 */
	ONE(1 , 20),
	TWO(2 , 50),
	THREE(3 , 100),
	FORE(4 , 200),
	;
	/**
	 * 消行数量、对应的分数
	 */
	private int lines , score;
	
	private EnumLineScore(int lines , int score){
		this.lines = lines;
		this.score = score;
	}

	/**
	 * 根据消除的行数获取分数
	 * @param line 消行数量
	 * @return 对应的分数
	 */
	public static int getLineScoreByLines(int line){
		int score = 0;
		EnumLineScore lineScore = null;
		EnumLineScore scores[] = EnumLineScore.values();
		for (EnumLineScore enumLineScore : scores) {
			if(enumLineScore.getLines() == line){
				lineScore = enumLineScore;
				break;
			}
		}
		if(lineScore != null){
			score = lineScore.getScore();
		}
		return score;
	}
	
	public int getLines() {
		return lines;
	}

	public int getScore() {
		return score;
	}
	
}
