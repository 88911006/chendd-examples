package cn.chendd.enums;

/**
 * 等级的下落速度,等级越高速度越小
 * @author chendd
 *
 */
public enum EnumLevelSpeed {

	ONE(1, 900L , "bg01.jpg"),
	TWO(2, 800L , "bg02.jpg"),
	THREE(3 , 700L , "bg03.jpg"),
	FORE(4 , 600L , "bg04.jpg"),
	FIVE(5 , 500L , "bg05.jpg"),
	SIX(6 , 400L , "bg06.jpg"),
	SEVEN(7 , 300L , "bg07.jpg");
	
	/**
	 * 等级
	 */
	private Integer level;
	/**
	 * 自动下落的延迟时间
	 */
	private Long sleep;
	/**
	 * 背景图片
	 */
	private String bgName;
	
	private EnumLevelSpeed(Integer level , Long sleep , String bgName){
		this.level = level;
		this.sleep = sleep;
		this.bgName = bgName;
	}

	/**
	 * 根据等级获取等级相应的系列参数
	 * @param level 等级
	 * @return 等级相应的数据
	 */
	public static EnumLevelSpeed getEnumDataByLevel(Integer level){
		EnumLevelSpeed defaultEnum = EnumLevelSpeed.SEVEN;
		EnumLevelSpeed values[] = EnumLevelSpeed.values();
		for (EnumLevelSpeed enumLevelSpeed : values) {
			if(enumLevelSpeed.getLevel().equals(level)){
				defaultEnum = enumLevelSpeed;
				break;
			}
		}
		return defaultEnum;
	}
	
	public Integer getLevel() {
		return level;
	}

	public Long getSleep() {
		return sleep;
	}

	public String getBgName() {
		return bgName;
	}
	
}
