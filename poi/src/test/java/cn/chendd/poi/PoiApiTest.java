package cn.chendd.poi;

import cn.chendd.poi.style.ExcelStyle;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Poi常用Api测试
 *
 * @author chendd
 * @date 2022/6/26 17:10
 */
@RunWith(JUnit4.class)
public class PoiApiTest {

    @Test
    public void createExcel2003() throws IOException {
        try (Workbook workbook2003 = new HSSFWorkbook();
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream();) {
            workbook2003.createSheet("Excel2003");
            workbook2003.write(outputStream);
            System.out.println(Arrays.toString(outputStream.toByteArray()));
        }
    }

    @Test
    public void createExcel2007() throws IOException {
        File file = File.createTempFile("Excel", ".xlsx");
        try (Workbook workbook2007 = new XSSFWorkbook();
             FileOutputStream outputStream = new FileOutputStream(file)) {
            Sheet sheet = workbook2007.createSheet("Excel2007");
            Row row = sheet.createRow(0);
            Cell cell = row.createCell(0);
            cell.setCellValue("hello");
            workbook2007.write(outputStream);
            System.out.println(file.getAbsolutePath());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            file.delete();
        }
    }

    @Test
    public void createExcel() throws IOException {
        File file = File.createTempFile("Excel", ".xlsx");
        try (Workbook workbook = new SXSSFWorkbook();
             FileOutputStream outputStream = new FileOutputStream(file)) {

            Sheet sheet = workbook.createSheet("Excel");
            //设置3列2行的冻结
            sheet.createFreezePane(3 , 2);
            //是否显示网格
            sheet.setDisplayGridlines(false);
            //是否打印网格
            sheet.setPrintGridlines(false);
            //设置纸张方向和纸张大小
            PrintSetup setup = sheet.getPrintSetup();
            setup.setLandscape(true);
            setup.setPaperSize(PrintSetup.A4_PAPERSIZE);
            //设置默认的行高
            sheet.setDefaultRowHeightInPoints(26);
            //设置列宽
            this.setColumnWidth(sheet);
            //填充标题行
            this.fillTitleRow(workbook , sheet , 0);
            //填充表头行
            this.fillHeaderRow(workbook , sheet , 1);
            //填充数据行
            this.fillDataRow(workbook , sheet , 2 , 1_000 * 50);
            //设置下拉框
            this.addSexDropdown(sheet , 2 ,1000 + 1 , 2 ,  2);
            workbook.write(outputStream);
            System.out.println(file.getAbsolutePath());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            file.delete();
        }
    }

    /**
     * 设置下拉框
     * @param sheet sheet
     * @param firstRow 开始行
     * @param lastRow 结束行
     * @param firstCol 开始列
     * @param lastCol 结束列
     */
    private void addSexDropdown(Sheet sheet, int firstRow, int lastRow, int firstCol, int lastCol) {
        DataValidationHelper validationHelper = sheet.getDataValidationHelper();
        DataValidationConstraint dataValidationConstraint = validationHelper.createExplicitListConstraint(new String[]{"男", "女"});
        CellRangeAddressList rangeList = new CellRangeAddressList(firstRow , lastRow , firstCol , lastCol);
        DataValidation dataValidation = validationHelper.createValidation(dataValidationConstraint, rangeList);
        sheet.addValidationData(dataValidation);
    }

    /**
     * 填充数据
     * @param workbook 工作簿
     * @param sheet sheet
     * @param index 索引行
     * @param number 行数
     */
    private void fillDataRow(Workbook workbook, Sheet sheet, int index, int number) {
        CellStyle leftStyle = ExcelStyle.createDataLeftStyle(workbook);
        CellStyle centerStyle = ExcelStyle.createDataCenterStyle(workbook);
        CellStyle amountStyle = ExcelStyle.createDataAmountStyle(workbook);
        CellStyle amountRedStyle = ExcelStyle.createDataAmountRedStyle(workbook);
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0 ; i < number ; i++) {
            Row row = sheet.createRow(i + index);
            //编号列
            Cell numCell = row.createCell(0);
            numCell.setCellStyle(centerStyle);
            numCell.setCellValue(i + 1);
            //姓名列
            Cell nameCell = row.createCell(1);
            nameCell.setCellStyle(leftStyle);
            nameCell.setCellValue(i % 2 == 0 ? "奕廷" : "梓瑜");
            //性别列
            Cell sexCell = row.createCell(2);
            sexCell.setCellStyle(centerStyle);
            sexCell.setCellValue(i % 2 == 0 ? "男" : "女");
            //出生日期
            Cell birthdayCell = row.createCell(3);
            birthdayCell.setCellStyle(centerStyle);
            birthdayCell.setCellValue(i % 2 == 0 ? "2021-07-09" : "2016-11-10");
            //零花钱
            Cell amountCell = row.createCell(4);
            Double amount = random.nextDouble(5000);
            if (amount.compareTo(1000.0D) < 0) {
                amountCell.setCellStyle(amountRedStyle);
            } else {
                amountCell.setCellStyle(amountStyle);
            }
            amountCell.setCellValue(amount);
            //联系地址
            Cell addressCell = row.createCell(5);
            addressCell.setCellStyle(leftStyle);
            addressCell.setCellValue(i % 2 == 0 ? "湖北省武汉市江夏区" : "湖北省襄阳市老河口市");
            if (i % 500 == 0) {
                try {
                    ((SXSSFSheet) sheet).flushRows();
                } catch (IOException ignore) {}
            }
        }
        
    }

    /**
     * 填充表头行
     * @param sheet sheet
     * @param index 索引行
     */
    private void fillHeaderRow(Workbook workbook , Sheet sheet, int index) {
        String[] headers = {
            "序号" , "姓名" , "性别" , "出生日期" , "零花钱（元）" , "联系地址"
        };
        Row row = sheet.createRow(index);
        CellStyle cellStyle = ExcelStyle.createHeaderStyle(workbook);
        for (int i = 0; i < headers.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(headers[i]);
            cell.setCellStyle(cellStyle);
        }
    }

    /**
     * 填充标题行
     * @param workbook workbook
     * @param sheet sheet
     * @param index 索引行
     */
    private void fillTitleRow(Workbook workbook , Sheet sheet, int index) {
        Row row = sheet.createRow(index);
        Cell cell = row.createCell(0);
        cell.setCellStyle(ExcelStyle.createTitleStyle(workbook));
        cell.setCellValue("用户信息表");
        //设置单元格合并
        sheet.addMergedRegion(new CellRangeAddress(index , index , 0 , 5));
        //设置超链接，点击连接至Sheet页名称为Excel的第A列第100行
        Hyperlink hyperlink = workbook.getCreationHelper().createHyperlink(HyperlinkType.DOCUMENT);
        hyperlink.setAddress("'Excel'!A100");
        cell.setHyperlink(hyperlink);
    }

    /**
     * 设置sheet页的列宽
     * @param sheet sheet
     */
    private void setColumnWidth(Sheet sheet) {
        int[][] columns = new int[][] {
                {0 , 256 * 8},
                {1 , 256 * 15},
                {2 , 256 * 15},
                {3 , 256 * 20},
                {4 , 256 * 25},
                {5 , 256 * 50}
        };
        for (int[] column : columns) {
            sheet.setColumnWidth(column[0] , column[1]);
        }
    }

}
