package cn.chendd.poi.style;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;

/**
 * Excel单元格样式
 *
 * @author chendd
 * @date 2022/6/26 21:40
 */
public final class ExcelStyle {

    /**
     * 设置标题样式
     *
     * @param workbook 工作簿
     * @return 样式
     */
    public static CellStyle createTitleStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.CENTER);
        Font font = workbook.createFont();
        font.setFontName("simSun");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        style.setFont(font);
        return style;
    }

    /**
     * 设置表头样式
     *
     * @param workbook 工作簿
     * @return 样式
     */
    public static CellStyle createHeaderStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.CENTER);
        Font font = workbook.createFont();
        font.setFontName("simSun");
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.BLUE.getIndex());
        font.setUnderline(Font.U_SINGLE);
        font.setBold(true);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setFont(font);
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return style;
    }

    /**
     * 创建数据居左的单元格样式
     *
     * @param workbook 工作簿
     * @return 样式
     */
    public static CellStyle createDataLeftStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.LEFT);
        Font font = workbook.createFont();
        font.setFontName("simSun");
        font.setFontHeightInPoints((short) 10);
        font.setBold(false);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setFont(font);
        return style;
    }

    /**
     * 创建数据居中的单元格样式
     *
     * @param workbook 工作簿
     * @return 样式
     */
    public static CellStyle createDataCenterStyle(Workbook workbook) {
        CellStyle style = createDataLeftStyle(workbook);
        style.setAlignment(HorizontalAlignment.CENTER);
        return style;
    }

    /**
     * 创建数据居右的单元格样式
     *
     * @param workbook 工作簿
     * @return 样式
     */
    public static CellStyle createDataRightStyle(Workbook workbook) {
        CellStyle style = createDataLeftStyle(workbook);
        style.setAlignment(HorizontalAlignment.RIGHT);
        return style;
    }

    /**
     * 金额格式化千分位
     * @param workbook 工作簿
     * @return 样式
     */
    public static CellStyle createDataAmountStyle(Workbook workbook) {
        CellStyle style = createDataRightStyle(workbook);
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
        return style;
    }

    /**
     * 金额格式化千分位，显示红色文本
     * @param workbook 工作簿
     * @return 样式
     */
    public static CellStyle createDataAmountRedStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.RIGHT);
        Font font = workbook.createFont();
        font.setColor(IndexedColors.RED.getIndex());
        font.setFontName("simSun");
        font.setFontHeightInPoints((short) 10);
        font.setBold(false);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setFont(font);
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
        return style;
    }

}
