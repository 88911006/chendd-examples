package cn.chendd.jedis.examples;

import org.junit.Before;
import org.junit.Test;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.Set;

/**
 * 简单测试一下
 * @author chendd
 * @net http://www.chendd.cn
 */
public class TestSessionID {

    Jedis jedis = null;
    String ID = "13604382-4592-4a84-913a-87643f2eb854";

    @Before
    public void init(){
        jedis = new Jedis("192.168.229.128" , 6379);
        jedis.select(5);//切换数据库
    }

    //测试获得某个session ID的所有key
    @Test
    public void testSessionIdKeys() throws Exception {
        String sessionId = "spring:session:sessions:" + ID;
        Boolean exists = jedis.exists(sessionId);
        if(! exists){
            return;
        }
        String type = jedis.type(sessionId);
        System.out.println("sessionId的数值类型为：" + type);
        Map<String, String> sessions = jedis.hgetAll(sessionId);
        System.out.println("sessionId所有key为：" + sessions.keySet());
        Set<Map.Entry<String , String>> entrySet = sessions.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            System.out.println(entry.getKey() + "--" + entry.getValue());
        }
    }

    //测试某个session ID的所有数据
    @Test
    public void testSessionAllValue(){
        String sessionId = "spring:session:sessions:" + ID;
        Map<byte[] , byte[]> map = jedis.hgetAll(sessionId.getBytes());
        JdkSerializationRedisSerializer jdk = new JdkSerializationRedisSerializer();
        for (Map.Entry<byte[], byte[]> entry : map.entrySet()) {
            Object value = jdk.deserialize(entry.getValue());
            System.out.println(value.getClass() + "---" + new String(entry.getKey()) + "---" + value);
        }
    }

    //测试反序列化某个session ID的所有数据及类型，循环获取数据方式
    @Test
    public void testSessionIdJDKSerializer() throws Exception {
        String sessionId = "spring:session:sessions:" + ID;
        Map<String, String> sessions = jedis.hgetAll(sessionId);
        Set<String> entrySet = sessions.keySet();
        JdkSerializationRedisSerializer jdk = new JdkSerializationRedisSerializer();
        for (String key : entrySet) {
            byte bytes[] = jedis.hget(sessionId.getBytes() , key.getBytes());
            Object value = jdk.deserialize(bytes);
            System.out.println(value.getClass() + "--" + key + "--" + value);
        }
    }

    //测试session ID Expires
    @Test
    public void testSessionIdExpires(){
        String sessionIdExpires = "spring:session:sessions:expires:" + ID;
        String type = jedis.type(sessionIdExpires);
        System.out.println("sessionId的数值类型为：" + type);
        String expires = jedis.get(sessionIdExpires);
        System.out.println("ssessionIdExpires的值为：" + expires);
    }

    @Test
    public void testSessionExpirations(){
        String sessionIdExpires = "spring:session:expirations:1558590480000";
        String type = jedis.type(sessionIdExpires);
        System.out.println("Expirations的数值类型为：" + type);
        String expires = jedis.get(sessionIdExpires);
        System.out.println("Expirations的值为：" + expires);

    }

}
