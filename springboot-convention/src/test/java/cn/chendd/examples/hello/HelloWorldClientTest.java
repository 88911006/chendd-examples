package cn.chendd.examples.hello;

import cn.chendd.comonents.SpringBeanFactory;
import cn.chendd.examples.BootstrapTest;
import cn.chendd.examples.hello.clients.HelloWorldClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * 测试接口注入类
 *
 * @author chendd
 */
public class HelloWorldClientTest extends BootstrapTest {

    @Autowired
    private HelloWorldClient helloWorldClient1;
    @Resource
    private HelloWorldClient helloWorldClient2;
    private HelloWorldClient helloWorldClient3;

    @Before
    public void init() {
        helloWorldClient3 = SpringBeanFactory.getBean(HelloWorldClient.class);
    }

    @Test
    public void context() {
        Assert.assertEquals(helloWorldClient1, helloWorldClient2);
        Assert.assertEquals(helloWorldClient1, helloWorldClient3);
        System.out.println("调用无参数方法");
        System.out.println(helloWorldClient1.sayHello());
        System.out.println("调用1个参数方法");
        System.out.println(helloWorldClient1.sayHello("chendd"));
        System.out.println("调用2个参数方法");
        System.out.println(helloWorldClient1.sayHello("chendd" , Math.PI));
    }


}
