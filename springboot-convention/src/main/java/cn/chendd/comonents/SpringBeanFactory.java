package cn.chendd.comonents;

import lombok.Getter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * SpringBean组件获取工具类
 *
 * @author chendd
 */
@Component
public class SpringBeanFactory implements ApplicationContextAware {

    @Getter
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringBeanFactory.applicationContext = applicationContext;
    }

    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    public static <T> T getBean(String beanName , Class<T> clazz){
        return applicationContext.getBean(beanName , clazz);
    }

    public static <T> Map<String , T> getBeans(Class<T> clazz){
        return applicationContext.getBeansOfType(clazz);
    }

}
