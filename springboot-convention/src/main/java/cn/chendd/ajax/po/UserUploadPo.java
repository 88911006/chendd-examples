package cn.chendd.ajax.po;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
/**
 * 用户文件上传参数对象
 *
 * @author chendd
 * @date 2024/7/21 14:09
 */
@Data
public class UserUploadPo {

    private String name;

    private String website;

    private List<String> hobbies;

    List<MultipartFile> files;
}
