package cn.chendd.ajax.controller;

import cn.chendd.ajax.bo.UserUploadBo;
import cn.chendd.ajax.po.UserUploadPo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * ajax文件上传Controller接口定义
 *
 * @author chendd
 * @date 2024/7/21 14:06
 */
@RestController
@RequestMapping("/ajax/upload")
public class AjaxUploadController {

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public UserUploadBo upload(UserUploadPo param) {
        return new UserUploadBo(param);
    }

}
