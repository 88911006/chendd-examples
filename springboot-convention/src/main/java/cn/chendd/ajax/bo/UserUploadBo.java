package cn.chendd.ajax.bo;

import cn.chendd.ajax.po.UserUploadPo;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户上传文件数据对象
 *
 * @author chendd
 * @date 2024/7/21 14:38
 */
@Data
public class UserUploadBo {

    private String name;

    private String website;

    private List<String> hobbies;

    private List<FileInfo> files;

    public UserUploadBo(UserUploadPo param) {
        this.name = param.getName();
        this.website = param.getWebsite();
        this.hobbies = param.getHobbies();
        this.files = param.getFiles().stream().map(file -> {
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileName(file.getOriginalFilename());
            fileInfo.setSize(file.getSize());
            return fileInfo;
        }).collect(Collectors.toList());
    }

    @Data
    public static class FileInfo {

        private String fileName;

        private long size;

    }

}
