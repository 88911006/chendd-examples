package cn.chendd.examples.hello.clients;

import cn.chendd.helloclient.HelloClient;

/**
 * 测试类
 *
 * @author chendd
 */
@HelloClient(value = "HelloWorldClient")
public interface HelloWorldClient {

    String sayHello();

    String sayHello(String name);

    String sayHello(String name , Double random);

}
