package cn.chendd;

import cn.chendd.helloclient.EnableHelloClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author chendd
 */
@SpringBootApplication
@EnableHelloClient(basePackages = {"cn.chendd.**.clients"})
public class Bootstrap {

    public static void main(String[] args) {
        SpringApplication.run(Bootstrap.class , args);
    }

}
