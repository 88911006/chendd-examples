package cn.chendd.convention.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * HTML便捷访问Controller
 *
 * @author chendd
 * @date 2024/7/21 13:26
 */
@Controller
public class HtmlAccessController {

    @RequestMapping(value = "/ajax/*.html", method = RequestMethod.GET)
    public String ajaxHtmlPage(HttpServletRequest request) {

        return StringUtils.substringBeforeLast(request.getRequestURI() , ".");
    }

}
