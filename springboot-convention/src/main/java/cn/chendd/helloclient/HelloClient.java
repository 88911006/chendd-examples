package cn.chendd.helloclient;

import java.lang.annotation.*;

/**
 * HelloClient
 *
 * @author chendd
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HelloClient {

    String value();

}
