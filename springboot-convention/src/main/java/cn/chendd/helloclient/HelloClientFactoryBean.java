package cn.chendd.helloclient;

import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Proxy;

/**
 * 组件创建工厂
 *
 * @author chendd
 */
public class HelloClientFactoryBean implements FactoryBean<Object> {


    private Class<?> interfaceClass;

    /**
     * 构造函数
     */
    public HelloClientFactoryBean() {
        super();
    }

    /**
     * 构造函数
     * @param beanClassName 构造函数
     */
    public HelloClientFactoryBean(String beanClassName) {
        try {
            interfaceClass = Class.forName(beanClassName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 构造函数
     * @param params 构造函数
     */
    public HelloClientFactoryBean(Object[] params) {

    }

    /**
     * 构造函数
     * @param interfaceClass 实现
     */
    public HelloClientFactoryBean(Class<?> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public Object getObject() {
        HelloProxyHandler proxyHandler = new HelloProxyHandler(interfaceClass);
        return Proxy.newProxyInstance(ClassLoader.getSystemClassLoader() , new Class[]{interfaceClass} , proxyHandler);
    }

    @Override
    public Class<?> getObjectType() {
        return interfaceClass;
    }

}
