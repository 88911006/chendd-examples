package cn.chendd.helloclient;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.type.AnnotationMetadata;

import java.lang.annotation.Annotation;

/**
 * HelloClientRepositoryConfigurationSourceSupport
 *
 * @author chendd
 */
public class HelloClientRepositoryConfigurationSourceSupport implements ImportBeanDefinitionRegistrar {

    @SuppressWarnings("all")
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        final MergedAnnotations annotations = importingClassMetadata.getAnnotations();
        String[] basePackages = null;
        for (MergedAnnotation<Annotation> annotation : annotations) {
            if (EnableHelloClient.class.equals(annotation.getType())) {
                final Object source = annotation.getSource();
                assert source != null;
                final EnableHelloClient httpFeignAnnotation = (EnableHelloClient) ((Class) source).getAnnotation(EnableHelloClient.class);
                basePackages = httpFeignAnnotation.basePackages();
            }
        }
        HelloClientScan scan = new HelloClientScan(registry);
        assert basePackages != null;
        scan.doScan(basePackages);
    }

}
