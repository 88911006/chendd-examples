package cn.chendd.helloclient;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 注解参数类
 *
 * @author chendd
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(HelloClientRepositoryConfigurationSourceSupport.class)
public @interface EnableHelloClient {

    String[] basePackages() default {};

}
