package cn.chendd.feign.httpclient;

import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;

/**
 * @author chendd
 */
public class HttpFeignFormError implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        return FeignException.errorStatus(methodKey , response);
    }
}
