package cn.chendd.feign.httpclient;

import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.StandardHttpRequestRetryHandler;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.util.concurrent.TimeUnit;

/**
 * @author chendd
 */
public class HttpClientConfig {

    private static CloseableHttpClient httpClient;

    public static CloseableHttpClient getHttpClient() {
        if (httpClient == null) {
            synchronized (CloseableHttpClient.class) {
                if (httpClient == null) {
                    // 配置连接池
                    httpClient = HttpClients.custom()
                            .setConnectionManager(new PoolingHttpClientConnectionManager())
                            .setMaxConnTotal(20)
                            .setRetryHandler(new StandardHttpRequestRetryHandler(3 , true))
                            .setConnectionTimeToLive(10 , TimeUnit.SECONDS)
                            .setDefaultConnectionConfig(ConnectionConfig.DEFAULT)
                            .setDefaultSocketConfig(SocketConfig.DEFAULT)
                            .build();
                }
            }
        }
        return httpClient;
    }

}
