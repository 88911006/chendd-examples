package cn.chendd.feign.httpclient;


import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

import java.util.concurrent.TimeUnit;

/**
 * @author chendd
 */
public class OkHttpClientConfig {

    private static OkHttpClient httpClient;

    public static OkHttpClient getHttpClient() {
        if (httpClient == null) {
            synchronized (OkHttpClientConfig.class) {
                if (httpClient == null) {
                    // 配置连接池
                    httpClient = new OkHttpClient.Builder()
                            .connectionPool(new ConnectionPool(20 , 2 , TimeUnit.MINUTES))
                            .connectTimeout(10 , TimeUnit.SECONDS)
                            .readTimeout(300 , TimeUnit.SECONDS)
                            .writeTimeout(600 , TimeUnit.SECONDS)
                            .retryOnConnectionFailure(true)
                            .build();

                }
            }
        }
        return httpClient;
    }

}
