package cn.chendd.feign.httpclient;

import java.lang.annotation.*;

/**
 * @author chendd
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface FeignUploadFile {


}
