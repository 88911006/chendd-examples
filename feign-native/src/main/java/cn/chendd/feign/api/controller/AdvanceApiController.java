package cn.chendd.feign.api.controller;

import cn.chendd.feign.api.po.UploadParam;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 简单Api测试
 *
 * @author chendd
 */
@RestController
@RequestMapping(value = "/api/advance" , consumes = MediaType.ALL_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
public class AdvanceApiController {

    @GetMapping(value = "/download" , produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void download(HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION , ContentDisposition.attachment().filename("AdvanceApiController.class").build().toString());
        try (ServletOutputStream outputStream = response.getOutputStream();
             InputStream inputStream = getClass().getResourceAsStream("AdvanceApiController.class")){
            StreamUtils.copy(inputStream , outputStream);
        }
    }

    @PostMapping(value = "/upload_param" , consumes = MediaType.MULTIPART_FORM_DATA_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String , Object> uploadParam(@RequestPart("files") List<MultipartFile> files ,
                                       @RequestParam Integer id,
                                       @RequestParam String name,
                                       @RequestParam List<String> nikeNames
                                       ) {
        Map<String , Object> paramMap = new HashMap<>(16);
        paramMap.put("id" , id);
        paramMap.put("name" , name);
        paramMap.put("nikeNames" , nikeNames);
        if (files != null && !files.isEmpty()) {
            paramMap.put("files" , files.stream().map(MultipartFile::getOriginalFilename).collect(Collectors.toList()));
        }
        return paramMap;
    }

    @PostMapping(value = "/upload_body" , consumes = MediaType.MULTIPART_FORM_DATA_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String , Object> uploadBody(UploadParam param) {
        Map<String , Object> paramMap = new HashMap<>(16);
        paramMap.put("id" , param.getId());
        paramMap.put("name" , param.getName());
        paramMap.put("nikeNames" , param.getNikeNames());
        if (param.getFiles() != null && !param.getFiles().isEmpty()) {
            paramMap.put("files" , param.getFiles().stream().map(MultipartFile::getOriginalFilename).collect(Collectors.toList()));
        }
        return paramMap;
    }

}
