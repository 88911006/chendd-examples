package cn.chendd.feign.api.controller;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;

/**
 * 简单Api测试
 *
 * @author chendd
 */
@RestController
@RequestMapping(value = "/api/simple" , consumes = MediaType.ALL_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
public class SimpleApiController {

    @GetMapping(value = "/hello")
    public String hello() {
        return "world";
    }

    @GetMapping(value = "/hello/{name}")
    public String hello(@PathVariable String name) {
        return "hello：" + name;
    }

    @PostMapping(value = "/hello/{id}")
    public Triple<String , String , String> hello(@PathVariable String id ,
                                                  @RequestParam("name") String name ,
                                                  String website) {
        return Triple.of(id , name , website);
    }

    @PutMapping(value = "/hello")
    public Point hello(@RequestBody Point param) {
        return param;
    }

}
