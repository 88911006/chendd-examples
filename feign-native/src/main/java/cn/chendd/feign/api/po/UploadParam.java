package cn.chendd.feign.api.po;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author chendd
 */
@Data
public class UploadParam {

    private Integer id;

    private String name;

    private List<String> nikeNames;

    private List<MultipartFile> files;
}
