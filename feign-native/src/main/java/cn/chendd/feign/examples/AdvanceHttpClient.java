package cn.chendd.feign.examples;

import cn.chendd.feign.api.po.UploadParam;
import cn.chendd.feign.httpclient.FeignUploadFile;
import feign.Response;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 简单的接口调用
 *
 * @author chendd
 */
public interface AdvanceHttpClient {


    /**
     * 下载yaml文本文件
     * @return response
     */
    @GetMapping(value = "/api/advance/download" , produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    Response downloadFile();

    /**
     * 多文件上传（附带其它参数）
     * @param files 多文件
     * @param id 参数对象
     * @param name 参数对象
     * @param nikeNames 参数对象
     * @return 上传结果
     */
    @PostMapping(value = "/api/advance/upload_param" , consumes = MediaType.MULTIPART_FORM_DATA_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    @FeignUploadFile
    Map<String , Object> uploadParam(@RequestPart(value = "files") List<MultipartFile> files ,
                                @RequestParam(name = "id") Integer id,
                                @RequestParam(name = "name") String name,
                                @RequestParam(name = "nikeNames") List<String> nikeNames);

    /**
     * 多文件上传（附带其它参数）
     * @param param 参数对象
     * @return 上传结果
     */
    @PostMapping(value = "/api/advance/upload_body" , consumes = MediaType.MULTIPART_FORM_DATA_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    @FeignUploadFile
    Map<String , Object> uploadBody(@RequestPart UploadParam param);

}
