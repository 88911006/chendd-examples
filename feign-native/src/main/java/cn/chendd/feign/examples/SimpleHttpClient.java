package cn.chendd.feign.examples;

import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;

/**
 * 简单的接口调用
 *
 * @author chendd
 */
public interface SimpleHttpClient {

    /**
     * 无参数
     */
    @GetMapping(value = "/api/simple/hello" , produces = MediaType.APPLICATION_JSON_VALUE)
    String sayHello();

    /**
     * 1个参数
     */
    @GetMapping(value = "/api/simple/hello/{name}")
    String sayHello(@PathVariable("name") String name);

    /**
     * 3个参数【拼接在URL上传递的参数】
     */
    @PostMapping(value = "/api/simple/hello/{id}")
    String sayHello(@PathVariable("id") String id , @RequestParam("name") String name , @RequestParam("website") String website);

    /**
     * 3个参数【拼接在URL上传递的参数】
     */
    @PutMapping(value = "/api/simple/hello")
    String sayHello(@RequestBody Point param);

}
