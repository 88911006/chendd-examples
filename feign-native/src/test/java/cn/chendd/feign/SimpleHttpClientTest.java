package cn.chendd.feign;

import cn.chendd.feign.examples.SimpleHttpClient;
import cn.chendd.feign.httpclient.HttpClientConfig;
import cn.chendd.feign.httpclient.HttpFeignFormDecoder;
import cn.chendd.feign.httpclient.HttpFeignFormEncoder;
import cn.chendd.feign.httpclient.HttpFeignFormError;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.Logger;
import feign.httpclient.ApacheHttpClient;
import feign.slf4j.Slf4jLogger;
import feign.spring.SpringContract;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;

/**
 * 接口测试【简单】
 *
 * @author chendd
 */
@RunWith(JUnit4.class)
public class SimpleHttpClientTest {

    private SimpleHttpClient httpClient;

    @Before
    public void init() {
        ObjectMapper objectMapper = new ObjectMapper();
        final Feign.Builder builder = new Feign.Builder()
                .contract(new SpringContract())
                //.client(new OkHttpClient(OkHttpClientConfig.getHttpClient()))
                .client(new ApacheHttpClient(HttpClientConfig.getHttpClient()))
                .encoder(new HttpFeignFormEncoder(objectMapper))
                .decoder(new HttpFeignFormDecoder(objectMapper))
                .errorDecoder(new HttpFeignFormError())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL);
        httpClient = builder.target(SimpleHttpClient.class, "http://127.0.0.1:8080");
    }

    @Test
    public void hello0ArgsTest() {
        System.out.println("===调用无参数返回String接口===");
        httpClient.sayHello();
    }

    @Test
    public void hello1ArgsTest() {
        System.out.println("===调用@PathVariable参数返回String接口===");
        httpClient.sayHello("chendd");
    }

    @Test
    public void hello3ArgsTest() {
        System.out.println("===调用@PathVariable、@RequestParam===");
        httpClient.sayHello("88911" , "陈冬冬" , "https://www.chendd.cn");
    }

    @Test
    public void helloBodyArgsTest() {
        System.out.println("===调用@RequestBody接口===");
        httpClient.sayHello(new Point(88911 , 36));
    }

}
