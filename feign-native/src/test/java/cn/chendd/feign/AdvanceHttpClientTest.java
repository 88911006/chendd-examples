package cn.chendd.feign;

import cn.chendd.feign.api.po.UploadParam;
import cn.chendd.feign.examples.AdvanceHttpClient;
import cn.chendd.feign.httpclient.HttpFeignFormDecoder;
import cn.chendd.feign.httpclient.HttpFeignFormEncoder;
import cn.chendd.feign.httpclient.HttpFeignFormError;
import cn.chendd.feign.httpclient.OkHttpClientConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import feign.Feign;
import feign.Logger;
import feign.Response;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import feign.spring.SpringContract;
import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 接口测试【进阶】
 *
 * @author chendd
 */
@RunWith(JUnit4.class)
public class AdvanceHttpClientTest {

    private AdvanceHttpClient httpClient;

    @Before
    public void init() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        final Feign.Builder builder = new Feign.Builder()
                .contract(new SpringContract())
                .client(new OkHttpClient(OkHttpClientConfig.getHttpClient()))
                .encoder(new HttpFeignFormEncoder(objectMapper))
                .decoder(new HttpFeignFormDecoder(objectMapper))
                .errorDecoder(new HttpFeignFormError())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL);
        httpClient = builder.target(AdvanceHttpClient.class, "http://127.0.0.1:8080");
    }

    @Test
    public void downloadFile() {
        try (Response response = this.httpClient.downloadFile()) {
            final Collection<String> contentDisposition = response.headers().getOrDefault(HttpHeaders.CONTENT_DISPOSITION, null);
            AtomicReference<File> fileReference = new AtomicReference<>();
            if (contentDisposition != null && !contentDisposition.isEmpty()) {
                File tempFolder = FileUtils.getTempDirectory();
                contentDisposition.forEach(item -> {
                    String fileName = ContentDisposition.parse(item).getFilename();
                    fileReference.set(new File(tempFolder, fileName));
                });
            }
            try (InputStream inputStream = response.body().asInputStream();
                 FileOutputStream outputStream = new FileOutputStream(fileReference.get())
            ) {
                StreamUtils.copy(inputStream, outputStream);
                System.out.println("文件下载成功：" + fileReference.get().getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void uploadFileParams() {
        String folder = "E:\\temp\\upload-temp\\";
        File[] files = new File(folder).listFiles();
        List<MultipartFile> uploadFiles = Lists.newArrayList();
        for (File file : files) {
            try (InputStream inputStream = Files.newInputStream(file.toPath())) {
                MultipartFile multipartFile = new MockMultipartFile(file.getName() , file.getName() ,
                        MediaType.APPLICATION_OCTET_STREAM_VALUE, inputStream);
                uploadFiles.add(multipartFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, Object> result = this.httpClient.uploadParam(uploadFiles , 911 , "陈冬冬" , Arrays.asList("aaa" , "bbb" , "ccc"));
        System.out.println("===文件上传汇总=== , result = " + result);
    }

    @Test
    public void uploadFileBody() {
        String folder = "E:\\temp\\upload-temp\\";
        File[] files = new File(folder).listFiles();
        List<MultipartFile> uploadFiles = Lists.newArrayList();
        for (File file : files) {
            try (InputStream inputStream = Files.newInputStream(file.toPath())) {
                MultipartFile multipartFile = new MockMultipartFile(file.getName() , file.getName() ,
                        MediaType.APPLICATION_OCTET_STREAM_VALUE, inputStream);
                uploadFiles.add(multipartFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        UploadParam uploadParam = new UploadParam();
        uploadParam.setId(911);
        uploadParam.setName("陈冬冬");
        uploadParam.setNikeNames(Arrays.asList("aaa" , "bbb" , "ccc"));
        uploadParam.setFiles(uploadFiles);
        Map<String, Object> result = this.httpClient.uploadBody(uploadParam);
        System.out.println("===文件上传汇总=== , result = " + result);
    }

}
