package cn.chendd.xml;

import com.google.common.io.Files;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Random;

/**
 * 测试xml生成
 *
 * @author chendd
 * @date 2023/2/18 9:02
 */
@RunWith(JUnit4.class)
public class RowLineCreateTest {


    @Test
    public void createXml() throws Exception {
        String fileFolder = URLDecoder.decode(getClass().getResource("").getFile() , StandardCharsets.UTF_8.name());
        File file = new File(fileFolder , "data.xml");
        BufferedWriter writer = Files.newWriter(file, Charset.defaultCharset());
        this.writeBefore(writer);
        this.writeList(writer);
        this.writeAfter(writer);
        writer.flush();
        writer.close();
    }

    private void writeList(BufferedWriter writer) throws IOException {
        StringBuilder listBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 1 ; i <= 2_000_002 ; i++) {
            listBuilder.
                 append("            <tr>\n")
                    .append("                ")
                    .append("<td>").append(i).append("</td>")
                    .append("<td>").append(random.nextInt()).append("</td>")
                 .append("\n            </tr>").append('\n');
            if (i % 50 == 0) {
                writer.write(listBuilder.toString());
                listBuilder.setLength(0);
            }
        }
        if (listBuilder.length() > 0) {
            writer.write(listBuilder.toString());
            listBuilder.setLength(0);
        }
    }

    private void writeAfter(BufferedWriter writer) throws IOException {
        String xml =
                "        </table>\n" +
                "    </body>\n" +
                "</html>";
        writer.write(xml);
    }

    private void writeBefore(BufferedWriter writer) throws IOException {
        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "    <head>\n" +
                "        <meta charset=\"utf-8\" />\n" +
                "        <title>xml文件</title>\n" +
                "        <link href=\"https://www.chendd.cn\"></link>\n" +
                "        <meta content=\"陈冬冬\" name=\"author\" />\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <table>\n" +
                "            <tr>\n" +
                "               <th>序号</th><th>随机数字</th>" +
                "           </tr>\n";
        writer.write(xml);
    }


}
