package cn.chendd.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.text.MessageFormat;

/**
 * 启动类
 *
 * @author chendd
 * @date 2023/5/16 15:40
 */
@SpringBootApplication
public class Bootstrap {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(Bootstrap.class, args);
        String[] names = applicationContext.getBeanDefinitionNames();
        for (String name : names) {
            System.out.println(MessageFormat.format("bean name = [{0}] , bean class = [{1}]" ,
                    name , applicationContext.getBean(name).toString()));
        }
    }


}
