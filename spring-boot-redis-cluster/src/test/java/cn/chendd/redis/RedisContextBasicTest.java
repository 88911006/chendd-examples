package cn.chendd.redis;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 测试redis的基本信息
 *
 * @author chendd
 * @date 2023/5/21 22:22
 */
public class RedisContextBasicTest extends BaseTest {

    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void contextClusterNodes() {
        LettuceConnectionFactory factory = (LettuceConnectionFactory) this.redisTemplate.getConnectionFactory();
        assert factory != null;
        System.out.println("cluster nodes：");
        Set<RedisNode> clusterNodes = factory.getClusterConfiguration().getClusterNodes();
        for (RedisNode node : clusterNodes) {
            System.out.println(node.asString());
        }
    }

    @Test
    public void writeAndRead() {
        BoundValueOperations<String, String> v1 = this.stringRedisTemplate.boundValueOps("v1");
        v1.setIfAbsent("chendd");
        System.out.println("v1 = " + v1.get());
    }


    /**
     * 循环10次，添加10个值，观察各个节点存储的数据
     */
    @Test
    public void forStringRedisTemplate() {
        System.out.println(String.format("当前共有 %d 个客户端连接" , stringRedisTemplate.getClientList().size()));
        StopWatch stopWatch = StopWatch.createStarted();
        int max = 10;
        for (int i = 1 ; i <= max ; i++) {
            BoundValueOperations<String, String> value = this.stringRedisTemplate.boundValueOps("chendd-" + i);
            value.set(String.valueOf(i) , Duration.ofMinutes(5));
        }
        System.out.println("循环 " + max + " 次，耗时：" + stopWatch.getTime(TimeUnit.MILLISECONDS) + " 毫秒！");
    }

}
