package cn.chendd.redis;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.concurrent.*;

/**
 * Redis连接池测试，验证使用RedisTemplate的连接池工作机制
 * 知识点
 * （1）与 spring.redis.lettuce.pool.enabled 参数有关；
 *
 * @author chendd
 * @date 2023/5/19 22:22
 */
public class ReadisConnectionPoolTest extends BaseTest {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 累计循环10000个线程，观察getClientList的参数值
     */
    @Test
    public void poolStringRedisTemplate() throws InterruptedException {
        System.out.println(String.format("当前共有 %d 个客户端连接" , stringRedisTemplate.getClientList().size()));
        int count = 100;
        int number = 100;
        StopWatch stopWatch = StopWatch.createStarted();
        CountDownLatch countDownLatch = new CountDownLatch(count * number);
        CopyOnWriteArraySet<Integer> set = new CopyOnWriteArraySet<>();
        ExecutorService service = Executors.newFixedThreadPool(50);
        for (int j = 1 ; j <= count ; j++) {
            final int tempJ = j;
            service.submit(() -> {
                for (int i = 1; i <= number; i++) {
                    BoundValueOperations<String, String> value = stringRedisTemplate.boundValueOps("chendd-" + tempJ + "-" + i);
                    value.set(tempJ + "-" + i , Duration.ofMinutes(5));
                    set.add(stringRedisTemplate.getClientList().size());
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
        System.out.println("执行时间：" + stopWatch.getTime(TimeUnit.SECONDS) + "秒，结果 = " + StringUtils.join(set));
    }

}
