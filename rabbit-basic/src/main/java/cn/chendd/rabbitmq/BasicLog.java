package cn.chendd.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志输出器
 *
 * @author chendd
 * @date 2021/8/28 21:30
 */
public class BasicLog {

    /**
     * 定义日志输出器
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BasicLog.class);

    /**
     * 日志输出对象
     * @return 日志输出对象
     */
    public static Logger getLogger() {
        return LOGGER;
    }

}
