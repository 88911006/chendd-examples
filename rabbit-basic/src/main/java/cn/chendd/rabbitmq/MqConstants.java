package cn.chendd.rabbitmq;

/**
 * MQ常量类
 *
 * @author chendd
 * @date 2021/8/28 17:04
 */
public interface MqConstants {

    /**
     * 服务器
     */
    String HOST = "192.168.244.138";
    /**
     * 端口号
     */
    int PORT = 5672;
    /**
     * 虚拟主机
     */
    String VIRTUAL_HOST = "examples-chendd";
    /**
     * 用户名
     */
    String USERNAME = "admin";
    /**
     * 密码
     */
    String PASSWORD = "admin";

    /**
     * 交换机名称
     */
    String EXCHANGE_BLOG = "exchange_blog";
    /**
     * 队列名称
     */
    String QUEUE_BLOG = "queue_blog";
    /**
     * empty
     */
    String EMPTY = "";

}
