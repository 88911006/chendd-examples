package cn.chendd.rabbitmq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static cn.chendd.rabbitmq.MqConstants.*;

/**
 * MQ连接管理类
 *
 * @author chendd
 * @date 2021/8/28 17:03
 */
class MqConnection {


    /**
     * 构造新的conn连接
     * @return MQ连接
     * @throws IOException IO异常
     * @throws TimeoutException 连接超时异常
     */
    static Connection newConnection() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);
        factory.setPort(PORT);
        factory.setVirtualHost(VIRTUAL_HOST);
        factory.setUsername(USERNAME);
        factory.setPassword(PASSWORD);
        ///使用uri构造
        /*factory.setUri("amqp://admin:admin@192.168.244.138:5672/examples-chendd");*/
        return factory.newConnection();
    }

}
