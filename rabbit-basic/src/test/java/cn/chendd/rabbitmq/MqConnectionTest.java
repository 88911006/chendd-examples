package cn.chendd.rabbitmq;

import com.rabbitmq.client.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;

import static cn.chendd.rabbitmq.MqConstants.EMPTY;
import static cn.chendd.rabbitmq.MqConstants.EXCHANGE_BLOG;
import static cn.chendd.rabbitmq.MqConstants.QUEUE_BLOG;

/**
 * MQ连接测试类
 *
 * @author chendd
 * @date 2021/8/28 18:19
 */
@RunWith(JUnit4.class)
public class MqConnectionTest {

    /**
     * 生产者
     */
    @Test
    public void sendMessage() throws IOException, TimeoutException {
        try (Connection connection = MqConnection.newConnection();
             Channel channel = connection.createChannel()) {
            String message = "hello: chendd , DateTime: " + LocalDateTime.now().toString();
            channel.queueBind(QUEUE_BLOG, EXCHANGE_BLOG, EMPTY);
            channel.basicPublish(EXCHANGE_BLOG, EMPTY, MessageProperties.BASIC, message.getBytes());
        }
        BasicLog.getLogger().info("消息发送完毕");
    }

    /**
     * 消费者
     */
    @Test
    public void receiveMessage() throws IOException, TimeoutException {
        try (Connection connection = MqConnection.newConnection();
             Channel channel = connection.createChannel()) {
            channel.basicConsume(QUEUE_BLOG , true , new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    BasicLog.getLogger().info("body = {}" , new String(body));
                }
            });
            System.in.read();
        }
    }

}
