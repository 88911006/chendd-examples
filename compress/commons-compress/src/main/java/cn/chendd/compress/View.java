package cn.chendd.compress;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 预览文件对象
 *
 * @author chendd
 * @date 2023/3/5 8:19
 */

public class View {

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 压缩前大小
     */
    private long compressedSize;

    /**
     * 压缩后大小
     */
    private long uncompressedSize;

    /**
     * 是否为文件夹
     */
    private boolean directory;

    /**
     * 最后修改时间
     */
    private long lastModifiedTime;

    /**
     * 要获取的 fileName
     *
     * @return fileName 值
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 要设置的 fileName
     *
     * @param fileName fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 要获取的 compressedSize
     *
     * @return compressedSize 值
     */
    public long getCompressedSize() {
        return compressedSize;
    }

    /**
     * 要设置的 compressedSize
     *
     * @param compressedSize compressedSize
     */
    public void setCompressedSize(long compressedSize) {
        this.compressedSize = compressedSize;
    }

    /**
     * 要获取的 uncompressedSize
     *
     * @return uncompressedSize 值
     */
    public long getUncompressedSize() {
        return uncompressedSize;
    }

    /**
     * 要设置的 uncompressedSize
     *
     * @param uncompressedSize uncompressedSize
     */
    public void setUncompressedSize(long uncompressedSize) {
        this.uncompressedSize = uncompressedSize;
    }

    /**
     * 要获取的 directory
     *
     * @return directory 值
     */
    public boolean isDirectory() {
        return directory;
    }

    /**
     * 要设置的 directory
     *
     * @param directory directory
     */
    public void setDirectory(boolean directory) {
        this.directory = directory;
    }

    /**
     * 要获取的 lastModifiedTime
     *
     * @return lastModifiedTime 值
     */
    public long getLastModifiedTime() {
        return lastModifiedTime;
    }

    /**
     * 要设置的 lastModifiedTime
     *
     * @param lastModifiedTime lastModifiedTime
     */
    public void setLastModifiedTime(long lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
