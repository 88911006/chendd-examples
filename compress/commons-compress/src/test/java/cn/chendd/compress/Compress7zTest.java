package cn.chendd.compress;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 7z压缩和解压缩示例
 *
 * @author chendd
 * @date 2023/3/5 17:25
 */
@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Compress7zTest {

    /**
     * 压缩文件
     */
    @Test
    public void sevenZipFile() throws IOException {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "源文件/哈喽.txt");
        File zipFile = new File(projectHome , "压缩文件夹/sevenZ_哈喽.7z");
        Compress7z.sevenZ(file , zipFile);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 压缩文件夹
     */
    @Test
    public void sevenZipFolder() throws IOException {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "源文件/复杂文件夹");
        File zipFile = new File(projectHome , "压缩文件夹/sevenZ_复杂文件夹.7z");
        Compress7z.sevenZ(file , zipFile);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 解压缩7z文件
     */
    @Test
    public void unSevenZipFile() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File zipFile = new File(projectHome , "压缩文件夹/sevenZ_哈喽.7z");
        File file = new File(projectHome , "解压缩文件夹/sevenZ_哈喽");
        Compress7z.unSevenZ(zipFile , file);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("解压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 解压缩7z文件夹
     */
    @Test
    public void unSevenZipFolder() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File zipFile = new File(projectHome , "压缩文件夹/sevenZ_复杂文件夹.7z");
        File file = new File(projectHome , "解压缩文件夹/sevenZ_复杂文件夹");
        Compress7z.unSevenZ(zipFile , file);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("解压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 解压缩带密码的7z文件【Apache Commons Compress不支持生成带密码的7z，需要自己手工创建】
     * 本例中的7z文件设置了压缩文件名
     */
    @Test
    public void unSevenZipFolderPassword() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File zipFile = new File(projectHome , "压缩文件夹/sevenZ_带密码_简单文件夹.7z");
        File file = new File(projectHome , "解压缩文件夹/sevenZ_带密码_简单文件夹");
        Compress7z.unSevenZ(zipFile , file , "https://www.chendd.cn");
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("解压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 预览压缩包文件列表
     */
    @Test
    public void view() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File zipFile = new File(projectHome , "压缩文件夹/sevenZ_复杂文件夹.7z");
        System.out.println("预览压缩包的文件：");
        List<View> list = Compress7z.view(zipFile);
        list.forEach(System.out::println);
    }


}
