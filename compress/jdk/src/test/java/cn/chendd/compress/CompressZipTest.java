package cn.chendd.compress;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 测试压缩zip
 *
 * @author chendd
 * @date 2023/3/4 20:44
 */
@RunWith(JUnit4.class)
public class CompressZipTest {

    /**
     * 测试压缩文件
     */
    @Test
    public void zipFile() throws IOException {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "源文件/哈喽.txt");
        File zipFile = new File(projectHome , "压缩文件夹/哈喽.zip");
        CompressZip.zip(file , zipFile);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 测试压缩文件夹
     */
    @Test
    public void zipFolder() throws IOException {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File folder = new File(projectHome , "源文件/简单文件夹");
        File zipFile = new File(projectHome , "压缩文件夹/简单文件夹.zip");
        CompressZip.zip(folder , zipFile);
        System.out.println("压缩源文件：" + folder.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 测试压缩复杂结构文件夹
     */
    @Test
    public void zipComplexFolder() throws IOException {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File folder = new File(projectHome , "源文件/复杂文件夹");
        File zipFile = new File(projectHome , "压缩文件夹/复杂文件夹.zip");
        CompressZip.zip(folder , zipFile);
        System.out.println("压缩源文件：" + folder.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

}
