package cn.chendd.compress.sevenz;

import org.apache.commons.compress.utils.Lists;

import java.io.File;
import java.util.List;

/**
 * @author chendd
 * @date 2023/03/05 19:45
 */
public class CompressOutItemStructure {

    public static List<Item> create(File compressFile) {
        List<Item> itemList = Lists.newArrayList();
        generare7zFile(compressFile , itemList);
        return itemList;
    }

    private static void generare7zFile(File inFile , List<Item> itemList) {
        if (inFile.isFile()) {
            itemList.add(new Item(inFile));
            return;
        }
        File[] files = inFile.listFiles();
        if (files == null || files.length == 0) {
            //空文件夹
            itemList.add(new Item(inFile));
            return;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                generare7zFile(file, itemList);
            } else {
                itemList.add(new Item(file));
            }
        }
    }

}
