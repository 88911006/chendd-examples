package cn.chendd.compress.sevenz;

import java.io.File;

/**
 * @author chendd
 * @date 2023/03/05 19:45
 */
public class Item {

    private File file;

    public Item(File file) {
        this.file = file;
    }

    /**
     * 要获取的 file
     *
     * @return file 值
     */
    public File getFile() {
        return file;
    }

    /**
     * 要设置的 file
     *
     * @param file file
     */
    public void setFile(File file) {
        this.file = file;
    }

}
