package cn.chendd.compress;

import net.sf.sevenzipjbinding.IInArchive;
import net.sf.sevenzipjbinding.SevenZip;
import net.sf.sevenzipjbinding.SevenZipException;
import net.sf.sevenzipjbinding.impl.RandomAccessFileInStream;
import net.sf.sevenzipjbinding.simple.ISimpleInArchive;
import net.sf.sevenzipjbinding.simple.ISimpleInArchiveItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * 提取压缩包文件列表
 *
 * @author chendd
 * @date 2023/3/5 21:13
 */
@RunWith(JUnit4.class)
public class CompressListItemsTest {

    private final String password = "https://www.chendd.cn";

    /**
     * 提取rar文件
     */
    @Test
    public void testNotPassword() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "压缩文件夹/7zipjbinging_复杂文件夹.rar");
        RandomAccessFile randomAccessFile = null;
        IInArchive inArchive = null;
        try {
            randomAccessFile = new RandomAccessFile(file, "r");
            inArchive = SevenZip.openInArchive(null, // autodetect archive type
                    new RandomAccessFileInStream(randomAccessFile));
            // Getting simple interface of the archive inArchive
            ISimpleInArchive simpleInArchive = inArchive.getSimpleInterface();
            System.out.println("   Size   |  isFolder | Compr.Sz. |           CreationTime         |           LastAccessTime       | Filename");
            System.out.println("----------+-----------+---------");

            for (ISimpleInArchiveItem item : simpleInArchive.getArchiveItems()) {
                System.out.println(String.format("%9s | %9s | %9s | %30s | %30s | %s", //
                        item.getSize(), item.isFolder() ,
                        item.getPackedSize(),
                        item.getLastAccessTime(), item.getCreationTime() ,
                        item.getPath()));
                System.out.println(item.getComment());
            }
        } catch (Exception e) {
            System.err.println("Error occurs: " + e);
            e.printStackTrace();
        } finally {
            if (inArchive != null) {
                try {
                    inArchive.close();
                } catch (SevenZipException e) {
                    System.err.println("Error closing archive: " + e);
                }
            }
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException e) {
                    System.err.println("Error closing file: " + e);
                }
            }
        }
    }

    /**
     * 提取rar文件（含密码）
     */
    @Test
    public void testUsePassword() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "压缩文件夹/7zipjbinging_带密码_简单文件夹.rar");
        RandomAccessFile randomAccessFile = null;
        IInArchive inArchive = null;
        try {
            randomAccessFile = new RandomAccessFile(file, "r");
            inArchive = SevenZip.openInArchive(null, // autodetect archive type
                    new RandomAccessFileInStream(randomAccessFile) , password);
            // Getting simple interface of the archive inArchive
            ISimpleInArchive simpleInArchive = inArchive.getSimpleInterface();
            System.out.println("   Size   |  isFolder | Compr.Sz. |           CreationTime         |           LastAccessTime       | Filename");
            System.out.println("----------+-----------+---------");

            for (ISimpleInArchiveItem item : simpleInArchive.getArchiveItems()) {
                System.out.println(String.format("%9s | %9s | %9s | %30s | %30s | %s", //
                        item.getSize(), item.isFolder() ,
                        item.getPackedSize(),
                        item.getLastAccessTime(), item.getCreationTime() ,
                        item.getPath()));
                System.out.println(item.getComment());
            }
        } catch (Exception e) {
            System.err.println("Error occurs: " + e);
            e.printStackTrace();
        } finally {
            if (inArchive != null) {
                try {
                    inArchive.close();
                } catch (SevenZipException e) {
                    System.err.println("Error closing archive: " + e);
                }
            }
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException e) {
                    System.err.println("Error closing file: " + e);
                }
            }
        }
    }

}
