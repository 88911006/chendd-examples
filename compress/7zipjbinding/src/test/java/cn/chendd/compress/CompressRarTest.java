package cn.chendd.compress;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

import java.io.File;

/**
 * 测试rar格式压缩包文件的操作
 *
 * @author chendd
 * @date 2023/3/5 19:29
 */
@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CompressRarTest {

    /**
     * 测试文件生成rar
     */
    @Test
    public void toRarFile() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "源文件/哈喽.txt");
        File zipFile = new File(projectHome , "压缩文件夹/7zipjbinging_哈喽.rar");
        CompressRar.sevenZ(file , zipFile);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 测试文件夹生成rar
     */
    @Test
    public void toRarFolder() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "源文件/复杂文件夹");
        File zipFile = new File(projectHome , "压缩文件夹/7zipjbinging_复杂文件夹.rar");
        CompressRar.sevenZ(file , zipFile);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 测试带密码的文件夹生成rar（加密文件名）
     */
    @Test
    public void toRarPasswordFolder() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File file = new File(projectHome , "源文件/简单文件夹");
        File zipFile = new File(projectHome , "压缩文件夹/7zipjbinging_带密码_简单文件夹.rar");
        CompressRar.sevenZ(file , zipFile , "https://www.chendd.cn");
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 解压缩 rar 文件
     */
    @Test
    public void unRarFile() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File zipFile = new File(projectHome , "压缩文件夹/7zipjbinging_复杂文件夹.rar");
        File file = new File(projectHome , "解压缩文件夹/7zipjbinging_复杂文件夹");
        CompressRar.unSevenZ(zipFile , file);
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("解压缩后文件：" + zipFile.getAbsolutePath());
    }

    /**
     * 解压缩带密码的 rar 文件
     */
    @Test
    public void unRarPasswordFile() {
        File projectHome = new File(System.getProperty("user.dir")).getParentFile();
        File zipFile = new File(projectHome , "压缩文件夹/7zipjbinging_带密码_简单文件夹.rar");
        File file = new File(projectHome , "解压缩文件夹/7zipjbinging_带密码_简单文件夹");
        CompressRar.unSevenZ(zipFile , file , "https://www.chendd.cn");
        System.out.println("压缩源文件：" + file.getAbsolutePath());
        System.out.println("解压缩后文件：" + zipFile.getAbsolutePath());
    }

}
