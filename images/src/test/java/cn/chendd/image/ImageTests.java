package cn.chendd.image;

import cn.chendd.core.utils.ImageUtil;
import org.junit.jupiter.api.Test;

import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;

/**
 * 图片测试实现
 *
 * @author chendd
 * @date 2021/12/6 22:31
 */
public class ImageTests {

    @Test
    public void testWatermarkImage() {
        long begin = System.currentTimeMillis();
        String path = getClass().getResource("/image.png").getPath();
        FileSystemView systemView = FileSystemView.getFileSystemView();
        File outFile = new File(systemView.getHomeDirectory() , "watemark.png");
        ImageUtil.watermark(new File(path) , outFile , new Font("微软雅黑", Font.BOLD, 26) ,
                Color.RED , "http://www.chendd.cn" , ImageUtil.TextPositions.BOTTOM_CENTER , 0.5f);
        System.out.println("程序耗时：" + (System.currentTimeMillis() - begin));
    }

    @Test
    public void testWatermarkLongImage() {
        long begin = System.currentTimeMillis();
        String path = getClass().getResource("/image.png").getPath();
        FileSystemView systemView = FileSystemView.getFileSystemView();
        File outFile = new File(systemView.getHomeDirectory() , "watemark_long.png");
        ImageUtil.watermarkLongImage(new File(path) , outFile , "http://www.chendd.cn" , new Font("微软雅黑", Font.BOLD, 26) , Color.BLUE);
        System.out.println("程序耗时：" + (System.currentTimeMillis() - begin));
    }

}
