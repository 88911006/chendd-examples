package cn.chendd.dbf;

import com.linuxense.javadbf.DBFDataType;
import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;

/**
 * DbfT写文件测试类
 *
 * @author chendd
 * @date 2023/6/30 13:06
 */
@RunWith(JUnit4.class)
public class DbfWriterTest {

    @Test
    public void writeDbf() throws Exception {
        File desktop = FileSystemView.getFileSystemView().getHomeDirectory();
        File dbfFile = new File(desktop , "用户信息.dbf");
        Charset charset = Charset.forName("GBK");
        try (DBFWriter writer = new DBFWriter(new FileOutputStream(dbfFile) , charset)) {
            //设置字段类型
            DBFField[] fields = new DBFField[4];
            //字段 id
            DBFField idField = new DBFField("id" , DBFDataType.NUMERIC , 16);
            fields[0] = idField;
            //字段 name
            DBFField nameField = new DBFField("name" , DBFDataType.CHARACTER , 32);
            fields[1] = nameField;
            //字段 bonus
            DBFField performanceField = new DBFField("bonus" , DBFDataType.NUMERIC , 16 , 2);
            fields[2] = performanceField;
            //字段 birthday
            DBFField birthdayField = new DBFField("birthday" , DBFDataType.DATE);
            fields[3] = birthdayField;

            writer.setFields(fields);
            //设置数据
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            for (int i=1 ; i <= 10 ; i++) {
                Object[] values = new Object[4];
                values[0] = 1000 + i;
                values[1] = "chen冬冬_" + i;
                values[2] = new BigDecimal(Math.random() * 10000).setScale(2 , RoundingMode.HALF_UP);
                values[3] = dateFormat.parse("2023070" + i);
                writer.addRecord(values);
            }
        }
        System.out.println(String.format("文件 %s 已生成" , dbfFile.getAbsolutePath()));
    }

}
