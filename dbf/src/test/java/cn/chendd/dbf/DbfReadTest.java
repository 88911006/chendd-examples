package cn.chendd.dbf;

import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * DbfT写文件测试类
 *
 * @author chendd
 * @date 2023/6/30 13:06
 */
@RunWith(JUnit4.class)
public class DbfReadTest {

    @Test
    public void readDbf() throws Exception {
        File desktop = FileSystemView.getFileSystemView().getHomeDirectory();
        File dbfFile = new File(desktop , "用户信息.dbf");
        if (! dbfFile.exists()) {
            System.err.println(String.format("文件 %s 未找到，请先运行文件写入示例！" , dbfFile.getAbsolutePath()));
            return;
        }
        Charset charset = Charset.forName("GBK");
        try (DBFReader reader = new DBFReader(new FileInputStream(dbfFile) , charset)) {
            int fieldCount = reader.getFieldCount();
            System.out.println("===字段===");
            for (int i = 0; i < fieldCount; i++) {
                DBFField field = reader.getField(i);
                System.out.println(String.format("%s , %s , %s , %s" ,
                        field.getName() , field.getType() , field.getLength() , field.getDecimalCount()));
            }
            System.out.println("===数据===");
            int recordCount = reader.getRecordCount();
            for (int i = 0; i < recordCount; i++) {
                Object[] values = reader.nextRecord();
                System.out.println(Arrays.toString(values));
            }
        }
        System.out.println(String.format("文件 %s 已读取" , dbfFile.getAbsolutePath()));
    }

}
