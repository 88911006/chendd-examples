package cn.chendd.jsch.utils;

import lombok.Builder;
import lombok.Data;

/**
 * sftp连接信息
 *
 * @author chendd
 * @date 2023/11/18 9:07
 */
@Data
public class SftpParam {

    /**
     * 用户名
     */
    private String username = "root";
    /**
     * 密码
     */
    private String password = "root";
    /**
     * ip地址
     */
    private String host = "192.168.244.138";
    /**
     * 端口号
     */
    private int port = 22;
    /**
     * 超时时间
     */
    private int timeout = 1000 * 10;

}
