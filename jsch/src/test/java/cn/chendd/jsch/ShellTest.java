package cn.chendd.jsch;

import cn.chendd.jsch.utils.SftpParam;
import com.jcraft.jsch.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;

/**
 * Shell命令测试
 *
 * @author chendd
 * @date 2023/11/18 15:23
 */
@RunWith(JUnit4.class)
public class ShellTest {

    @Test
    public void shellTest() throws JSchException, IOException {
        JSch jsch = new JSch();
        SftpParam param = new SftpParam();
        Session session = null;
        ChannelShell channel;
        try {
            session = jsch.getSession(param.getUsername(), param.getHost(), param.getPort());
            final String password = param.getPassword();
            if (StringUtils.isNotEmpty(password)) {
                session.setPassword(password);
            }
            session.setTimeout(param.getTimeout());
            session.setConfig("StrictHostKeyChecking" , "no");
            session.connect();
            channel = (ChannelShell) session.openChannel("shell");
        } catch (JSchException e) {
            if (session != null) {
                session.disconnect();
            }
            throw new RuntimeException(e);
        }
        try {
            this.shellTest(channel);
        } finally {
            channel.disconnect();
            session.disconnect();
        }
    }

    private void shellTest(ChannelShell channel) throws JSchException, IOException {
        channel.connect();

        //如果什么都不输出：Last login: Fri Nov 17 15:29:09 2023 from 192.168.244.1
        try (PrintStream out = new PrintStream(channel.getOutputStream())) {
            out.println("pwd");
            out.flush();
            out.println("ip addr");
            out.flush();
            out.println("ping -c 5 127.0.0.1");
            out.flush();
            out.println("ll");
            out.flush();
            out.println("ls");
            out.flush();
            out.println("exit");
            out.flush();
        }

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(channel.getInputStream()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        }
    }

}
