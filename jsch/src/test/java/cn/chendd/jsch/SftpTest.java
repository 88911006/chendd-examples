package cn.chendd.jsch;

import cn.chendd.jsch.utils.SftpParam;
import cn.chendd.jsch.utils.SftpUtils;
import com.jcraft.jsch.JSch;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;

/**
 * 测试sftp
 *
 * @author chendd
 * @date 2023/11/18 9:54
 */
@RunWith(JUnit4.class)
public class SftpTest {

    @Test
    public void version() {
        System.out.println(JSch.VERSION);
    }

    /**
     * 获取文件夹列表
     */
    @Test
    public void listFileTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            List<SftpUtils.FileEntry> files = sftp.listFiles("/app/arthas/arthas-class");
            for (SftpUtils.FileEntry file : files) {
                System.out.println((file.isDir() ? "文件夹  " : "文件    ") + file.getLongname());
            }
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 上传文件
     */
    @Test
    public void uploadFileTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            String file = getClass().getResource("SftpTest.class").getFile();
            sftp.uploadFile(file , "/app/arthas/arthas-class/SftpTest.class");
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 下载文件
     */
    @Test
    public void downloadFileTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            File destFile = new File(FileUtils.getTempDirectory() , "SftpTest.class");
            sftp.downloadFile("/app/arthas/arthas-class/SftpTest.class" , destFile.getPath());
            Assert.assertTrue(destFile.exists());
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 重命名文件
     */
    @Test
    public void renameTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            sftp.rename("/app/arthas/arthas-class/SftpTest.class" , "/app/arthas/arthas-class/SftpTest（2）.class");
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 删除文件
     */
    @Test
    public void removeFileTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            sftp.removeFile("/app/arthas/arthas-class/SftpTest.class");
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 删除文件夹
     */
    @Test
    public void removeFolderTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            sftp.removeFolder("/app/arthas/arthas-class/bbb");
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 路径是否存在
     */
    @Test
    public void existsTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            System.out.println("文件夹是否存在：" + sftp.exists("/app/arthas/arthas-class/"));
            System.out.println("文件是否存在：" + sftp.exists("/app/arthas/arthas-class"));
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 路径是否存在
     */
    @Test
    public void mkdirsTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            System.out.println("文件夹是否存在：" + sftp.exists("/app/arthas/arthas-class/aaa/bbb/ccc/"));
            System.out.println("======创建多层文件夹======");
            sftp.mkdirs("/app/arthas/arthas-class/aaa/bbb/ccc");
            System.out.println("文件夹是否存在：" + sftp.exists("/app/arthas/arthas-class/aaa/bbb/ccc/"));
        } finally {
            sftp.disconnect();
        }
    }

}
