package cn.chendd.jsch;

import cn.chendd.jsch.utils.SftpParam;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 运行命令测试
 *
 * @author chendd
 * @date 2023/11/18 14:25
 */
@RunWith(JUnit4.class)
public class ExecTest {

    /**
     * 执行脚本
     */
    @Test
    public void commandTest() throws IOException, JSchException {
        JSch jsch = new JSch();
        SftpParam param = new SftpParam();
        Session session = null;
        ChannelExec channel;
        try {
            session = jsch.getSession(param.getUsername(), param.getHost(), param.getPort());
            final String password = param.getPassword();
            if (StringUtils.isNotEmpty(password)) {
                session.setPassword(password);
            }
            session.setTimeout(param.getTimeout());
            session.setConfig("StrictHostKeyChecking" , "no");
            session.connect();
            channel = (ChannelExec) session.openChannel("exec");
        } catch (JSchException e) {
            if (session != null) {
                session.disconnect();
            }
            throw new RuntimeException(e);
        }
        try {
            this.commandTest(channel);
        } finally {
            channel.disconnect();
            session.disconnect();
        }
    }

    private void commandTest(ChannelExec channel) throws IOException, JSchException {
        String commands = StringUtils.join(new String[] {"ip addr" , "pwd" , "ping -c 5 127.0.0.1" , "ll" , "ls"} , ";");
        System.out.println("执行命令列表：" + commands);
        channel.setCommand(commands);
        channel.connect();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(channel.getInputStream()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        }
    }

}
