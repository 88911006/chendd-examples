;(function ($) {

    var getMessage = function (icon, message) {
        var template = [
            '<div class="media"><div class="media-left">',
                '<img class="media-object img-lg img-circle" src="plugins/icons/' + icon + '.png">',
            '</div>',
            '<div class="media-body">' + message + '</div></div>'
        ];
        return template.join("");
    };
    //提示框
    $.alert = function (message, fn, title, icon) {
        bootbox.alert({
            title: title || "警告",
            className: "dialog-alert",
            centerVertical: true,
            backdrop: true,
            message: getMessage(icon || 'warn', message),
            locale: "zh_CN",
            callback: fn || function () {},
            buttons: {
                ok: {
                    label: "确定",
                    className: "btn-primary"
                }
            }
        });
    };

    $.alert.success = function(message , fn) {
        $.alert(message , fn , "提示" , "success");
    };

    $.alert.warn = function(message , fn) {
        $.alert(message , fn , "警告" , "warn");
    };

    $.alert.error = function(message , fn) {
        $.alert(message , fn , "错误" , "error");
    };

    //确认框
    $.confirm = function (message, fn, title) {
        bootbox.confirm({
            title: title || "询问",
            className: "dialog-confirm",
            centerVertical: true,
            backdrop: false,
            message: getMessage('question', message),
            callback: fn,
            locale: "zh_CN",
            buttons: {
                cancel: {
                    label: "取消",
                    className: "btn-default"
                },
                confirm: {
                    label: "确定",
                    className: "btn-danger"
                }
            }
        });
    };

    //遮罩层
    $.loading = function(message){
        window.loading = bootbox.dialog({
            message: "<div align='center'><i class='fa fa-spin fa-spinner'></i>&nbsp;&nbsp;" + message + "</div>",
            closeButton: false,
            centerVertical: true,
            size: "small"
        });
        return window.loading;
    };

    $.loading.show = function(message) {
        //防止重复显示
        if(!window.loading) {
            $.loading(message);
        }
    }

    $.loading.hide = function() {
        if(window.loading) {
            window.loading.modal("hide");
        }
    }

})(jQuery);