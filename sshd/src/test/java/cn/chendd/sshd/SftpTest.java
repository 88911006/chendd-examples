package cn.chendd.sshd;

import cn.chendd.sshd.utils.SftpParam;
import cn.chendd.sshd.utils.SftpUtils;
import org.apache.commons.io.FileUtils;
import org.apache.sshd.common.config.VersionProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;

/**
 * SftpTest Test
 *
 * @author chendd
 * @date 2023/11/17 16:03
 */
@RunWith(JUnit4.class)
public class SftpTest {

    @Test
    public void version() {
        System.out.println(VersionProperties.getVersionProperties().get(VersionProperties.REPORTED_VERSION));
    }

    /**
     * 获取文件夹列表
     */
    @Test
    public void listFileTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            List<SftpUtils.FileEntry> files = sftp.listFiles("/app/arthas/arthas-class");
            for (SftpUtils.FileEntry file : files) {
                System.out.println((file.isDir() ? "文件夹  " : "文件    ") + file.getLongname());
            }
        } finally {
            sftp.disconnect();
        }
    }

    @Test
    public void uploadFileTest() throws Exception {
        final SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            String file = getClass().getResource("SftpTest.class").getFile();
            sftp.uploadFile(new File(file), "/app/arthas/arthas-class/SftpTest.class");
        } finally {
            sftp.disconnect();
        }
    }

    @Test
    public void downloadFileTest() throws Exception {
        final SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            File destFile = new File(FileUtils.getTempDirectory() , "SftpTest.class");
            sftp.downloadFile("/app/arthas/arthas-class/SftpTest.class" , destFile);
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 重命名文件
     */
    @Test
    public void renameTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            sftp.renameFile("/app/arthas/arthas-class/SftpTest.class" , "/app/arthas/arthas-class/SftpTest（2）.class");
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 删除文件
     */
    @Test
    public void removeFileTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            sftp.removeFile("/app/arthas/arthas-class/SftpTest（2）.class");
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 路径是否存在
     */
    @Test
    public void existsTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            System.out.println("文件夹是否存在：" + sftp.exists("/app/arthas/arthas-class/"));
            System.out.println("文件是否存在：" + sftp.exists("/app/arthas/arthas-class"));
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 路径是否存在
     */
    @Test
    public void mkdirsTest() throws Exception {
        SftpUtils sftp = SftpUtils.newInstance(new SftpParam());
        try {
            System.out.println("文件夹是否存在：" + sftp.exists("/app/arthas/arthas-class/aaa/bbb/ccc/"));
            System.out.println("======创建多层文件夹======");
            sftp.mkdirs("/app/arthas/arthas-class/aaa/bbb/ccc/");
            System.out.println("文件夹是否存在：" + sftp.exists("/app/arthas/arthas-class/aaa/bbb/ccc/"));
        } finally {
            sftp.disconnect();
        }
    }

}
