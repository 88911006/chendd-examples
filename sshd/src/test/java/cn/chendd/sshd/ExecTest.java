package cn.chendd.sshd;

import cn.chendd.sshd.utils.SftpParam;
import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.channel.ChannelExec;
import org.apache.sshd.client.session.ClientSession;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * 运行命令测试
 *
 * @author chendd
 * @date 2023/11/20 20:52
 */
public class ExecTest {

    /**
     * 执行脚本
     */
    @Test
    public void commandTest() throws IOException {
        SshClient sshClient = SshClient.setUpDefaultClient();
        sshClient.start();
        SftpParam param = new SftpParam();
        ClientSession session = sshClient.connect(param.getUsername(), param.getHost(), param.getPort()).verify(5 , TimeUnit.SECONDS).getSession();
        session.addPasswordIdentity(param.getPassword());
        session.auth().verify();
        String commands = StringUtils.join(new String[] {"ip addr" , "pwd" , "ping -c 5 127.0.0.1" , "ll" , "ls"} , ";");
        System.out.println("执行命令列表：" + commands);
        try (ChannelExec channel = session.createExecChannel(commands)) {
            channel.open().verify(5 , TimeUnit.SECONDS);
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(channel.getInvertedOut()))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                }
            }
        }
        System.out.println("ExecTest.execTest");
        session.close();
        sshClient.close();
    }

}
