package cn.chendd.sshd;

import cn.chendd.sshd.utils.SftpParam;
import org.apache.commons.io.FileUtils;
import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.session.ClientSession;
import org.apache.sshd.sftp.client.SftpClientFactory;
import org.apache.sshd.sftp.client.fs.SftpFileSystem;
import org.apache.sshd.sftp.client.fs.SftpPath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Exec Test
 *
 * @author chendd
 * @date 2023/11/20 19:10
 */
@RunWith(JUnit4.class)
public class SftpFileSystemTest {

    /**
     * 文件信息获取
     */
    @Test
    public void fileTest() throws IOException {
        function((sshClient, session, fileSystem) -> {
            final SftpPath path = fileSystem.getPath("/app/arthas/arthas-class/nohup.out");
            System.out.println("文件名称：" + path.getFileName());
            System.out.println("文件大小：" + Files.size(path));
            //注：因时区差异需要设置小时+8
            System.out.println("最后更新时间：" + Files.getLastModifiedTime(path));
            System.out.println("文件权限：" + PosixFilePermissions.toString(Files.getPosixFilePermissions(path)));
        });
    }

    /**
     * 文件夹信息获取
     */
    @Test
    public void folderTest() throws IOException {
        function((sshClient, session, fileSystem) -> {
            final SftpPath defaultDir = fileSystem.getDefaultDir();
            System.out.println("获取系统默认文件夹：" + defaultDir.toRealPath());
            final SftpPath path = fileSystem.getPath("/app/arthas/arthas-class/");
            System.out.println("文件夹名称：" + path.getFileName());
            System.out.println("文件夹大小：" + Files.size(path));
            //注：因时区差异需要设置小时+8
            System.out.println("最后更新时间：" + Files.getLastModifiedTime(path));
            System.out.println("文件夹权限：" + PosixFilePermissions.toString(Files.getPosixFilePermissions(path)));

            System.out.println("=========================列出文件夹中全部======================");
            try (final Stream<Path> stream = Files.list(path)) {
                final List<Path> paths = stream.collect(Collectors.toList());
                for (Path p : paths) {
                    if (Files.isDirectory(p)) {
                        System.out.println("文件夹：" + p.getFileName());
                    } else {
                        System.out.println("文件：  " + p.getFileName());
                    }
                }
            }
            System.out.println("=========================列出文件夹中全部======================");
        });
    }

    /**
     * 创建多层级文件夹目录
     */
    @Test
    public void mkdirsTest() throws IOException {
        function((sshClient, session, fileSystem) -> {
            final SftpPath path = fileSystem.getPath("/app/arthas/arthas-class/a/b/c/");
            Files.createDirectories(path);
        });
    }

    /**
     * 删除【空文件夹或者具体文件】
     */
    @Test
    public void removeTest() throws IOException {
        function((sshClient, session, fileSystem) -> {
            final SftpPath path = fileSystem.getPath("/app/arthas/arthas-class/a/b/c/");
            Files.delete(path);
        });
    }

    /**
     * 上传文件
     */
    @Test
    public void uploadTest() throws IOException {
        function((sshClient, session, fileSystem) -> {
            final File file = new File(getClass().getResource("SftpFileSystemTest.class").getFile());
            final Path path = Paths.get(file.toURI());
            Files.copy(path , fileSystem.getPath("/app/arthas/arthas-class/" + file.getName()) , StandardCopyOption.REPLACE_EXISTING);
        });
    }

    /**
     * 下载文件
     */
    @Test
    public void downloadTest() throws IOException {
        function((sshClient, session, fileSystem) -> {
            File destFile = new File(FileUtils.getTempDirectory() , "SftpFileSystemTest.class");
            final SftpPath path = fileSystem.getPath("/app/arthas/arthas-class/SftpFileSystemTest.class");
            Files.copy(path , destFile.toPath() , StandardCopyOption.REPLACE_EXISTING);
        });
    }

    protected void function(Runnable runnable) throws IOException {
        SshClient sshClient = SshClient.setUpDefaultClient();
        sshClient.start();

        SftpParam param = new SftpParam();
        ClientSession session = sshClient.connect(param.getUsername(), param.getHost(), param.getPort()).verify().getSession();
        session.addPasswordIdentity(param.getPassword());
        session.auth().verify();
        SftpFileSystem fileSystem = SftpClientFactory.instance().createSftpFileSystem(session);

        runnable.run(sshClient , session , fileSystem);

        fileSystem.close();
        session.close();
        sshClient.close();
    }

    @FunctionalInterface
    protected interface Runnable {

        void run(SshClient sshClient , ClientSession session , SftpFileSystem fileSystem) throws IOException;

    }

}
