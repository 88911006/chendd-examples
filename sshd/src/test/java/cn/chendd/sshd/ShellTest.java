package cn.chendd.sshd;

import cn.chendd.sshd.utils.SftpParam;
import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.channel.ChannelShell;
import org.apache.sshd.client.session.ClientSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

/**
 * Shell命令测试
 *
 * @author chendd
 * @date 2023/11/20 22:25
 */
@RunWith(JUnit4.class)
public class ShellTest {

    @Test
    public void shellTest() throws IOException {
        SshClient sshClient = SshClient.setUpDefaultClient();
        sshClient.start();
        SftpParam param = new SftpParam();
        ClientSession session = sshClient.connect(param.getUsername(), param.getHost(), param.getPort()).verify(5 , TimeUnit.SECONDS).getSession();
        session.addPasswordIdentity(param.getPassword());
        session.auth().verify();
        String[] commands = new String[] {"ip addr" , "pwd" , "ping -c 5 127.0.0.1" , "ll" , "ls"};
        System.out.println("执行命令列表：" + StringUtils.join(commands , ";"));
        try (ChannelShell channel = session.createShellChannel()) {
            channel.open().verify(5 , TimeUnit.SECONDS);
            // 执行命令
            try (PrintStream out = new PrintStream(channel.getInvertedIn())) {
                out.println("pwd");
                out.flush();
                out.println("ip addr");
                out.flush();
                out.println("ping -c 5 127.0.0.1");
                out.flush();
                out.println("ll");
                out.flush();
                out.println("ls");
                out.flush();
                out.println("exit");
                out.flush();
            }
            //channel.waitFor(Collections.singleton(ClientChannelEvent.CLOSED), 0);
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(channel.getInvertedOut()))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                }
            }
        }
        session.close();
        sshClient.close();
    }


}
