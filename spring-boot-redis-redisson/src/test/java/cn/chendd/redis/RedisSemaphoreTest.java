package cn.chendd.redis;

import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RSemaphore;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 信号量（Semaphore）简单测试，semaphore 持久化存储
 *
 * @author chendd
 * @date 2023/5/23 21:56
 */
public class RedisSemaphoreTest extends BaseTest {

    @Resource
    private Redisson redisson;

    @Test
    public void simpleApi() throws InterruptedException {
        //构造信号量
        RSemaphore semaphore = redisson.getSemaphore("semaphore");
        if (semaphore.isExists() && semaphore.availablePermits() == 0) {
            System.err.println("活已经全部分配完了...，干不了了，可以删除 [semaphore] 再试");
            ///semaphore.delete();
            return;
        }
        //尝试设置初始范围为 10，仅当不存在时设置
        boolean set = semaphore.trySetPermits(10);
        System.out.println("trySetPermits = " + set);
        ExecutorService executorService = Executors.newFixedThreadPool(15);
        AtomicInteger value = new AtomicInteger(0);
        //模拟100次请求，使用线程池进行消费
        for (int i = 1 ; i <= 100 ; i++) {
            executorService.submit(() -> {
                boolean consumer = semaphore.tryAcquire();
                if (consumer) {
                    try {
                        System.out.println(String.format("线程 %s 获取到令牌，干活中...%d" , Thread.currentThread().getName() , value.addAndGet(1)));
                        TimeUnit.SECONDS.sleep(1L);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        semaphore.release();
                        System.out.println(String.format("线程 %s 获取到令牌，干完活...%d" , Thread.currentThread().getName() , value.addAndGet(1)));
                    }
                } else {
                    System.err.println(String.format("线程 %s 未获取到令牌，歇息中...%d" , Thread.currentThread().getName() , value.addAndGet(1)));
                }
            });
        }
        //主线程休眠 1 分钟
        TimeUnit.SECONDS.sleep(10L);
    }

}
