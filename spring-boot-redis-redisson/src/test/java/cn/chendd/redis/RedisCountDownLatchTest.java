package cn.chendd.redis;

import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RCountDownLatch;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 闭锁（CountDownLatch）可以多开客户端验证
 * @author chendd
 * @date 2023/5/23 17:50
 */
public class RedisCountDownLatchTest extends BaseTest {

    @Resource
    private Redisson redisson;

    @Test
    public void simpleApi() throws InterruptedException {
        RCountDownLatch countDownLatch = this.redisson.getCountDownLatch("anyCountDownLatch");
        System.out.println("释放存在：" + countDownLatch.isExists());
        int value = 50;
        boolean set = countDownLatch.trySetCount(value);
        System.out.println("尝试设置值：" + set);
        System.out.println("数量减少1");
        countDownLatch.countDown();
        System.out.println("当前总数：" + countDownLatch.getCount());

        //开循环 1 秒钟消费一个
        for (int i = 0; i < value; i++) {
            try {
                TimeUnit.SECONDS.sleep(1L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            countDownLatch.countDown();
            long count = countDownLatch.getCount();
            System.out.println("消费 1 个，还剩：" + count);
            if (count <= 0) {
                break;
            }
        }
        System.out.println("等待全部消费完毕，等待中...");
        countDownLatch.await();
        System.out.println("操作执行完毕");
    }

}
