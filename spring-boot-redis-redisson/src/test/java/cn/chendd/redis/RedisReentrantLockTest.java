package cn.chendd.redis;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import sun.management.VMManagement;

import javax.annotation.Resource;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 测试分布式可重入锁
 *
 * @author chendd
 * @date 2023/5/23 14:44
 */
@SuppressWarnings("all")
public class RedisReentrantLockTest extends BaseTest {

    @Resource
    private Redisson redisson;

    @Test
    public void tryLock() throws Exception {
        int pid = jvmPid();
        System.out.println(String.format("进程 %d 开始执行，时间：%s" , pid , getDateTime()));
        RLock lock = redisson.getLock("tryLock");
        boolean isLock = lock.tryLock();
        System.out.println(String.format("进程 %d 是否获取到锁：%b，时间：%s" , pid , isLock , getDateTime()));
        if (isLock) {
            try {
                System.out.println(String.format("进程 %d 开始幸苦干活 15 秒，时间：%s" , pid , getDateTime()));
                TimeUnit.SECONDS.sleep(15L);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                System.out.println(String.format("进程 %d 活干完了准备释放，时间：%s" , pid , getDateTime()));
            }
        } else {
            System.out.println(String.format("进程 %d 未获取到锁，时间：%s" , pid , getDateTime()));
        }
    }

    @Test
    public void tryLockTimeout() throws Exception {
        int pid = jvmPid();
        System.out.println(String.format("进程 %d 开始执行，时间：%s" , pid , getDateTime()));
        RLock lock = redisson.getLock("tryLockTimeout");
        boolean isLock = lock.tryLock(20 , TimeUnit.SECONDS);
        System.out.println(String.format("进程 %d 是否获取到锁：%b，时间：%s" , pid , isLock , getDateTime()));
        if (isLock) {
            try {
                System.out.println(String.format("进程 %d 开始幸苦干活 15 秒，时间：%s" , pid , getDateTime()));
                TimeUnit.SECONDS.sleep(15L);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                System.out.println(String.format("进程 %d 活干完了准备释放，时间：%s" , pid , getDateTime()));
            }
        } else {
            System.out.println(String.format("进程 %d 未获取到锁，时间：%s" , pid , getDateTime()));
        }
    }

    @Test
    public void tryDeadLock1() {
        RLock lock = redisson.getLock("tryDeadLock");
        lock.lock();
        System.out.println(String.format("进程 %s 获取到锁了，但不释放，其它进程无法再获得锁，验证内部的防死锁机制，时间：%s" , jvmPid() , getDateTime()));
        System.out.println(String.format("进程 %s 获取到锁了，锁的状态：%b" , jvmPid() , lock.isLocked()));
        try {
            TimeUnit.SECONDS.sleep(30L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(String.format("进程 %s 模拟退出，远程锁超时倒计时" , jvmPid()));
    }

    @Test
    public void tryDeadLock2() {
        RLock lock = redisson.getLock("tryDeadLock");
        while (! lock.tryLock()) {
            System.out.println(String.format("线程 %s 开始执行，不获得锁不罢休，时间：%s" , Thread.currentThread().getName() , getDateTime()));
            try {
                TimeUnit.SECONDS.sleep(1L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(String.format("终是由于防死锁（看门狗）机制，还是拿到了锁，时间：%s" , getDateTime()));
        lock.unlock();
    }

    /**
     * 模拟死锁，同一个进程中，获得锁的线程不释放，其它线程将无法再获取到锁
     * 大家都知道，如果负责储存这个分布式锁的Redis节点宕机以后，而且这个锁正好处于锁住的状态时，这个锁会出现锁死的状态。
     * 为了避免这种情况的发生，Redisson内部提供了一个监控锁的看门狗，它的作用是在Redisson实例被关闭前，不断的延长锁的有效期。
     * 默认情况下，看门狗的检查锁的超时时间是30秒钟，也可以通过修改Config.lockWatchdogTimeout来另行指定。
     * 另外Redisson还通过加锁的方法提供了leaseTime的参数来指定加锁的时间。超过这个时间后锁便自动解开了。
     */
    @Test
    public void tryDeadLockByThread() throws Exception {
        //新开启一个线程获得锁
        CountDownLatch countDownLatch = new CountDownLatch(1);
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                RLock lock = redisson.getLock("tryDeadLockByThread");
                //错误的代码逻辑
                lock.lock();
                countDownLatch.countDown();
                System.out.println(String.format("进程 %s 获取到锁了，但不释放，其它线程（进程）无法再获得锁，验证内部的防死锁机制" , jvmPid()));
            }
        });
        thread.start();

        countDownLatch.await();

        AtomicBoolean flag = new AtomicBoolean(false);
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    RLock lock = redisson.getLock("tryDeadLockByThread");
                    boolean locked = lock.tryLock();
                    if (locked) {
                        flag.set(true);
                        System.out.println(String.format("终是拿到了锁，时间：%s" , getDateTime()));
                        lock.unlock();
                        break;
                    }
                    System.out.println(String.format("线程 %s 开始执行，不获得锁不罢休，时间：%s" , Thread.currentThread().getName() , getDateTime()));
                    try {
                        TimeUnit.SECONDS.sleep(1L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        //未拿到锁继续循环等，防止主线程退出
        while (! flag.get()) {
            //当获取到锁后被释放了，主线程自然结束
            TimeUnit.SECONDS.sleep(2L);
        }
    }

    public static String getDateTime() {
        return DateFormatUtils.format(new Date() , DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.getPattern());
    }

    public static final int jvmPid() {
        try {
            RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
            Field jvm = FieldUtils.getField(runtime.getClass(), "jvm", true);
            VMManagement management = (VMManagement) jvm.get(runtime);
            return (int) MethodUtils.invokeMethod(management , true , "getProcessId");
        } catch (Exception e) {
            return -1;
        }
    }

}
