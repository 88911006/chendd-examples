package cn.chendd.redis;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试redis
 *
 * @author chendd
 * @date 2023/5/21 22:28
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Bootstrap.class , webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BaseTest {

    @Test
    public void contextLoad() {

    }

}