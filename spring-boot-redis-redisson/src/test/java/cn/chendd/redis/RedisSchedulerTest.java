package cn.chendd.redis;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.RedissonNode;
import org.redisson.api.CronSchedule;
import org.redisson.api.RScheduledExecutorService;
import org.redisson.api.RedissonClient;
import org.redisson.api.annotation.RInject;
import org.redisson.config.RedissonNodeConfig;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务测试类，持久化数据存储
 *
 * @author chendd
 * @date 2023/5/23 12:17
 */
public class RedisSchedulerTest extends BaseTest {

    @Resource
    private Redisson redisson;

    @Test
    public void scheduler() throws InterruptedException {
        RScheduledExecutorService executorService = redisson.getExecutorService("chenddExecutor");
        if (executorService.isShutdown()) {
            executorService.delete();
        }

        RedissonNodeConfig nodeConfig = new RedissonNodeConfig(this.redisson.getConfig());
        //设置工作线程池和线程优先级
        nodeConfig.setExecutorServiceWorkers(Collections.singletonMap("chenddExecutor", 5));
        RedissonNode node = RedissonNode.create(nodeConfig);
        node.start();

        executorService.scheduleWithFixedDelay(new RunnableTask("aaa") , 1L , 5L , TimeUnit.SECONDS);
        executorService.schedule(new RunnableTask("bbb"), CronSchedule.of("0/5 * * * * ?"));

        System.out.println("休眠 100 秒");
        TimeUnit.SECONDS.sleep(100L);
        node.shutdown();
        executorService.shutdown();
        this.redisson.shutdown();
    }

    public static class RunnableTask implements Runnable , Serializable {

        @RInject
        RedissonClient redissonClient;

        private String flag;

        public RunnableTask(String flag) {
            this.flag = flag;
        }

        @Override
        public void run() {
            System.out.println(flag + " 线程Name --- " + Thread.currentThread().getName() + " --- 当前时间：" +
                    DateFormatUtils.format(new Date() , DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.getPattern()));
        }
    }

}
