package cn.chendd.redis;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RBatch;
import org.redisson.config.Config;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 测试redis的基本信息
 *
 * @author chendd
 * @date 2023/5/21 22:30
 */
public class RedisContextBasicTest extends BaseTest {

    @Resource
    private Redisson redisson;

    @Test
    public void contextRedissonConfig() {
        Config config = this.redisson.getConfig();
        List<String> nodeAddresses = config.useClusterServers().getNodeAddresses();
        nodeAddresses.stream().forEach(System.out::println);
        System.out.println(JSONObject.toJSONString(config , true));
    }
}
