package cn.chendd.redis;

import org.apache.commons.lang3.StringUtils;
import org.redisson.spring.starter.RedissonProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 启动类
 *
 * @author chendd
 * @date 2023/5/16 15:40
 */
@SpringBootApplication
public class Bootstrap {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(Bootstrap.class, args);
        String[] names = applicationContext.getBeanDefinitionNames();
        List<String> list = Arrays.stream(names).filter(name -> StringUtils.containsIgnoreCase(name, "redis")).collect(Collectors.toList());
        list.forEach(name -> {
            System.out.println(name + "---" + applicationContext.getBean(name));
        });
    }


}
