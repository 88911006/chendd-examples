package cn.chendd.redis.configuration;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;


/**
 * Redis 配置类
 *
 * @author chendd
 * @date 2023/5/21 22:16
 */
@Configuration
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConfiguration {

    @Bean
    public StringRedisTemplate stringRedisTemplate(@Autowired RedisConnectionFactory factory) {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(factory);
        return stringRedisTemplate;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(@Autowired RedisConnectionFactory factory) {
        GenericFastJsonRedisSerializer fastjson = new GenericFastJsonRedisSerializer();
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setConnectionFactory(factory);
        template.setValueSerializer(fastjson);
        template.setHashValueSerializer(fastjson);
        template.afterPropertiesSet();
        return template;
    }

}
