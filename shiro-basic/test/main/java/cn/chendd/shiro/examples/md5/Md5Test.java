package cn.chendd.shiro.examples.md5;

import org.apache.shiro.codec.Hex;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;
import org.junit.Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author chendd
 * @date 2019/06/02 0:28
 * 测试Shiro中Md5的加密实现，<b>多次迭代粗的密文与1次+1次...N次的结果是不一样的</b>
 */
public class Md5Test {

    private String source = "cdd";//密文
    private String salt = "admin";//加盐
    private Integer hashIterations = 2;//迭代次数

    /*
     * 加盐加密的实现：cdd为密文，admin为加盐
     * 1、使用1个参数的加盐加密的字符串累加；
     * 2、使用2个参数的密文和加盐；
     * 3、使用3个参数的密文与加盐各加盐1次；
     */
    @Test
    public void testMd5(){
        //假设cdd为待加密字符，admin为盐，以下3种均结果相同
        String md51 = new Md5Hash("admincdd" ).toString();
        String result1 = new Md5Hash(md51 , "admin").toString();
        System.out.println("1)加盐两次后的密文：" + result1);
        //-----------------------------------------------------------------------------------
        String md52 = new Md5Hash("cdd" , "admin" ).toString();
        String result2 = new Md5Hash(md52 , "admin").toString();
        System.out.println("2)加盐两次后的密文：" + result2);
        //-----------------------------------------------------------------------------------
        String md53 = new Md5Hash("cdd" , "admin" ,1).toString();
        String result3 = new Md5Hash(md53 , "admin",1).toString();
        System.out.println("3)加盐两次后的密文：" + result3);

    }

    /**
     * 多次加盐加密的迭代实现：基于源码整理
     */
    @Test
    public void testMd5SaltHashIterationsSource() throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        byte saltByte[] = ByteSource.Util.bytes(salt).getBytes();
        digest.reset();
        digest.update(saltByte);
        byte[] hashed = digest.digest(source.getBytes());
        int iterations = hashIterations - 1; //already hashed once above
        for (int i = 0; i < iterations; i++) {
            digest.reset();
            hashed = digest.digest(hashed);
        }
        System.out.println("基于源码整理的多次加盐迭代结果：" + Hex.encodeToString(hashed));
    }

    /**
     * 加盐加密的多次迭代实现：API直接调用
     */
    @Test
    public void testMd5SaltHashIterationsAPI(){
        Md5Hash md5 = new Md5Hash(source , salt , hashIterations);
        System.out.println("基于API调用的多次加盐迭代结果：" + md5.toHex());
    }
}
