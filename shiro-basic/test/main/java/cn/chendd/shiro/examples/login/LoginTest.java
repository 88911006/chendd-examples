package cn.chendd.shiro.examples.login;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LoginTest {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    protected String username = "chendd";
    protected String password = "www.chendd.cn";
    protected Subject loginSubject;

    public void testLogin(Realm realm) {
        SecurityManager securityManager = new DefaultSecurityManager(realm);
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            loginSubject = securityManager.login(subject, token);
            if (loginSubject.isAuthenticated()) {
                logger.debug("用户已经认证");
            }
            logger.info("用户名 {} 认证成功", loginSubject.getPrincipal());
        } catch (AuthenticationException e) {
            logger.error("用户名 {} 认证失败", username, e);
        }

    }

}
