package cn.chendd.shiro.examples.login;

import cn.chendd.shiro.custom.LoginRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * @author chendd
 * 自定义登录验证器（模拟从数据库中获取账号登录）
 */
public class LoginRealmTest extends LoginTest {

    @Test
    public void testLoginRealm() {
        logger.info("###测试自定义认证器登录--用户名 {} ，密码 {} ###" , username , password);
        String resourcePath = "classpath:shiro/shiro-loginRealm.ini";
        Realm realm = new LoginRealm(resourcePath);
        SecurityManager securityManager = new DefaultSecurityManager(realm);
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            Subject loginSubject = securityManager.login(subject, token);
            if (loginSubject.isAuthenticated()) {
                logger.debug("用户已经认证");
            }
            logger.info("用户名 {} 认证成功", loginSubject.getPrincipal());
            logger.info("是否拥有角色manager：{}", loginSubject.hasRole("manager"));
            logger.info("是否拥有联系人查询权限：{}", loginSubject.isPermitted("concat:select"));

        } catch (AuthenticationException e) {
            logger.error("用户名 {} 认证失败", username, e);
        }
    }

}
