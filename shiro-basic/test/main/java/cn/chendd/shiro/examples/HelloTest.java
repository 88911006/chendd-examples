package cn.chendd.shiro.examples;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.text.PropertiesRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(JUnit4.class)
public class HelloTest {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    protected String username = "chendd";
    protected String password = "chendd123";

    @Test
    public void testLogin() {
        logger.info("###测试登录----用户名 {} ，密码 {} ###" , username , password);
        //默认路径为：classpath:shiro-users.properties
        PropertiesRealm realm = new PropertiesRealm();
        realm.onInit();
        SecurityManager securityManager = new DefaultSecurityManager(realm);
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            Subject loginSubject = securityManager.login(subject, token);
            if (loginSubject.isAuthenticated()) {
                logger.info("用户名 {} 认证成功", loginSubject.getPrincipal());
                boolean managerRole = loginSubject.hasRole("manager");
                logger.info("是否拥有指定角色[manager]：{}" , managerRole);
                boolean sayHelloPermitted = loginSubject.isPermitted("sayHello");
                logger.info("是否拥有指定权限[sayHello]：{}" , sayHelloPermitted);
            }
        } catch (AuthenticationException e) {
            logger.error("用户名 {} 认证失败", username, e);
            throw e;
        }
    }

}
