package cn.chendd.shiro.examples.login;

import org.apache.shiro.realm.Realm;
import org.apache.shiro.realm.text.IniRealm;
import org.junit.Test;

/**
 * @author chendd
 * 测试根据用户名与密码认证登录
 */
public class LoginUserTest extends LoginTest {

    @Test
    public void testLoginUser() {
        logger.info("###测试登录----用户名 {} ，密码 {} ###" , username , password);
        String resourcePath = "classpath:shiro/shiro-loginUser.ini";
        Realm realm = new IniRealm(resourcePath);
        super.testLogin(realm);//使用Ini配置文件读取参数的方式认证用户

    }

}
