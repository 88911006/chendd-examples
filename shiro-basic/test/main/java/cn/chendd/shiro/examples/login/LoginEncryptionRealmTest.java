package cn.chendd.shiro.examples.login;

import cn.chendd.shiro.custom.LoginEncryptionRealm;
import org.apache.shiro.realm.Realm;
import org.junit.Test;

/**
 * @author chendd
 * @date 2019/6/2 0:52
 * 测试登录时使用自定义加密规则的授权认证
 */
public class LoginEncryptionRealmTest extends LoginTest{

    @Test
    public void testLoginEncryptionRealm() {
        logger.info("###测试自定义认证器登录--加密认证###");
        Realm realm = new LoginEncryptionRealm();
        super.testLogin(realm);//使用自定义的方式认证用户
    }

}
