package cn.chendd.shiro.examples.permision;

import cn.chendd.shiro.examples.login.LoginTest;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.realm.text.IniRealm;
import org.junit.Test;

import java.util.Arrays;

/**
 * 角色权限简单测试，用户、角色、权限来源于ini文件
 * @author chendd
 */
public class PermisionTest extends LoginTest {

    @Test
    public void testPermisionByAdmin(){
        super.username = "admin";
        super.password = "admin123";
        logger.info("###测试用户角色权限认证--用户名 {} ，密码 {} ###" , username , password);
        String resourcePath = "classpath:shiro/shiro-role-permision.ini";
        Realm realm = new IniRealm(resourcePath);
        super.testLogin(realm);//使用自定义的方式认证用户
        //认证完以后的用户角色权限
        logger.debug("###验证{}拥有角色###" , username);
        boolean hasRoles[] = super.loginSubject.hasRoles(Arrays.asList("admin" , "coder" , "tester"));
        logger.info("{}用户拥有角色：" + Arrays.toString(hasRoles) , username);
        logger.warn("###验证admin权限，不限制权限###");
        logger.info("admin角色的用户管理新增权限：" + super.loginSubject.isPermitted("usermanager:insert"));
        logger.info("admin角色的用户管理未定义权限：" + super.loginSubject.isPermitted("usermanager:delete"));
        logger.info("admin角色的未定义角色的未定义权限：" + super.loginSubject.isPermitted("chendd:www.chendd.cn"));
    }

    @Test
    public void testPermisionByCoder(){
        super.username = "chendd";
        super.password = "chendd123";
        logger.info("###测试用户角色权限认证--用户名 {} ，密码 {} ###" , username , password);
        String resourcePath = "classpath:shiro/shiro-role-permision.ini";
        Realm realm = new IniRealm(resourcePath);
        super.testLogin(realm);//使用自定义的方式认证用户
        //认证完以后的用户角色权限
        logger.debug("###验证{}拥有角色###" , username);
        boolean hasRoles[] = super.loginSubject.hasRoles(Arrays.asList("admin" , "coder" , "tester"));
        logger.info("{}拥有角色：" + Arrays.toString(hasRoles) , username);
        logger.warn("###验证coder权限###");
        logger.info("coder角色的用户管理新增权限：" + super.loginSubject.isPermitted("usermanager:insert"));
        logger.info("coder角色的用户管理未定义权限：" + super.loginSubject.isPermitted("usermanager:select"));
        logger.warn("###验证tester权限###");
        logger.info("tester角色的权限：" + super.loginSubject.isPermitted("menumanager:insert"));
        logger.info("tester角色的权限：" + super.loginSubject.isPermitted("menumanager:test:test"));
    }

    @Test
    public void testPermisionByTester(){
        super.username = "tt";
        super.password = "tt123";
        logger.info("###测试用户角色权限认证--用户名 {} ，密码 {} ###" , username , password);
        String resourcePath = "classpath:shiro/shiro-role-permision.ini";
        Realm realm = new IniRealm(resourcePath);
        super.testLogin(realm);//使用自定义的方式认证用户
        //认证完以后的用户角色权限
        logger.warn("###验证tester权限###");
        logger.info("tester角色的权限：" + super.loginSubject.isPermitted("menumanager:insert"));
        logger.info("tester角色的权限：" + super.loginSubject.isPermitted("menumanager:test:test"));
    }

}
