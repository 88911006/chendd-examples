package cn.chendd.shiro.custom.examples;

/**
 * @Author chendd
 * @date 2019/6/1 19:07
 * 定义用户Dao，模拟数据库查询用户
 */
public class SysUserDao {

    /**
     * 默认数据库表中用户名为chendd，其它为不存在
     */
    public SysUser getUserByUserName(String userName) {
        if("chendd".equals(userName)){
            //假设密文为：密码www.chendd.cn加盐chendd迭代2次后的结果
            String password = "2ea79b57429e821ee55f861d1009d3eb";//假设这是按规则加密后存储的密码数据
            return new SysUser(1001 , userName , password , "www.chendd.cn");
        }
        return null;
    }

}
