package cn.chendd.shiro.custom.examples;

/**
 * @Author chendd
 * @Description 定义用户对象
 */
public class SysUser {

    private Integer userId;
    private String userName;
    private String password;
    private String url;

    public SysUser(Integer userId, String userName, String password, String url) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.url = url;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
