package cn.chendd.bless.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 好运实体对象
 *
 * @author chendd
 * @date 2023/1/20 16:11
 */
@TableName("test")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodLuck {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @TableField("id")
    private Long id;

    @TableField("name")
    private String name;

    @TableField("remark")
    private String remark;

}
