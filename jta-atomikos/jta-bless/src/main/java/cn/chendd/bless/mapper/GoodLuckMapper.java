package cn.chendd.bless.mapper;

import cn.chendd.bless.model.GoodLuck;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 好运Mapper接口定义
 *
 * @author chendd
 * @date 2023/1/20 16:12
 */
public interface GoodLuckMapper extends BaseMapper<GoodLuck> {


}
