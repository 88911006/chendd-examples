/*创建数据库*/
CREATE SCHEMA `Struts` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;

/**创建表*/
CREATE TABLE `test` (
  `id` varchar(32) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;