package cn.chendd.users.mapper;

import cn.chendd.users.model.Users;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户Mapper接口定义
 *
 * @author chendd
 * @date 2023/1/20 15:53
 */
public interface UsersMapper extends BaseMapper<Users> {

    /**
     * 查询用户
     * @return 用户对象
     */
    Users queryUsers();

}
