package cn.chendd.users.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 用户对象实体定义
 *
 * @author chendd
 * @date 2023/1/20 15:51
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("User")
public class Users {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @TableField("id")
    private Long id;

    @TableField("name")
    private String name;

    @TableField("sex")
    private String sex;

    @TableField("email")
    private String email;

    @TableField("createTime")
    private String createTime;

}
