/*创建数据库*/
CREATE SCHEMA `chendd-examples` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;

/**创建表*/
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(4) DEFAULT NULL COMMENT '性别枚举类SexEnum',
  `email` varchar(43) DEFAULT NULL COMMENT '邮箱',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';