package cn.chendd.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * 参数配置
 *
 * @author chendd
 * @date 2023/1/20 14:18
 */
@Configuration
@ImportResource(locations= {"classpath:spring-context.xml"})
public class ContextConfiguration {


}
