package cn.chendd;

import cn.chendd.users.mapper.UsersMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 启动类主入口
 *
 * @author chendd
 * @date 2023/1/20 13:45
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class Bootstrap {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(Bootstrap.class, args);
        UsersMapper usersMapper = applicationContext.getBean(UsersMapper.class);
        System.out.println(usersMapper.queryUsers());
    }

}
