package cn.chendd.transactional.controller;

import cn.chendd.transactional.service.TransactionalService;
import cn.chendd.users.model.Users;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 事务管理Controller
 *
 * @author chendd
 * @date 2023/1/20 16:38
 */
@RestController
public class TransactionalController {

    @Resource
    private TransactionalService transactionalService;

    @GetMapping("/commit")
    public void saveData() {
        this.transactionalService.saveData();
    }

    @GetMapping("/rollback/mybatis-plus")
    public void saveDataMyBatisPlus() {
        this.transactionalService.saveDataMyBatisPlus();
    }

    @GetMapping("/rollback/mybatis-plus-jdbc-template")
    public void saveDataMyBatisPlusOrJdbcTemplate() {
        this.transactionalService.saveDataMyBatisPlusOrJdbcTemplate();
    }

    @GetMapping("/users")
    public Users queryUsers() {
        return this.transactionalService.queryUsers();
    }

}
