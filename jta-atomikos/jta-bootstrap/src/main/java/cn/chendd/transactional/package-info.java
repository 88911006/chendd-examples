/**
 * 验证JTA事务管理
 */
@NonNullApi
@NonNullFields
package cn.chendd.transactional;

import org.springframework.lang.NonNullApi;
import org.springframework.lang.NonNullFields;