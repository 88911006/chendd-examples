package cn.chendd.transactional.service;

import cn.chendd.users.model.Users;

/**
 * 验证事务接口定义
 *
 * @author chendd
 * @date 2023/1/20 16:28
 */
public interface TransactionalService {

    /**
     * 保存数据对象
     */
    void saveData();

    /**
     * 保存数据对象
     */
    void saveDataMyBatisPlus();

    /**
     * 保存数据对象
     */
    void saveDataMyBatisPlusOrJdbcTemplate();

    /**
     * 查询一条用户数据
     * @return 用户数据
     */
    Users queryUsers();
}
