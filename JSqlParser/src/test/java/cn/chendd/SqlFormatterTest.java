package cn.chendd;

import cn.hutool.db.sql.SqlFormatter;
import com.manticore.jsqlformatter.JSQLFormatter;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.util.deparser.ExpressionDeParser;
import net.sf.jsqlparser.util.deparser.StatementDeParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.StringReader;

/**
 * SQL格式化
 *
 * @author chendd
 * @date 2023/7/29 15:56
 */
@RunWith(JUnit4.class)
public class SqlFormatterTest {

    final String INSERT_SQL =
            "UPDATE risk.counterparty SET id_counterparty = :id_counterparty\n" +
                    "    , label = :label , description = :description , id_counterparty_group_type = :id_counterparty_group_type\n" +
                    "    , id_counterparty_type = :id_counterparty_type\n" +
                    "    , id_counterparty_sub_type = :id_counterparty_sub_type\n" +
                    "    , id_country_group = :id_country_group\n" +
                    "    , id_country = :id_country, id_country_state = :id_country_state , id_district = :id_district\n" +
                    "    , id_city = :id_city\n" +
                    "    , id_industrial_sector = :id_industrial_sector\n" +
                    "    , id_industrial_sub_sector = :id_industrial_sub_sector\n" +
                    "    , block_auto_update_flag = :block_auto_update_flag\n" +
                    "    , id_user_editor = :id_user_editor\n" +
                    "    , id_organization_unit = :id_organization_unit , id_status = :id_status\n" +
                    "    , update_timestamp = current_timestamp WHERE id_counterparty_ref = :id_counterparty_ref";

    /**
     * 格式化SQL语句为Java 定义变量的规范
     * @throws Exception 异常处理
     */
    @Test
    public void formatToJava() throws Exception {
        System.out.println(INSERT_SQL);
        System.out.println();
        System.out.println(JSQLFormatter.formatToJava(INSERT_SQL, 4));
    }

    /**
     * 格式化SQL为tree结构的描述
     * @throws Exception 异常处理
     */
    @Test
    public void formatToTree1() throws Exception {
        String sql = "select '11' a , '22' b , c , to_date('20230729' , 'yyyymmdd') d from dual";
        System.out.println(JSQLFormatter.formatToTree(sql));
    }

    @Test
    public void formatToTree2() throws Exception {
        System.out.println(JSQLFormatter.formatToTree(INSERT_SQL));
    }

    @Test
    public void formatToTree3() throws Exception {
        String sql =
                "UPDATE cfe.calendar\n" +
                "SET year_offset = ?            /* year offset */\n" +
                "    , settlement_shift = ?     /* settlement shift */\n" +
                "    , friday_is_holiday = ?    /* friday is a holiday */\n" +
                "    , saturday_is_holiday = ?  /* saturday is a holiday */\n" +
                "    , sunday_is_holiday = ?    /* sunday is a holiday */\n" +
                "WHERE id_calendar = ?";
        System.out.println(JSQLFormatter.formatToTree(sql));
    }

    @Test
    public void format1() throws Exception {
        System.out.println(JSQLFormatter.format(INSERT_SQL));
    }

    @Test
    public void format2() throws Exception {
        String sql =
                "UPDATE cfe.calendar SET year_offset = ? , settlement_shift = ?  , friday_is_holiday = ?   , " +
                        "saturday_is_holiday = ?  , sunday_is_holiday = ? WHERE id_calendar = ? ";
        System.out.println(JSQLFormatter.format(sql));
    }

    @Test
    public void format2Html() {
        String sql =
                "UPDATE cfe.calendar SET year_offset = ? , settlement_shift = ?  , friday_is_holiday = ?   , " +
                        "saturday_is_holiday = ?  , sunday_is_holiday = ? WHERE id_calendar = ? " +
                        "and UPDATE_TIME = 1";
        String format = SqlFormatter.format(sql);
        String[] keywords = {"UPDATE", "SET", "WHERE"};
        // 对关键字进行高亮替换
        for (String keyword : keywords) {
            format = format.replaceAll("(?i)\\b" + keyword + "\\b", "<span class=\"highlight\">" + keyword + "</span>");
        }
        System.out.println("<style>" +
                ".highlight {" +
                "    font-weight: bold;" +
                "    color: BLUE;" +
                "}" +
                "</style>\n" +
                "<pre>" + format + "</pre>");

    }

}
