package cn.chendd;


import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.select.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * sql解析器测试
 *
 * @author chendd
 * @date 2023/7/29 16:26
 */
@RunWith(JUnit4.class)
public class SqlParserTest {

    /**
     * SQL语句生成
     * SELECT *, 'chendd' AS name, website FROM users t1
     */
    @Test
    public void createSql() {
        Select select = new Select();
        PlainSelect plainSelect = new PlainSelect();
        Table table = new Table("users").withAlias(new Alias("t1", false));

        List<SelectItem> list = new ArrayList<>();
        list.add(new AllColumns());
        list.add(new SelectExpressionItem(new StringValue("chendd")).withAlias(new Alias("name" , true)));
        list.add(new SelectExpressionItem(new Column("website")));
        plainSelect.setSelectItems(list);
        plainSelect.setFromItem(table);
        select.setSelectBody(plainSelect);
        System.out.println(select.toString());
    }

    /**
     * 获取脚本文件的执行类型和语句
     */
    @Test
    public void parseSql() throws Exception {
        Path path = new File(getClass().getResource("/script.sql").getFile()).toPath();
        String sql = new String(Files.readAllBytes(path));
        Statements statements = CCJSqlParserUtil.parseStatements(sql);
        List<Statement> list = statements.getStatements();
        for (Statement statement : list) {
            System.out.println(statement.getClass().getSimpleName() + "：" + statement.toString());
        }
    }

    /**
     * 去除Order语句和Group
     * 输出【SELECT id, name, sum(age) age FROM users WHERE age > 18 GROUP BY id, name】
     */
    @Test
    public void removeOrder() throws Exception {
        String sql = "SELECT id, name , sum(age) age FROM users WHERE age > 18 group by id , name ORDER BY name ASC , age DESC";
        Select select = (Select) CCJSqlParserUtil.parse(sql);
        PlainSelect selectBody = (PlainSelect) select.getSelectBody();
        List<OrderByElement> orderElements = selectBody.getOrderByElements();
        System.out.println(orderElements);
        // 移除排序部分
        selectBody.setOrderByElements(null);
        //  移除分组部分
        /*plainSelect.setGroupByElement(null);*/
        System.out.println(select.toString());
    }


}
