package cn.chendd;

import cn.hutool.db.sql.SqlFormatter;
import com.manticore.jsqlformatter.JSQLFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * hutool工具类SQL格式化
 * 无视SQL语句的格式语法
 *
 * @author chendd
 * @date 2023/7/29 15:56
 */
@RunWith(JUnit4.class)
public class HutoolSqlFormatterTest {

    final String INSERT_SQL_1 = "INSERT INTO `chendd-blog`.`quartz_triggers`(`SCHED_NAME`,\n" +
            "`TRIGGER_NAME`,`TRIGGER_GROUP`,\n" +
            "`JOB_NAME`,`JOB_GROUP`,`DESCRIPTION`,`NEXT_FIRE_TIME`,`PREV_FIRE_TIME`,\n" +
            "`PRIORITY`,`TRIGGER_STATE`,`TRIGGER_TYPE`,\n" +
            "`START_TIME`,`END_TIME`,`CALENDAR_NAME`,`MISFIRE_INSTR`,\n" +
            "`JOB_DATA`)\n" +
            "VALUES\n" +
            "(<{SCHED_NAME: }>,\n" +
            "<{TRIGGER_NAME: }>,<{TRIGGER_GROUP: }>,<{JOB_NAME: }>,<{JOB_GROUP: }>,\n" +
            "<{DESCRIPTION: }>,\n" +
            "<{NEXT_FIRE_TIME: }>,<{PREV_FIRE_TIME: }>,\n" +
            "<{PRIORITY: }>,<{TRIGGER_STATE: }>,<{TRIGGER_TYPE: }>,\n" +
            "<{START_TIME: }>,\n" +
            "<{END_TIME: }>,<{CALENDAR_NAME: }>,<{MISFIRE_INSTR: }>,<{JOB_DATA: }>);\n";

    final String INSERT_SQL_2 =
            "INSERT INTO risk.tmp_ccf (\n" +
            "    \"ID_INSTRUMENT\"\n" +
            "    , \"TENOR\"\n" +
            "    , \"STATUS\"\n" +
            "    , \"OBSERVATION_DATE\"\n" +
            "    , \"BALANCE\"\n" +
            "    , \"LIMIT\"\n" +
            "    , \"DR_BALANCE\"\n" +
            "    , \"OPEN_LIMIT\" )\n" +
            "SELECT  '1000042339'       /* ID_INSTRUMENT */\n" +
            "        , 0                /* TENOR */\n" +
            "        , 'DEFAULT'        /* STATUS */\n" +
            "        , {d '2020-02-27'} /* OBSERVATION_DATE */\n" +
            "        , - 142574953.65   /* BALANCE */\n" +
            "        , 300000000        /* LIMIT */\n" +
            "        , - 142574953.65   /* DR_BALANCE */\n" +
            "        , 157425046.35     /* OPEN_LIMIT */\n" +
            "FROM dual\n" +
            ";";

    @Test
    public void hutool1() {
        System.out.println(INSERT_SQL_1);
        System.out.println(SqlFormatter.format(INSERT_SQL_1));
    }

    @Test
    public void hutool2() {
        System.out.println(INSERT_SQL_2);
        System.out.println(SqlFormatter.format(INSERT_SQL_2));
    }

}
