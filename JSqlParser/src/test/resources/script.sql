-- CREATE TABLE CFE.INTEREST_PERIOD
CREATE TABLE cfe.interest_period (
    id_instrument                VARCHAR (40)    NOT NULL
    , id_fixingmode              NUMBER (5)      DEFAULT 0 NOT NULL
    , fixing_date                DATE
    , change_date                DATE
    , base_rate                  VARCHAR (12)
    , base_margin_rate           DECIMAL (12,9)
    , par_rate                   VARCHAR (12)
    , par_margin_rate            DECIMAL (12,9)
    , id_payment_convention      VARCHAR (12)
    , id_day_count_convention    VARCHAR (12)
    , id_day_incl_convention     VARCHAR (12)
    , fix_amount                 DECIMAL (23,5)
    , id_currency_fix_amount     VARCHAR (3)
    , id_script                  VARCHAR (12)
)
;

-- UNIQUE
CREATE UNIQUE INDEX cfe.interest_period_idx1
    ON cfe.interest_period( id_instrument, change_date )
;

-- ALTER TABLE DROP COLUMN
ALTER TABLE risk.collateral
    DROP COLUMN id_status;

-- ORACLE ALTER TABLE ADD COLUMN
ALTER TABLE risk.collateral
    ADD id_status VARCHAR (1) NULL
;

-- 查询SQL
SELECT DISTINCT
            a.id_collateral , 'A' AA , ' ; ' BB
        FROM cfe.collateral a
            LEFT JOIN cfe.collateral_ref b
                ON a.id_collateral = b.id_collateral
        WHERE b.id_collateral_ref IS NULL;

-- MERGE 3
MERGE INTO cfe.instrument_import_measure imp
    USING s
        ON ( imp.id_instrument = s.id_instrument
                AND imp.measure = 'YIELD_P'
                AND imp.id_instrument = s.id_instrument
                AND imp.measure = 'YIELD_P' )
WHEN MATCHED THEN
    UPDATE SET  imp.value = s.yield
;

-- INSERT RATIO COLLECTION RATIOS
INSERT INTO risk.counterparty_ratio
VALUES ( ?, ?, ? );

-- UPDATE COLLATERAL_TYPE
UPDATE common.collateral_type
SET hair_cut = least
WHERE id_collateral_type_ref IN (   SELECT id_collateral_type_ref
                                    FROM common.collateral_type a
                                    WHERE id_status IN (    'C', 'H', 'C'
                                                            , 'H', 'C', 'H'
                                                            , 'C', 'H' )
);

-- UPDATE START JOINS
UPDATE sc_borrower b
    INNER JOIN sc_credit_apply a
        ON a.borrower_id = b.id
SET b.name = '0.7505105896846266'
    , a.credit_line = a.credit_line + 1
WHERE b.id = 3;

------------------------------------------------------------------------------------------------------------------------
-- CONFIGURATION
------------------------------------------------------------------------------------------------------------------------

UPDATE cfe.calendar
SET     year_offset = ?                    /* year offset */
        , settlement_shift = To_Char( ? )  /* settlement shift */
        , friday_is_holiday = ?            /* friday is a holiday */
        , saturday_is_holiday = ?          /* saturday is a holiday */
        , sunday_is_holiday = ?            /* sunday is a holiday */
WHERE id_calendar = ?;

COMMIT;

--增加表注释
comment on table CHENDD is '表注释';

--增加字段注释
comment on column CHENDD.id is '主键ID';

DROP TABLE `www.chendd.cn`;

TRUNCATE TABLE `www.chendd.cn`;

DELETE FROM `www.chendd.cn`;
