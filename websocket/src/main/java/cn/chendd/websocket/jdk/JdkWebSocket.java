package cn.chendd.websocket.jdk;

import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * websocket实现
 *
 * @date 2023/6/2 18:02
 */
@Component
@ServerEndpoint(value = "/websocket/jdk")
public class JdkWebSocket {

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocketConfig.onOpen");
    }

    @OnMessage
    public void onMessage(Session session , String message) {
        System.out.println("WebSocketConfig.onMessage-->" + session + "--->" + message);
    }

    @OnClose
    public void onClose() {
        System.out.println("WebSocketConfig.onClose");
    }

    @OnError
    public void onError(Session sesison , Throwable throwable) {
        System.out.println("WebSocketConfig.onError-->" + sesison + "--->" + throwable);
    }

}
