package cn.chendd.websocket.spring;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * SpringWebSocketHandler
 *
 * @author chendd
 * @date 2023/6/2 22:08
 */
@Component
public class SpringWebSocketHandler extends TextWebSocketHandler {

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        System.out.println("SpringWebSocketHandler.afterConnectionEstablished");
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        super.handleMessage(session, message);
        System.out.println("SpringWebSocketHandler.handleMessage");
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        super.handleTextMessage(session, message);
        System.out.println("SpringWebSocketHandler.handleTextMessage");
    }

    @Override
    protected void handlePongMessage(WebSocketSession session, PongMessage message) throws Exception {
        super.handlePongMessage(session, message);
        System.out.println("SpringWebSocketHandler.handlePongMessage");
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        super.handleTransportError(session, exception);
        System.out.println("SpringWebSocketHandler.handleTransportError");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        System.out.println("SpringWebSocketHandler.afterConnectionClosed");
    }

    @Override
    public boolean supportsPartialMessages() {
        System.out.println("SpringWebSocketHandler.supportsPartialMessages");
        return super.supportsPartialMessages();
    }
}
