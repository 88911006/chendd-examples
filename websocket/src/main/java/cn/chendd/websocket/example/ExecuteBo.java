package cn.chendd.websocket.example;

/**
 * 数据对象
 *
 * @author chendd
 * @date 2023/6/3 21:04
 */
public class ExecuteBo {

    private String code;
    private Integer value;

    public ExecuteBo(Integer value, String code) {
        this.value = value;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
