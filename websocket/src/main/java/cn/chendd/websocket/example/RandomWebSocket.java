package cn.chendd.websocket.example;

import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;

/**
 * 随机显示
 *
 * @author chendd
 * @date 2023/6/3 21:02
 */
@Component
@ServerEndpoint(value = "/websocket/random")
public class RandomWebSocket {

    @OnOpen
    public void onOpen(Session session) throws IOException {
        Response<List<ExecuteBo>> response = ExecuteService.getDataCompleteRandom();
        session.getBasicRemote().sendText(JSON.toJSONString(response));

        System.out.println("RandomWebSocket.onOpen--->" + session);
    }

    @OnMessage
    public void onMessage(Session session , String message) {
        System.out.println("WebSocketConfig.onMessage-->" + session + "--->" + message);
    }

    @OnClose
    public void onClose() {
        System.out.println("WebSocketConfig.onClose");
    }

    @OnError
    public void onError(Session sesison , Throwable throwable) {
        System.out.println("WebSocketConfig.onError-->" + sesison + "--->" + throwable);
    }

}
