package cn.chendd.websocket.example;

/**
 * Enum
 *
 * @author chendd
 * @date 2023/6/3 21:05
 */
public enum ExecuteEnum {

    /**
     *
     */
    Number_1(1 , "&#x1F0B1;"),
    Number_2(2 , "&#x1F0B2;"),
    Number_3(3 , "&#x1F0B3;"),
    Number_4(4 , "&#x1F0B4;"),
    Number_5(5 , "&#x1F0B5;"),
    Number_6(6 , "&#x1F0B6;"),
    Number_7(7 , "&#x1F0B7;"),
    Number_8(8 , "&#x1F0B8;"),
    Number_9(9 , "&#x1F0B9;"),
    Number_10(10 , "&#x1F0BA;"),
    Number_11(11 , "&#x1F0BB;"),
    Number_12(12 , "&#x1F0BD;"),
    Number_13(13 , "&#x1F0BE;"),
    Number_14(14 , "&#x1F0DF;"),
    Number_15(15 , "&#x1F0BF;"),

    ;

    private Integer value;

    private String code;

    ExecuteEnum(Integer value , String code) {
        this.value = value;
        this.code = code;
    }

    public Integer getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }
}
