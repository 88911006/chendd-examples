package cn.chendd.websocket.example;

import com.google.common.collect.Lists;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Service
 *
 * @author chendd
 * @date 2023/6/3 21:04
 */
public class ExecuteService {

    public static Response<List<ExecuteBo>> getDataCompleteOrder() {
        List<ExecuteBo> resultList = Lists.newArrayList();
        ExecuteEnum[] values = ExecuteEnum.values();
        List<ExecuteEnum> list = Lists.newArrayList(values);
        for (ExecuteEnum executeEnum : list) {
            resultList.add(new ExecuteBo(executeEnum.getValue() , executeEnum.getCode()));
        }
        return new Response<>(TypeEnum.Complete , resultList) ;
    }

    public static Response<List<ExecuteBo>> getDataCompleteRandom() {
        List<ExecuteBo> resultList = Lists.newArrayList();
        ExecuteEnum[] values = ExecuteEnum.values();
        List<ExecuteEnum> list = Lists.newArrayList(values);
        Iterator<ExecuteEnum> it = Lists.newArrayList(list).iterator();
        Random random = new Random();
        while (it.hasNext()) {
            it.next();
            int value = random.nextInt(list.size());
            ExecuteEnum executeEnum = list.get(value);
            list.remove(executeEnum);
            resultList.add(new ExecuteBo(executeEnum.getValue() , executeEnum.getCode()));
        }
        return new Response<>(TypeEnum.Complete , resultList) ;
    }

}
