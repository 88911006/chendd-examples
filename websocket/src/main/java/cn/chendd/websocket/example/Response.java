package cn.chendd.websocket.example;

/**
 * 数据对象
 *
 * @author chendd
 * @date 2023/6/3 21:53
 */
public class Response<T> {

    private TypeEnum type;

    private T data;

    public Response() {

    }

    public Response(TypeEnum type, T data) {
        this.type = type;
        this.data = data;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
