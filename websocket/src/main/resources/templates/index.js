$(function() {
    var webSocket = new WebSocket("ws://127.0.0.1:8080/websocket/random");

    webSocket.onopen = function (evt) {

    };

    webSocket.onmessage = function (evt) {
        var result = JSON.parse(evt.data);
        if (result.type === "Complete") {
            var data = result.data;
            var html = "" , template = "<li><div value='${value}'>${code}</div></li>";
            for (var i=0 ; i < data.length ; i++) {
                var item = data[i];
                html = html + template.replace("${value}" , item.value).replace("${code}" , item.code);
            }
            $(".source").append(html);
            initBindEvent();
            return;
        }
    };

    webSocket.onclose = function (evt) {
        console.log("onClose" , evt);
    };

    webSocket.onerror = function (evt) {
        console.log("onError" , evt);
    };
});

function initBindEvent() {
    //源元素绑定事件
    document.querySelector(".source").onselectstart = function(){return false;};
    var sourceElements = document.querySelectorAll(".source div");
    for (var i=0 ; i < sourceElements.length ; i++) {
        var element = sourceElements[i];
        (function(elt) {
            elt.onmousedown = function () {
                sourceMove(event , elt);
            };
            elt.onselectstart = function(){return false;}; //取消字段选择功能
        })(element);
    }
}

function sourceMove(event , element) {
    window.mouseDown = true;
    window.moveElement = element;
    window.oldX = event.clientX - element.offsetLeft;
    window.oldY = event.clientY - element.offsetTop;
}

$(function () {

    document.onselectstart = function(){return false;}

    window.onmousemove = function (e) {
        if (! window.mouseDown) {
            return;
        }

        console.log("移动中");
        window.newX = e.clientX;
        window.newY = e.clientY;
        window.moveElement.style.position = "absolute";
        window.moveElement.style.left = (newX - oldX) + "px";
        window.moveElement.style.top = (newY - oldY) + "px";
        //放置拖动到 上、左、右、下 以外的区域
        var left = window.moveElement.offsetLeft;
        var top = window.moveElement.offsetTop;
        if (left < 0) {
            window.moveElement.style.left = "0px";
        }
        if (top < 0) {
            window.moveElement.style.top = "0px";
        }
        var width = window.moveElement.offsetWidth;
        if (left + width > document.body.clientWidth) {
            window.moveElement.style.left = document.body.clientWidth - width + "px";
        }
        var height = window.moveElement.scrollHeight;
        console.log(document.body.clientHeight);
        console.log(document.body.offsetHeight );
        console.log(document.body.scrollHeight);
        console.log(window.screen.availHeight);
        console.log(window.screen.height);
        console.log(top + "," + height + "," + document.body.scrollHeight);
        if (top + height > document.body.offsetHeight) {
            window.moveElement.style.top = document.body.scrollHeight - height + "px";
        }

    };

    window.onmouseup = function (e) {
        window.mouseDown = false;
        window.moveElement = false;
    };
});

/**
 * 顺序排列
 */
function orderDispose() {
    var source = [];
    $(".source li").each(function() {
        source.push($(this));
    });
    //循环数据，将数据转换为三行五列
    var length = source.length;
    for (var i=0 ; i < length ; i=i+3) {
        $(".target_1").append($(source[i]));
        $(".target_2").append($(source[i + 1]));
        $(".target_3").append($(source[i + 2]));
    }
}

/**
 * 转换为按1排列【3-->1-->2】
 */
function convert1Dispose() {
    var target1 = [] , target2 = [] , target3 = [];
    $(".target li").each(function(i , item) {
        if (i + 1 <= 5) {
            target1.push(item);
        } else if (i + 1 <= 10) {
            target2.push(item);
        } else {
            target3.push(item);
        }
    });
    //获取所有数据
    $(".target").empty();
    var source = [];
    for (var i=0 ; i < target3.length ; i++) {
        source.push($(target3[i]));
    }
    for (var i=0 ; i < target1.length ; i++) {
        source.push($(target1[i]));
    }
    for (var i=0 ; i < target2.length ; i++) {
        source.push($(target2[i]));
    }
    //循环数据，将数据转换为三行五列
    var length = source.length;
    for (var i=0 ; i < length ; i=i+3) {
        $(".target_1").append($(source[i]));
        $(".target_2").append($(source[i + 1]));
        $(".target_3").append($(source[i + 2]));
    }
}

/**
 * 转换为按2排列【1-->2-->3】
 */
function convert2Dispose() {
    var target1 = [] , target2 = [] , target3 = [];
    $(".target li").each(function(i , item) {
        if (i + 1 <= 5) {
            target1.push(item);
        } else if (i + 1 <= 10) {
            target2.push(item);
        } else {
            target3.push(item);
        }
    });
    //获取所有数据
    $(".target").empty();
    var source = [];
    for (var i=0 ; i < target1.length ; i++) {
        source.push($(target1[i]));
    }
    for (var i=0 ; i < target2.length ; i++) {
        source.push($(target2[i]));
    }
    for (var i=0 ; i < target3.length ; i++) {
        source.push($(target3[i]));
    }
    //循环数据，将数据转换为三行五列
    var length = source.length;
    for (var i=0 ; i < length ; i=i+3) {
        $(".target_1").append($(source[i]));
        $(".target_2").append($(source[i + 1]));
        $(".target_3").append($(source[i + 2]));
    }
}

/**
 * 转换为按3排列【1-->3-->2】
 */
function convert3Dispose() {
    var target1 = [] , target2 = [] , target3 = [];
    $(".target li").each(function(i , item) {
        if (i + 1 <= 5) {
            target1.push(item);
        } else if (i + 1 <= 10) {
            target2.push(item);
        } else {
            target3.push(item);
        }
    });
    //获取所有数据
    $(".target").empty();
    var source = [];
    for (var i=0 ; i < target1.length ; i++) {
        source.push($(target1[i]));
    }
    for (var i=0 ; i < target3.length ; i++) {
        source.push($(target3[i]));
    }
    for (var i=0 ; i < target2.length ; i++) {
        source.push($(target2[i]));
    }
    //循环数据，将数据转换为三行五列
    var length = source.length;
    for (var i=0 ; i < length ; i=i+3) {
        $(".target_1").append($(source[i]));
        $(".target_2").append($(source[i + 1]));
        $(".target_3").append($(source[i + 2]));
    }
}