package cn.chendd.forest;

import com.thebeastshop.forest.springboot.annotation.ForestScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author chendd
 * @date 2021/4/26 13:04
 */
@ForestScan(basePackages = {"cn.chendd.**.client"})
@SpringBootApplication
public class Bootstrap {

    /**
     * 主方法
     * @param args 启动参数
     */
    public static void main(String[] args) {
        SpringApplication.run(Bootstrap.class, args);
    }

}
