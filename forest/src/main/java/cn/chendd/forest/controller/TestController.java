package cn.chendd.forest.controller;

import cn.chendd.forest.client.HelloClient;
import cn.chendd.forest.client.TagClient;
import cn.chendd.forest.vo.TagManageResult;
import cn.chendd.result.BaseResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 测试Controller
 *
 * @author chendd
 * @date 2021/4/26 13:08
 */
@RestController
public class TestController {

    @Resource
    private HelloClient helloClient;
    @Resource
    private TagClient tagClient;

    @GetMapping("/test")
    public BaseResult<List<Point>> test() {
        BaseResult<List<Point>> result = this.helloClient.getAllList();
        return result;
    }

    @GetMapping("/testTag")
    public BaseResult<List<TagManageResult>> testTag() {
        BaseResult<List<TagManageResult>> result = this.tagClient.getTagList();
        return result;
    }

    @GetMapping("/api")
    public BaseResult<List<Point>> getAllList() {
        List<Point> dataList = new ArrayList<Point>();
        dataList.add(new Point(10 , 100));
        dataList.add(new Point(20 , 200));
        dataList.add(new Point(30 , 300));
        dataList.add(new Point(40 , 400));
        dataList.add(new Point(50 , 500));
        return new BaseResult<List<Point>>(dataList);
    }

}
