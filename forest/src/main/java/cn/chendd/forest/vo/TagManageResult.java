package cn.chendd.forest.vo;

/**
 * @author chendd
 * @date 2020/7/15 10:42
 */
public class TagManageResult {
    
    private Long id;

    private String tag;

    private Strong strong;

    private String sortOrder;

    private Integer counts;

    private String style;

    public static class Strong {

        private String text , value;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Strong getStrong() {
        return strong;
    }

    public void setStrong(Strong strong) {
        this.strong = strong;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getCounts() {
        return counts;
    }

    public void setCounts(Integer counts) {
        this.counts = counts;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
