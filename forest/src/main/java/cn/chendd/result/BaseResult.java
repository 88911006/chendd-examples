package cn.chendd.result;

/**
 * 接口返回类型封装类
 *
 * @author chendd
 * @date 2021/4/26 13:09
 */
public class BaseResult<T> {

    private String result;
    private String message;
    private T data;

    public BaseResult() {

    }

    public BaseResult(T data) {
        this.data = data;
        result = "success";
        this.message = null;
    }

    public BaseResult(String result, String message, T data) {
        this.result = result;
        this.message = message;
        this.data = data;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
