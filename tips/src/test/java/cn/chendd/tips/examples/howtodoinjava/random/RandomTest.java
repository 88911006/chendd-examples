package cn.chendd.tips.examples.howtodoinjava.random;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Random;
import java.util.SplittableRandom;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机数生成
 *
 * @author chendd
 * @date 2023/4/28 9:10
 */
@RunWith(JUnit4.class)
public class RandomTest {

    @Test
    public void math() {
        System.out.println("随机数 0 - 1 之间：" + Math.random());
        System.out.println("随机数 0 - 1000 之间：" + (int) (Math.random() * 1000));
    }

    @Test
    public void random() {
        Random random = new Random();
        System.out.println("随机数：" + random.nextInt());
        System.out.println("随机 0 - 1000 之间：" + random.nextInt(1000));
    }

    @Test
    public void threadLocalRandom() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        System.out.println("随机数：" + random.nextInt());
        System.out.println("随机 0 - 1000 之间：" + random.nextInt(1000));
    }

    @Test
    public void splittableRandom() {
        SplittableRandom random = new SplittableRandom();
        System.out.println("随机数：" + random.nextInt());
        System.out.println("随机 0 - 1000 之间：" + random.nextInt(1000));
    }

    @Test
    public void uuid() {
        System.out.println("UUID：" + UUID.randomUUID().toString());
    }

    @Test
    public void hutool() {
        System.out.println("randomUUID：" + IdUtil.randomUUID());
        System.out.println("simpleUUID：" + IdUtil.simpleUUID());
        System.out.println("fastSimpleUUID：" + IdUtil.fastSimpleUUID());
        for (int i = 1 ; i <= 5 ; i++) {
            System.out.println("objectId：" + IdUtil.objectId());
        }
        Snowflake snowflake = IdUtil.getSnowflake();
        for (int i = 1 ; i <= 5 ; i++) {
            System.out.println("snowflake：" + snowflake.nextId());
        }
    }

    @Test
    public void mybatisPlus() {
        for (int i = 1 ; i <= 5 ; i++) {
            System.out.println("getId：" + IdWorker.getId());
        }
        System.out.println("get32UUID：" + IdWorker.get32UUID());
        System.out.println("getTimeId：" + IdWorker.getTimeId());
    }

}
