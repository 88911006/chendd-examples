package cn.chendd.tips.examples.varible;

import org.junit.Test;
import java.lang.reflect.Parameter;

/**
 * 使用JDK8新特性实现
 * @author chendd
 * @date 2019-08-08 21:39:20
 */
public class GetDefinitionByJDK8 extends BaseTest {

    public GetDefinitionByJDK8() throws NoSuchMethodException {
    }

    @Test
    public void testJDKVariable() {
        Parameter[] parameters = method.getParameters();
        for (Parameter parameter : parameters) {
            System.out.println(parameter);
        }
    }

}
