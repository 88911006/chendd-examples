package cn.chendd.tips.examples.genericity;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.junit.Test;

import java.lang.reflect.*;
import java.util.List;

/**
 * @author chendd
 */
public class GetDefinitionByJDK8 extends BastTest {

    @Test
    public void testField() throws Exception {
        Field field = FieldUtils.getField(super.getClass() , "pointList" , true);
        System.out.println("成员变量类型与泛型：" + field.getGenericType());
        ParameterizedType type = (ParameterizedType) field.getGenericType();
        for (Type argument : type.getActualTypeArguments()) {
            System.out.println("泛型：" + argument.getTypeName());
        }
    }

    @Test
    public void testReturnList(){
        Method method = MethodUtils.getAccessibleMethod(super.getClass() , "queryForList");
        ParameterizedType type = (ParameterizedType) method.getGenericReturnType();
        System.out.println("返回值类型与泛型：" + type);
        for (Type argument : type.getActualTypeArguments()) {
            System.out.println("泛型：" + argument.getTypeName());
        }
    }

    @Test
    public void testParamList(){
        Method method = MethodUtils.getAccessibleMethod(super.getClass() , "size" , List.class);
        Parameter[] parameters = method.getParameters();
        for (Parameter parameter : parameters) {
            ParameterizedType type = (ParameterizedType) parameter.getParameterizedType();
            System.out.println("方法参数类型与泛型：" + type);
            for (Type argument : type.getActualTypeArguments()) {
                System.out.println("泛型：" + argument.getTypeName());
            }
        }
    }



}
