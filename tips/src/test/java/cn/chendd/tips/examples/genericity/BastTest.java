package cn.chendd.tips.examples.genericity;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class BastTest {

    protected List<Point> pointList;

    public List<Point> queryForList(){
        List<Point> points = new ArrayList<Point>();
        points.add(new Point(1 , 1));
        points.add(new Point(2 , 2));
        points.add(new Point(3 , 3));
        return points;
    }

    public int size(List<Point> xList){
        return xList.size();
    }

}
