package cn.chendd.tips.examples.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author chendd
 * @since 2019-08-30 23:27
 * 数据库查询结果ResultSet结果集代理
 */
public class MapProxyHandler implements InvocationHandler {

    private Map<String , Object> map;

    public MapProxyHandler(Map<String , Object> map){
        this.map = map;
    }

    @Override
    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        String suffixs[] = {"get" , "is"};
        for (String suffix : suffixs) {
            if(methodName.startsWith(suffix)){
                //去掉get或is开头前缀，将剩余部分首字母小写
                int lens = suffix.length();
                String name = methodName.substring(lens , lens + 1).toLowerCase()
                        + methodName.substring(suffix.length() + 1);
                return map.get(name);
            }
        }
        return null;
    }
}
