package cn.chendd.tips.examples.proxy;

import cn.chendd.tips.examples.proxy.vo.User;
import cn.chendd.jdbc.DBManager;
import org.junit.Test;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author chendd
 * @since 2019-08-31 23:28
 */
public class MapProxyTest {

    @Test
    public void test() {
        List<Map<String, Object>> dataList = this.getDataList();
        //转换数据，将map转换为bean
        List<User> userList = new ArrayList<>();
        for (Map<String, Object> map : dataList) {
            User user = this.newInstance(User.class , map);
            userList.add(user);
        }
        //打印结果
        for (User user : userList) {
            System.out.println(user.getUser() + "," + user.getAccountLocked() + "," +
                    user.getPasswordLastChanged() + "," + user.getPasswordLastChanged());

        }
    }

    private <T> T newInstance(Class<T> clazz , Map<String, Object> map){
        MapProxyHandler proxyHandler = new MapProxyHandler(map);
        T t = (T) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader() , new Class[]{clazz} , proxyHandler);
        return t;
    }

    /**
     * 根据sql返回对应的结果集List<Map<String,Object>>
     *
     * @return
     */
    private List<Map<String, Object>> getDataList() {
        String sql = "select " +
                "user as user, " +
                "case when 1=2 then false else true end as flag, " +
                "account_locked as accountLocked, " +
                "password_last_changed passwordLastChanged from user";
        return DBManager.getDataList(sql);
    }

}
