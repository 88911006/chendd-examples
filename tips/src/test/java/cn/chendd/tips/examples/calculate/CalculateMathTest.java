package cn.chendd.tips.examples.calculate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * 金额计算
 *
 * @author chendd
 * @date 2023/4/22 20:22
 */
@RunWith(JUnit4.class)
public class CalculateMathTest {

    /**
     * 保留2位小数，四舍五入
     */
    @Test
    public void round() {
        System.out.println(Math.round(12345.554 * 100) / 100D);
        System.out.println(Math.round(12345.555 * 100) / 100D);
        System.out.println(Math.round(12345.556 * 100) / 100D);
    }

    /**
     * 保留2位小数，正舍负入（ceil：反过来，正入负舍）
     */
    @Test
    public void floor() {
        System.out.println(Math.floor(123.554 * 100) / 100D);
        System.out.println(Math.floor(123.555 * 100) / 100D);
        System.out.println(Math.floor(123.556 * 100) / 100D);

        System.out.println(Math.floor(-123.554 * 100) / 100D);
        System.out.println(Math.floor(-123.555 * 100) / 100D);
        System.out.println(Math.floor(-123.556 * 100) / 100D);
    }

}
