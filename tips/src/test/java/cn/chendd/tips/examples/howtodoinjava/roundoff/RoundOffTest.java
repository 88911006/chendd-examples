package cn.chendd.tips.examples.howtodoinjava.roundoff;

import org.apache.commons.math3.util.Precision;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * 四舍五入 验证
 *
 * @author chendd
 * @date 2023/4/16 18:10
 */
@RunWith(JUnit4.class)
public class RoundOffTest {

    private final Double value = 12345.56789;

    @Test
    public void bigDecimal() {
        BigDecimal result = new BigDecimal(this.value).setScale(2 , RoundingMode.HALF_UP);
        Assert.assertEquals(result.toString() , "12345.57");
    }

    @Test
    public void commonsMath() {
        double result = Precision.round(this.value, 2, RoundingMode.HALF_UP.ordinal());
        Assert.assertEquals(Double.toString(result), "12345.57");
    }

    @Test
    public void jdkMath() {
        double scale = Math.pow(10, 2);
        double result = Math.round(this.value * scale) / scale;
        Assert.assertEquals(Double.toString(result), "12345.57");
    }

    @Test
    public void decimalFormat() {
        DecimalFormat format = new DecimalFormat("###.##");
        String result = format.format(this.value);
        Assert.assertEquals(result, "12345.57");
    }

}
