package cn.chendd.tips.examples.proxy;

import cn.chendd.tips.examples.proxy.vo.User;
import cn.chendd.jdbc.DBManager;
import org.junit.Test;

import javax.sql.RowSet;
import java.lang.reflect.Proxy;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chendd
 * @since 2019-08-31 23:28
 */
public class RowSetProxyTest {

    @Test
    public void test() {
        String sql = "select " +
                "user as user, " +
                "case when 1=2 then false else true end as flag, " +
                "account_locked as accountLocked, " +
                "password_last_changed passwordLastChanged from user";
        List<User> dataList = this.getDataList(sql, User.class);
        //打印结果
        for (User user : dataList) {
            System.out.println(user.getUser() + "," + user.getAccountLocked() + "," +
                    user.getPasswordLastChanged() + "," + user.getPasswordLastChanged());
        }
    }

    private <T> T newInstance(Class<T> clazz , Map<String , Object> map){
        MapProxyHandler proxyHandler = new MapProxyHandler(map);
        T t = (T) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader() , new Class[]{clazz} , proxyHandler);
        return t;
    }

    /**
     * 根据sql返回对应的结果集List<Map<String,Object>>
     *
     * @return
     */
    private <T> List<T> getDataList(String sql , Class<T> clazz) {
        RowSet rowSet = DBManager.getResultSet(sql);
        List<T> dataList = new ArrayList<>();
        try {
            ResultSetMetaData rsmd = rowSet.getMetaData();
            int columnCount = rsmd.getColumnCount();
            while(rowSet.next()){
                Map<String , Object> columnMap = new HashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = rsmd.getColumnLabel(i);
                    Object value = rowSet.getObject(i);
                    columnMap.put(columnName , value);
                }
                T t = this.newInstance(clazz , columnMap);
                dataList.add(t);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(rowSet != null) {
                try {rowSet.close();} catch (SQLException e) {}
            }
        }
        return dataList;
    }

}
