package cn.chendd.tips.examples.howtodoinjava.format;

import cn.hutool.core.util.StrUtil;
import com.sun.javafx.binding.StringFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.PropertyPlaceholderHelper;

import java.text.MessageFormat;
import java.util.Formatter;
import java.util.Properties;

/**
 * 字符串格式化
 *
 * @author chendd
 * @date 2023/4/16 21:03
 */
@RunWith(JUnit4.class)
public class StringFormatTest {

    @Test
    public void string() {
        System.out.println(String.format("hello: %s，昨天是星期 %d", "chendd" , 6));
    }

    @Test
    public void string$() {
        //每个 %占位符 对应一个参数
        System.out.println(String.format("hello: %s，昨天是星期 %d，是的 %s 就是我", "chendd" , 6 , "chendd"));
        //每个%$ 对应一个参数索引
        System.out.println(String.format("hello: %1$s，昨天是星期 %2$d，是的 %1$s 就是我", "chendd" , 6));
    }

    @Deprecated
    @Test
    public void printf$() {
        //这种方法没有返回值，适用于打印输出时，而PrintStream需要 IO 流，个人不推荐
        System.out.printf("hello: %1$s，昨天是星期 %2$d，是的 %1$s 就是我%n", "chendd" , 6);
    }

    @Test
    public void formatter() {
        Formatter format1 = new Formatter().format("hello: %1$s，昨天是星期 %2$d，是的 %1$s 就是我", "chendd", 6);
        System.out.println(format1.out());
        Formatter format2 = new Formatter().format("hello: %s，昨天是星期 %d，是的 %s 就是我", "chendd", 6 , "chendd");
        System.out.println(format2.out());
    }

    @Test
    public void stringFormatter() {
        String format1 = StringFormatter.format("hello: %1$s，昨天是星期 %2$d，是的 %1$s 就是我", "chendd", 6).getValue();
        System.out.println(format1);
        String format2 = StringFormatter.format("hello: %s，昨天是星期 %d，是的 %s 就是我", "chendd", 6, "chendd").getValue();
        System.out.println(format2);
    }

    @Test
    public void logback() {
        Logger logger = LoggerFactory.getLogger(StringFormatTest.class);
        logger.info("hello: {}，昨天是星期 {}，是的 {} 就是我", "chendd" , 6 , "chendd");
    }

    @Test
    public void hutool() {
        String format = StrUtil.format("hello: {}，昨天是星期 {}，是的 {} 就是我", "chendd", 6, "chendd");
        System.out.println(format);
    }

    @Test
    public void messageFormat() {
        String format = MessageFormat.format("hello: {0}，昨天是星期 {1}，是的 {0} 就是我", "chendd", 6);
        System.out.println(format);
    }

    @Test
    public void springFormat() {
        Properties props = new Properties();
        props.put("name" , "chendd");
        props.put("date" , "6");

        String text1 = "hello: {name}，昨天是星期 {date}";
        String format1 = new PropertyPlaceholderHelper("{", "}").replacePlaceholders(text1, props);
        System.out.println(format1);

        String text2 = "hello: ${name}，昨天是星期 ${date}";
        String format2 = new PropertyPlaceholderHelper("${", "}").replacePlaceholders(text2, props);
        System.out.println(format2);
    }

}
