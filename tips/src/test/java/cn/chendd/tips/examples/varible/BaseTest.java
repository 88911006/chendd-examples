package cn.chendd.tips.examples.varible;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.awt.*;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * @author chendd
 */
@RunWith(JUnit4.class)
public abstract class BaseTest {

    protected Method method = super.getClass().getMethod("sayHello", String.class, Integer.TYPE,
            Character.class, Date.class, Point.class , Boolean.TYPE);

    public BaseTest() throws NoSuchMethodException {
    }

    /**
     * 定义一个函数，包含一些基本类型与包装类型及对象类型
     */
    public String sayHello(String name, int age, Character sex, Date birthday, Point point , boolean flag) {

        return "hello: " + name;
    }

}
