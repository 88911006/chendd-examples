package cn.chendd.tips.examples.proxy.vo;

import java.util.Date;

/**
 * @author chendd
 * @since 2019-09-01 09:03
 */
public interface User {

    public String getUser();

    public Long getFlag();

    public String getAccountLocked();

    public Date getPasswordLastChanged();

}
