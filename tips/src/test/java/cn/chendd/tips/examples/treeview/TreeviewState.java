package cn.chendd.tips.examples.treeview;

/**
 * 属性状态
 *
 * @author chendd
 * @date 2020/4/14 22:32
 */
public enum TreeviewState {

    checked,//是否选中
    disabled,//是否禁用
    expanded,//是否展开
    selected//是否选中

}
