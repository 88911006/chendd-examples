package cn.chendd.tips.examples.calculate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;

/**
 * 金额计算
 *
 * @author chendd
 * @date 2023/4/22 21:22
 */
@RunWith(JUnit4.class)
public class CalculateBigDecimalTest {

    /**
     * 保留2位小数，四舍五入
     */
    @Test
    public void round() {
        System.out.println(new BigDecimal("12345.554").setScale(2, BigDecimal.ROUND_HALF_UP));
        System.out.println(new BigDecimal("12345.555").setScale(2, BigDecimal.ROUND_HALF_UP));
        System.out.println(new BigDecimal("12345.556").setScale(2, BigDecimal.ROUND_HALF_UP));
    }

    /**
     * 保留2位小数，正舍负入
     */
    @Test
    public void floor() {
        System.out.println(new BigDecimal("12345.554").setScale(2, BigDecimal.ROUND_FLOOR));
        System.out.println(new BigDecimal("12345.555").setScale(2, BigDecimal.ROUND_FLOOR));
        System.out.println(new BigDecimal("12345.556").setScale(2, BigDecimal.ROUND_FLOOR));
        System.out.println();
        System.out.println(new BigDecimal("-12345.554").setScale(2, BigDecimal.ROUND_FLOOR));
        System.out.println(new BigDecimal("-12345.555").setScale(2, BigDecimal.ROUND_FLOOR));
        System.out.println(new BigDecimal("-12345.556").setScale(2, BigDecimal.ROUND_FLOOR));
    }

    /**
     * 保留2位小数，截位法
     */
    @Test
    public void down() {
        System.out.println(new BigDecimal("12345.554").setScale(2, BigDecimal.ROUND_DOWN));
        System.out.println(new BigDecimal("12345.555").setScale(2, BigDecimal.ROUND_DOWN));
        System.out.println(new BigDecimal("12345.556").setScale(2, BigDecimal.ROUND_DOWN));
    }

    /**
     * 保留2位小数，四舍六入五成双
     */
    @Test
    public void even() {
        System.out.println("12345.234\t=\t" + new BigDecimal("12345.234").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.236\t=\t" + new BigDecimal("12345.236").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.235\t=\t" + new BigDecimal("12345.235").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.245\t=\t" + new BigDecimal("12345.245").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println();
        System.out.println("12345.2450\t=\t" + new BigDecimal("12345.2450").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2451\t=\t" + new BigDecimal("12345.2451").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2452\t=\t" + new BigDecimal("12345.2452").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2453\t=\t" + new BigDecimal("12345.2453").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2454\t=\t" + new BigDecimal("12345.2454").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2456\t=\t" + new BigDecimal("12345.2455").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2457\t=\t" + new BigDecimal("12345.2456").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2458\t=\t" + new BigDecimal("12345.2457").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2459\t=\t" + new BigDecimal("12345.2458").setScale(2, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("12345.2459\t=\t" + new BigDecimal("12345.2459").setScale(2, BigDecimal.ROUND_HALF_EVEN));
    }

}
