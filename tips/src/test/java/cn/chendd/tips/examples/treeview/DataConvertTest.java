package cn.chendd.tips.examples.treeview;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * 数据转换测试
 *
 * @author chendd
 * @date 2020/4/14 22:34
 */
public class DataConvertTest {

    private Integer ROOT_ID = -1;//约定根节点为-1
    private List<City> cityList;

    @Test
    public void test(){
        System.out.println("======原始数据======");
        System.out.println(JSONObject.toJSONString(cityList , true));
        Stream<City> rootCity = cityList.stream().filter(city -> ROOT_ID.equals(city.getParentId()));
        List<Treeview> treeviewList = new ArrayList<>();
        this.convertTreeview(rootCity, treeviewList);
        for (Treeview treeview : treeviewList) {
            this.collectList(treeview , this.cityList);
        }
        System.out.println("======转换后的数据======");
        System.out.println(JSON.toJSONString(treeviewList , true));
    }

    /**
     * 获取原始数据中的一级菜单
     */
    private void convertTreeview(Stream<City> cityStream, List<Treeview> treeviewList) {
        cityStream.forEach(city -> {
            Treeview treeview = new Treeview();
            treeview.setId(city.getId());
            treeview.setText(city.getName());
            treeview.setState(null);
            treeview.setNodes(null);
            treeviewList.add(treeview);
        });
    }

    /**
     * 遍历一级菜单，并递归调用字级菜单，查找对应的菜单节点
     */
    private void collectList(Treeview treeview, List<City> cityList) {
        Integer nodeId = treeview.getId();
        for (City city : cityList) {
            Integer parentId = city.getParentId();
            if(parentId != null && parentId.equals(nodeId)){
                if(treeview.getNodes() == null){
                    treeview.setNodes(new ArrayList<>());
                }
                Treeview childMenuTreeview = new Treeview();
                childMenuTreeview.setId(city.getId());
                childMenuTreeview.setText(city.getName());
                childMenuTreeview.setState(null);
                treeview.getNodes().add(childMenuTreeview);
                this.collectList(childMenuTreeview , cityList);
            }
        }
    }

    @Before
    public void init(){
        cityList = new ArrayList<>();
        cityList.add(new City(1 , "北京市" , ROOT_ID));
        cityList.add(new City(2 , "海淀区" , 1));
        cityList.add(new City(3 , "朝阳区" , 1));
        cityList.add(new City(4 , "湖北省" , ROOT_ID));
        cityList.add(new City(5 , "武汉市" , 4));
        cityList.add(new City(6 , "襄阳市" , 4));
        cityList.add(new City(7 , "襄城区" , 6));
        cityList.add(new City(8 , "襄州区" , 6));
        cityList.add(new City(9 , "樊城区" , 6));
        cityList.add(new City(10 , "老河口市" , 6));
        cityList.add(new City(11 , "谷城县" , 6));
        cityList.add(new City(12 , "南漳县" , 6));
        cityList.add(new City(13 , "枣阳市" , 6));
        cityList.add(new City(14 , "保康县" , 6));
        cityList.add(new City(15 , "宜城市" , 6));
    }

}
