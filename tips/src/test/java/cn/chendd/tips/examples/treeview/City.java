package cn.chendd.tips.examples.treeview;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 城市数据对象
 *
 * @author chendd
 * @date 2020/4/14 22:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class City {

    private Integer id;
    private String name;
    private Integer parentId;

}
