package cn.chendd.tips.examples.treeview;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 构造Treeview结构数据
 *
 * @author chendd
 * @date 2020/4/14 22:30
 */
@Data
public class Treeview {

    private Integer id;

    private String text;

    private List<Treeview> nodes;

    private Map<TreeviewState, Boolean> state;//菜单状态

}
