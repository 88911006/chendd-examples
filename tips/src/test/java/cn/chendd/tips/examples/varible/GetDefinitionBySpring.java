package cn.chendd.tips.examples.varible;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

/**
 * 使用Spring core自带的函数实现
 * @author chendd
 * @date 2019-08-08 20:39:20
 */
public final class GetDefinitionBySpring extends BaseTest {

    public GetDefinitionBySpring() throws NoSuchMethodException {
        super();
    }

    @Test
    public void testLocalVariable() {
        LocalVariableTableParameterNameDiscoverer localParameter = new LocalVariableTableParameterNameDiscoverer();
        String parameterNames[] = localParameter.getParameterNames(method);
        Assert.assertEquals(parameterNames.length, method.getParameterCount());
        for (int i = 0; i < parameterNames.length; i++) {
            System.out.print(parameterNames[i] + ",  ");
        }
        System.out.println();
    }

    @Test
    public void testDefaultVariable() {
        DefaultParameterNameDiscoverer defaultParameter = new DefaultParameterNameDiscoverer();
        String[] parameterNames = defaultParameter.getParameterNames(method);
        Assert.assertEquals(parameterNames.length, method.getParameterCount());
        for (int i = 0; i < parameterNames.length; i++) {
            System.out.print(parameterNames[i] + ",  ");
        }
        System.out.println();
    }

}
