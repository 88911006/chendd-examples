package cn.chendd.tips.examples.proxy;

import org.junit.Test;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chendd
 * @since 2019-08-31 22:27
 * 测试List代理实现
 */
public class ObjectProxyTest {

    /**
     * List集合类的方法代理测试
     */
    @Test
    public void testListProxy(){
        List<String> list = new ArrayList<String>();
        ObjectProxyHandler proxyHandler = new ObjectProxyHandler(list);
        List<String> listProxy = (List<String>) Proxy.newProxyInstance(getClass().getClassLoader() ,
                list.getClass().getInterfaces() , proxyHandler);
        listProxy.add("chendd");
        listProxy.remove("chendd");
    }

    /**
     * 字符串的比较接口代理
     */
    @Test
    public void testGeneralProxy(){
        String value = "https://www.chendd.cn";
        ObjectProxyHandler proxyHandler = new ObjectProxyHandler(value);
        Comparable valueProxy = (Comparable) Proxy.newProxyInstance(getClass().getClassLoader() ,
                value.getClass().getInterfaces() , proxyHandler);
        valueProxy.compareTo("chendd");
    }

}
