package cn.chendd.jdbc;

import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chendd
 * @since 2019-08-31 22:36
 */
public class DBManager {

    public static Connection getConnection(){
        Connection conn = null;
        try{
            String driver = "com.mysql.jdbc.Driver";
            String URL = "jdbc:mysql://localhost:3306/mysql";
            Class.forName(driver).newInstance();
            return DriverManager.getConnection(URL , "root" , "root");
        } catch (Exception e){
            e.printStackTrace();
        }
        return conn;
    }

    public static void close(Object ... params) {
        if(params == null){
            return;
        }
        for (Object param : params) {
            if(param == null){
                continue;
            }
            if(param instanceof ResultSet){
                try {((ResultSet) param).close();} catch (SQLException e) {}
            } else if(param instanceof Statement){
                try {((Statement) param).close();} catch (SQLException e) {}
            } else if(param instanceof Connection){
                try {((Connection) param).close();} catch (SQLException e) {}
            }
        }
    }

    /**
     * 将sql查询结果的每条转换为map
     * @param sql
     * @param params
     * @return
     */
    public static List<Map<String , Object>> getDataList(String sql , Object ... params){
        Connection conn = getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;
        List<Map<String , Object>> dataList = new ArrayList<>();
        try {
            pst = conn.prepareStatement(sql);
            fillParameters(pst, params);
            rs = pst.executeQuery();
            while (rs.next()){
                Map<String , Object> columnMap = new HashMap<>();
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();
                for(int i=1 ; i <= columnCount ; i++){
                    String columnName = rsmd.getColumnLabel(i);
                    Object value = rs.getObject(i);
                    columnMap.put(columnName , value);
                }
                dataList.add(columnMap);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(rs , pst , conn);
        }
        return dataList;
    }

    /**
     * 据sql查询返回离线结果集
     * @param sql
     * @param params
     * @return 离线结果集
     */
    public static CachedRowSet getResultSet(String sql , Object ... params){
        Connection conn = getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;
        CachedRowSet rowSet = null;
        try {
            pst = conn.prepareStatement(sql);
            fillParameters(pst, params);
            rs = pst.executeQuery();
            rowSet = new CachedRowSetImpl();
            rowSet.populate(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(rs , pst , conn);
        }
        return rowSet;
    }

    private static void fillParameters(PreparedStatement pst, Object[] params) throws SQLException {
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof String) {
                    pst.setString(i + 1, (String) params[i]);
                    continue;
                }
                pst.setObject(i + 1, params[i]);
            }
        }
    }

}
