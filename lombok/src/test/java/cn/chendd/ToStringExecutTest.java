package cn.chendd;

import lombok.ToString;

/**
 * 排除两个属性
 */
@ToString(exclude = {"boss" , "flag"})
public class ToStringExecutTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;

}
