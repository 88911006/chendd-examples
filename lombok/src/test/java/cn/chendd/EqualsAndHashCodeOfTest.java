package cn.chendd;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = {"userId" , "userName"})
public class EqualsAndHashCodeOfTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;
}
