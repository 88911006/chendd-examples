package cn.chendd;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 测试 @Slf4j 注解，实现简单日志输出
 */
@Slf4j
public class Slf4jTest {

    @Test
    public void testLog(){
        log.debug("slf4j debug");
        log.info("slf4j info");
        log.warn("slf4j warn");
        log.error("slf4j error");
    }

}
