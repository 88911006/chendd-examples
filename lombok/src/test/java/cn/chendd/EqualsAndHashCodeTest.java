package cn.chendd;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class EqualsAndHashCodeTest {

    private final Double PI = Math.PI;

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;
}
