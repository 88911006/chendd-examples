package cn.chendd;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ConstructorTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;
}
