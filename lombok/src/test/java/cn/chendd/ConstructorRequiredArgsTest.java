package cn.chendd;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ConstructorRequiredArgsTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;
}
