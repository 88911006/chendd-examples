package cn.chendd;

import lombok.extern.log4j.Log4j;
import org.junit.Test;

/**
 * 测试 @Slf4j 注解，实现简单日志输出
 */
@Log4j
public class Log4jTest {

    @Test
    public void testLog(){
        log.debug("log4j debug");
        log.info("log4j info");
        log.warn("log4j warn");
        log.error("log4j error");
    }

}
