package cn.chendd;

import lombok.Getter;

@Getter
public class GetterTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;

}
