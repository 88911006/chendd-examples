package cn.chendd;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetterAndSetterTest {

    private final Double PI = Math.PI;

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;

}
