package cn.chendd;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(exclude = {"userId" , "userName"})
public class EqualsAndHashCodeExcludeTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;
}
