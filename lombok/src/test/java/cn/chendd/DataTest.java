package cn.chendd;

import lombok.Data;

@Data
public class DataTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;

}
