package cn.chendd;

import lombok.ToString;

/**
 * 包含两个属性
 */
@ToString(of = {"userId" , "userName"})
public class ToStringOfTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;

}
