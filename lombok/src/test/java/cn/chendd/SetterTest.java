package cn.chendd;

import lombok.Setter;

@Setter
public class SetterTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;

}
