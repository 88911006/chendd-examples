package cn.chendd;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class ToStringTest {

    private Integer userId;

    private String userName;

    private Boolean boss;

    private boolean flag;

}
