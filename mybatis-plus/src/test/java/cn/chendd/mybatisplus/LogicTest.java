package cn.chendd.mybatisplus;

import cn.chendd.base.BaseBootstrapTest;
import cn.chendd.mybatisplus.user.enums.SexEnum;
import cn.chendd.mybatisplus.user.model.User;
import cn.chendd.mybatisplus.user.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 逻辑删除测试
 *
 * @author chendd
 * @date 2022/6/30 21:49
 */
public class LogicTest extends BaseBootstrapTest {

    @Resource
    private UserService userService;

    private static Long id;

    @Before
    public void before() {
        if (id == null) {
            id = IdWorker.getId();
        }
    }

    @Test
    public void insert() {
        User user = new User();
        //新增时可不指定，有内置的和默认的多种id主键策略
        user.setUserId(id);
        user.setUserName("chendd");
        user.setSex(SexEnum.Boy);
        user.setBirthday(new Date());
        //新增时无需考虑逻辑删除字段，通过表的默认值填充
        this.userService.save(user);
    }

    @Test
    public void update() {
        User user = new User();
        user.setUserId(id);
        user.setUserName("陈冬冬");
        user.setSex(SexEnum.Boy);
        user.setBirthday(new Date());
        //新增时无需考虑逻辑删除字段，通过表的默认值填充
        this.userService.saveOrUpdate(user);
    }

    @Test
    public void delete() {
        this.userService.removeById(id);
    }

    @Test
    public void selectList() {
        this.userService.list();
    }

    @Test
    public void selectWrapper() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select("userId" , "userName").orderByAsc("userId");
        this.userService.list(wrapper);
    }

}
