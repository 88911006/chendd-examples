package cn.chendd.mybatisplus;

import cn.chendd.base.BaseBootstrapTest;
import cn.chendd.mybatisplus.user.model.User;
import cn.chendd.mybatisplus.user.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * 分页测试
 *
 * @author chendd
 * @date 2022/7/3 9:14
 */
public class PageTest extends BaseBootstrapTest {

    @Resource
    private UserService userService;

    @Test
    public void page() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        IPage<User> pageFinder = new Page<>(1 , 10);
        IPage<User> page = this.userService.page(pageFinder, queryWrapper);
        System.out.println(String.format("pageNumber = %d , pageSize = %d , pages = %d , totals = %d , data = %s" ,
                page.getCurrent() , page.getSize() , page.getPages() , page.getTotal() , page.getRecords().toString()));
    }

    @Test
    public void pageForMapper() {
        IPage<User> pageFinder = new Page<>(1 , 10);
        IPage<User> page = this.userService.pageList(pageFinder);
        System.out.println(String.format("pageNumber = %d , pageSize = %d , pages = %d , totals = %d , data = %s" ,
                page.getCurrent() , page.getSize() , page.getPages() , page.getTotal() , page.getRecords().toString()));
    }

}
