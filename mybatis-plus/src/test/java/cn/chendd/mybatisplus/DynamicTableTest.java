package cn.chendd.mybatisplus;

import cn.chendd.base.BaseBootstrapTest;
import cn.chendd.mybatisplus.operationlog.model.SysOperationLog;
import cn.chendd.mybatisplus.operationlog.po.SysOperationLogParam;
import cn.chendd.mybatisplus.operationlog.service.SysOperationLogService;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * 动态表测试
 *
 * @author chendd
 * @date 2022/7/3 19:17
 */
public class DynamicTableTest extends BaseBootstrapTest {

    @Resource
    private SysOperationLogService service;

    @Test
    public void testTable() {
        SysOperationLog entity = new SysOperationLog();
        entity.setUserId(1001L);
        entity.setUserName("chendd");
        //不设置tableDate字段表示操作原始表
        this.service.save(entity);
    }

    @Test
    public void testInsertDynamicTable() {
        SysOperationLog entity = new SysOperationLog();
        entity.setUserId(1001L);
        entity.setUserName("chendd");
        //设置tableDate字段表示操作转换日期后的表
        entity.setTableDate("2022-07-03");
        this.service.save(entity);
    }

    @Test
    public void testUpdateDynamicTable() {
        SysOperationLog entity = new SysOperationLog();
        entity.setUserId(1001L);
        entity.setUserName("chendd");
        //设置tableDate字段表示操作转换日期后的表
        entity.setTableDate("2022-07-03");
        this.service.updateSysOperationLog(entity);
    }

    @Test
    public void testCustomDynamicTable() {
        SysOperationLogParam param = new SysOperationLogParam();
        //设置动态表为拼接新的表后缀
        param.setTableSuffix("_666");
        this.service.querySysOperationLog(param);
    }

}
