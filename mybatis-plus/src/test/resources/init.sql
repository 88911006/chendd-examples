CREATE DATABASE `test` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `sys_user` (
                            `userId` bigint(20) NOT NULL COMMENT '用户ID',
                            `userName` varchar(32) DEFAULT NULL COMMENT '用户姓名',
                            `sex` varchar(8) DEFAULT NULL COMMENT '性别',
                            `birthday` datetime DEFAULT NULL COMMENT '出生日期',
                            `dataStatus` varchar(16) DEFAULT 'USABLE' COMMENT '数据是否有效状态',
                            PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

CREATE TABLE `sys_operationlog` (
                                    `id` bigint(20) NOT NULL COMMENT '主键ID',
                                    `userId` bigint(20) DEFAULT NULL COMMENT '用户Id',
                                    `userName` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '用户名称',
                                    `description` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT '功能描述',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统操作日志表';
