package cn.chendd.mybatisplus.user.service;

import cn.chendd.mybatisplus.user.model.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * UserService接口定义
 *
 * @author chendd
 * @date 2022/6/30 21:44
 */
public interface UserService extends IService<User> {

    /**
     * 查询数据分页
     * @param pageFinder 分页参数
     * @return 分页数据
     */
    IPage<User> pageList(IPage<User> pageFinder);
}
