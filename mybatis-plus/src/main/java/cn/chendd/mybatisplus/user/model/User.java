package cn.chendd.mybatisplus.user.model;

import cn.chendd.mybatisplus.user.enums.SexEnum;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;

/**
 * 用户信息实体对象
 *
 * @author chendd
 * @date 2022/6/30 21:30
 */
@Data
@TableName("sys_user")
public class User {

    /**
     * 用户id
     */
    @TableId(value = "userId" , type = IdType.ASSIGN_ID)
    @TableField("userId")
    private Long userId;

    /**
     * 用户名称
     */
    @TableField("userName")
    private String userName;

    /**
     * 性别
     */
    @TableField("sex")
    private SexEnum sex;

    /**
     * 出生日期
     */
    @TableField("birthday")
    private Date birthday;

    /**
     * 数据状态，可用或禁用
     */
    @TableField("dataStatus")
    @TableLogic
    @JsonIgnore
    private String dataStatus;

}
