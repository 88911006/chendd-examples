package cn.chendd.mybatisplus.user.enums;

/**
 * 性别枚举定义
 *
 * @author chendd
 * @date 2022/6/30 21:40
 */
public enum SexEnum {

    /**
     * 男
     */
    Boy,
    /**
     * 女
     */
    Girl,
    /**
     * 保密
     */
    Private

}
