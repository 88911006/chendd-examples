package cn.chendd.mybatisplus.user.service.impl;

import cn.chendd.mybatisplus.user.mapper.UserMapper;
import cn.chendd.mybatisplus.user.model.User;
import cn.chendd.mybatisplus.user.service.UserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * UserService接口实现
 *
 * @author chendd
 * @date 2022/6/30 21:46
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper , User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public IPage<User> pageList(IPage<User> pageFinder) {
        return this.userMapper.pageList(pageFinder);
    }
}
