package cn.chendd.mybatisplus.user.mapper;

import cn.chendd.mybatisplus.user.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 用户信息Mapper
 *
 * @author chendd
 * @date 2022/6/30 21:29
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 查询数据分页
     * @param pageFinder 分页参数
     * @return 分页数据
     */
    IPage<User> pageList(IPage<User> pageFinder);
}
