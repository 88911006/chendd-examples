package cn.chendd.mybatisplus.config;

import cn.chendd.mybatisplus.dynamic.DynamicTableName;
import cn.chendd.mybatisplus.operationlog.model.SysOperationLog;
import com.baomidou.mybatisplus.extension.parsers.DynamicTableNameParser;
import com.baomidou.mybatisplus.extension.parsers.ITableNameHandler;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.assertj.core.util.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * MybatisPlusConfig
 *
 * @author chendd
 * @date 2022/7/3 10:40
 */
@Configuration
public class MybatisPlusConfig {

    /**
     * 分页插件配置
     * @return 分页插件配置
     */
    @Bean
    public PaginationInterceptor getPaginationInterceptor(){
        PaginationInterceptor interceptor = new PaginationInterceptor();
        //构建动态标名解析器
        DynamicTableNameParser dynamicTableNameParser = new DynamicTableNameParser();
        Map<String, ITableNameHandler> tableNameMap = new HashMap<>();
        tableNameMap.put("Sys_operationlog", (metaObject, sql, tableName) -> {
            RoutingStatementHandler handler = (RoutingStatementHandler) metaObject.getOriginalObject();
            BoundSql boundSql = handler.getBoundSql();
            Object parameterObject = boundSql.getParameterObject();
            if(parameterObject instanceof SysOperationLog) {
                SysOperationLog sysOperationLog = (SysOperationLog) parameterObject;
                //日志表的分表规则：Sys_operationlog_202207
                return sysOperationLog.getTableNameCondition();
            }
            Object param = handler.getBoundSql().getParameterObject();
            if (param instanceof DynamicTableName) {
                DynamicTableName dynamicTableName = (DynamicTableName) param;
                return dynamicTableName.getTableNameCondition();
            } else if (param instanceof MapperMethod.ParamMap) {
                MapperMethod.ParamMap paramMap = (MapperMethod.ParamMap) handler.getBoundSql().getParameterObject();
                Set<Map.Entry<String , Object>> set = paramMap.entrySet();
                for (Map.Entry<String, Object> entry : set) {
                    Object value = entry.getValue();
                    if(value instanceof DynamicTableName) {
                        DynamicTableName dynamicTableName = (DynamicTableName) value;
                        return dynamicTableName.getTableNameCondition();
                    }
                }
            }
            /*QueryWrapper queryWrapper = (QueryWrapper) paramMap.get("ew");
            System.err.println(queryWrapper.getParamNameValuePairs());
            System.err.println(sql);
            System.err.println(tableName);*/
            return tableName;
        });
        dynamicTableNameParser.setTableNameHandlerMap(tableNameMap);
        interceptor.setSqlParserList(Lists.newArrayList(dynamicTableNameParser));
        return interceptor;
    }

}
