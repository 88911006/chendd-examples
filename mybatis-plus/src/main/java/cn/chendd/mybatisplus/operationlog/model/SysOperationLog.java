package cn.chendd.mybatisplus.operationlog.model;

import cn.chendd.mybatisplus.dynamic.DynamicTableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 系统操作日志实体定义
 *
 * @author chendd
 * @date 2019/9/20 16:20
 */
@TableName("Sys_operationlog")
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SysOperationLog implements DynamicTableName {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @TableField("id")
    private Long id;

    @TableField("userId")
    private Long userId;

    @TableField("userName")
    private String userName;

    @TableField("description")
    private String description;

    @JsonIgnore
    @TableField(exist = false)
    private String tableDate;

    /**
     * @return 动态表明后缀
     */
    @SneakyThrows
    @Override
    public String setTableNameCondition() {
        if (StringUtils.isBlank(this.tableDate)) {
            return "";
        }
        Date date = DateFormat.getDateInstance().parse(this.tableDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        String month = String.format("%02d" , calendar.get(Calendar.MONTH) + 1);
        return "_" + year + month;
    }

}
