package cn.chendd.mybatisplus.operationlog.service.impl;

import cn.chendd.mybatisplus.operationlog.mapper.SysOperationLogMapper;
import cn.chendd.mybatisplus.operationlog.model.SysOperationLog;
import cn.chendd.mybatisplus.operationlog.po.SysOperationLogParam;
import cn.chendd.mybatisplus.operationlog.service.SysOperationLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 操作日志Service接口实现
 *
 * @author chendd
 * @date 2022/7/3 19:10
 */
@Service
public class SysOperationLogServiceImpl extends ServiceImpl<SysOperationLogMapper , SysOperationLog> implements SysOperationLogService {

    @Resource
    private SysOperationLogMapper sysOperationLogMapper;

    @Override
    public void updateSysOperationLog(SysOperationLog entity) {
        this.sysOperationLogMapper.updateSysOperationLog(entity);
    }

    @Override
    public List<String> querySysOperationLog(SysOperationLogParam param) {
        return this.sysOperationLogMapper.querySysOperationLog(param);
    }
}
