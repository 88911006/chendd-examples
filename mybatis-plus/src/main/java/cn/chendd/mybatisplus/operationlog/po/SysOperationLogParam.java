package cn.chendd.mybatisplus.operationlog.po;

import cn.chendd.mybatisplus.dynamic.DynamicTableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 查询条件
 *
 * @author chendd
 * @date 2022/7/3 20:17
 */
@Getter
@Setter
public class SysOperationLogParam implements DynamicTableName {

    private String tableSuffix;

    @Override
    public String getTableName() {
        return "Sys_operationlog";
    }

    /**
     * @return 动态表明后缀
     */
    @Override
    public String setTableNameCondition() {
        return tableSuffix;
    }

}
