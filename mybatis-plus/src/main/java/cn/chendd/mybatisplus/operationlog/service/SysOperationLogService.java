package cn.chendd.mybatisplus.operationlog.service;

import cn.chendd.mybatisplus.operationlog.model.SysOperationLog;
import cn.chendd.mybatisplus.operationlog.po.SysOperationLogParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 操作日志Service接口定义
 *
 * @author chendd
 * @date 2022/7/3 19:09
 */
public interface SysOperationLogService extends IService<SysOperationLog> {

    /**
     * 修改数据
     * @param entity 数据
     */
    void updateSysOperationLog(SysOperationLog entity);

    /**
     * 查询列表
     * @param param 查询条件
     * @return 数据列表
     */
    List<String> querySysOperationLog(SysOperationLogParam param);
}
