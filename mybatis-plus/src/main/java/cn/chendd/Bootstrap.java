package cn.chendd;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 服务器启动类
 * @author chendd
 * @date 2019/9/12 13:21
 */
@SpringBootApplication
@MapperScan("cn.chendd.**.mapper")
@EnableTransactionManagement
public class Bootstrap {

    /**
    * 服务器启动
    * @param args 启动参数
    */
    public static void main(String[] args) {
        SpringApplication.run(Bootstrap.class , args);
    }

}
