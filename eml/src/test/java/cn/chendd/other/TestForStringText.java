package cn.chendd.other;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.dom.*;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.message.HeaderImpl;
import org.apache.james.mime4j.message.MultipartImpl;
import org.apache.james.mime4j.stream.Field;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chendd
 */
@SuppressWarnings("all")
public class TestForStringText {

    public static void main(String[] args) throws Exception {

        String folder = "eml文件夹";
        File[] files = new File(folder).listFiles((dir , name) -> name.endsWith(".eml"));
        for (File file : files) {
            try {
                reader(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void reader(File file) throws IOException {
        String text = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        String regex = "Content-Type.*\\s*Content-Disposition.*\\s*Content-Transfer-Encoding.*\\s*";
        Matcher matcher = Pattern.compile(regex , Pattern.CASE_INSENSITIVE).matcher(text);
        if (matcher.find()) {
            System.out.println();
            do {
                System.out.println("----文件：" + file.getName() + " 开始----");
                System.out.println(matcher.group());
                System.out.println("----文件：" + file.getName() + " 结束----");
            } while (matcher.find());
        }

    }

}
