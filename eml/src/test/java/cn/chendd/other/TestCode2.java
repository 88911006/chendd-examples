package cn.chendd.other;

import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.dom.*;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.message.HeaderImpl;
import org.apache.james.mime4j.message.MultipartImpl;
import org.apache.james.mime4j.stream.Field;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.List;
import java.util.TimeZone;

/**
 * @author chendd
 */
@SuppressWarnings("all")
public class TestCode2 {

    public static void main(String[] args) throws Exception {
        Message message = Message.Builder.of(new FileInputStream("文件.eml")).build();
        System.out.println("邮件ID：" + message.getMessageId());
        System.out.println("邮件标题：" + message.getSubject());
        System.out.println("邮件编码：" + message.getCharset());
        System.out.println("内容编码：" + message.getContentTransferEncoding());
        System.out.println("DispositionType：" + message.getDispositionType());
        System.out.println("文件名称：" + message.getFilename());
        System.out.println("文档类型：" + message.getMimeType());
        System.out.println("发送人：" + message.getFrom());
        System.out.println("接收人：" + message.getTo());
        System.out.println("抄送人：" + message.getCc());
        System.out.println("密送人：" + message.getBcc());
        TimeZone timeZone = TimeZone.getTimeZone(ZoneId.of("GMT"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(timeZone);
        System.out.println("邮件时间【格式化格林威治时间】：" + sdf.format(message.getDate()));
        System.out.println("ReplyTo：" + message.getReplyTo());
        System.out.println("Parent：" + message.getParent());
        HeaderImpl header = (HeaderImpl) message.getHeader();
        System.out.println("Header = " + header);
        List<Field> fields = header.getFields();
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            //System.out.println((i + 1) + "---" + field + "---" + field.getName() + "---" + field.getRaw() + "---" + field.getBody() + "---" + field.getNameLowerCase());
        }
        MultipartImpl body = (MultipartImpl) message.getBody();
        System.out.println("body = " + body);
        System.out.println("Epilogue = " + body.getEpilogue());
        System.out.println("Preamble = " + body.getPreamble());
        System.out.println("SubType = " + body.getSubType());
        System.out.println("EpilogueRaw = " + body.getEpilogueRaw());
        System.out.println("PreambleRaw = " + body.getPreambleRaw());
        System.out.println("BodyParts = " + body.getBodyParts());

        List<Entity> bodyParts = body.getBodyParts();
        for (int i = 0; i < bodyParts.size(); i++) {
            BodyPart bodyPart = (BodyPart) bodyParts.get(i);
            System.out.println((i + 1) + "----Test.main----begin-----");
            System.out.println(bodyPart.getCharset());
            System.out.println(bodyPart.getContentTransferEncoding());
            System.out.println(bodyPart.getDispositionType());
            System.out.println(bodyPart.getFilename());
            System.out.println(bodyPart.getHeader());
            System.out.println(bodyPart.getMimeType());
            System.out.println(bodyPart.getBody());
            Body bodyContent = bodyPart.getBody();
            if (bodyContent instanceof TextBody) {
                TextBody textBody = (TextBody) bodyContent;
                System.out.println("文本内容：" + IOUtils.toString(textBody.getReader()));
            } else if (bodyContent instanceof BinaryBody) {
                BinaryBody binaryBody = (BinaryBody) bodyContent;
                IOUtils.copy(binaryBody.getInputStream() , new FileOutputStream("c:\\eml\\attach\\" + bodyPart.getFilename()));
            }

            System.out.println(bodyPart.getParent());
            System.out.println(bodyPart.isMultipart());
            System.out.println((i + 1) + "----Test.main----end-----");
        }

        System.out.println("ContentTypeParameters = " + body.getContentTypeParameters());
        System.out.println("Count = " + body.getCount());
        System.out.println("Parent = " + body.getParent());
    }

}
