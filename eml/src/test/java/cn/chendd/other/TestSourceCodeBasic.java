package cn.chendd.other;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.io.BufferedLineReaderInputStream;
import org.apache.james.mime4j.message.DefaultMessageBuilder;
import org.apache.james.mime4j.message.MultipartImpl;
import org.apache.james.mime4j.message.SimpleContentHandler;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.MimeConfig;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 测试源代码中的api文件
 * @author chendd
 */
@Deprecated
public class TestSourceCodeBasic {

    public static void main(String[] args) throws Exception {
        Message message = Message.Builder.of(new FileInputStream("文件.eml")).build();
        System.out.println(message.getCharset());
        MultipartImpl body = (MultipartImpl) message.getBody();
        System.out.println(body.getParent());

        List<Entity> parts = body.getBodyParts();
        for (Entity part : parts) {
            System.out.println(part.getFilename());
            System.out.println(part.getCharset());
            System.out.println(part.getMimeType());
            System.out.println(part.getBody());
            System.out.println(part.getContentTransferEncoding());
            System.out.println(part.getDispositionType());
            System.out.println(part.getParent());
            Header header = part.getHeader();
            List<Field> fields = header.getFields();
            for (Field field : fields) {
                if ("Content-Type".equalsIgnoreCase(field.getName())) {
                    System.out.println(String.format("name = %20s | body = %20s | raw = %30s", field.getName(), field.getBody(), new String(field.getRaw().toByteArray() , "gb2312")));
                }
            }
            //if (true) return;
        }
        System.out.println("===========TestSourceCodeBasic.main===========");
        MimeConfig config = MimeConfig.DEFAULT;
        MimeStreamParser parser = new MimeStreamParser(config);
        parser.setContentHandler(new ContentHandler() {
            @Override
            public void startMessage() throws MimeException {
                System.out.println("TestSourceCodeBasic.startMessage");
            }

            @Override
            public void endMessage() throws MimeException {
                System.out.println("TestSourceCodeBasic.endMessage");
            }

            @Override
            public void startBodyPart() throws MimeException {
                System.out.println("TestSourceCodeBasic.startBodyPart");
            }

            @Override
            public void endBodyPart() throws MimeException {
                System.out.println("TestSourceCodeBasic.endBodyPart");
            }

            @Override
            public void startHeader() throws MimeException {
                System.out.println("TestSourceCodeBasic.startHeader");
            }

            @Override
            public void field(Field rawField) throws MimeException {
                System.out.println("TestSourceCodeBasic.field");
            }

            @Override
            public void endHeader() throws MimeException {
                System.out.println("TestSourceCodeBasic.endHeader");
            }

            @Override
            public void preamble(InputStream is) throws MimeException, IOException {
                System.out.println("TestSourceCodeBasic.preamble");
            }

            @Override
            public void epilogue(InputStream is) throws MimeException, IOException {
                System.out.println("TestSourceCodeBasic.epilogue");
            }

            @Override
            public void startMultipart(BodyDescriptor bd) throws MimeException {
                System.out.println("TestSourceCodeBasic.startMultipart");
            }

            @Override
            public void endMultipart() throws MimeException {
                System.out.println("TestSourceCodeBasic.endMultipart");
            }

            @Override
            public void body(BodyDescriptor bd, InputStream is) throws MimeException, IOException {
                System.out.println("TestSourceCodeBasic.body");
            }

            @Override
            public void raw(InputStream is) throws MimeException, IOException {
                System.out.println("TestSourceCodeBasic.raw");
            }
        });

        parser.setContentHandler(new SimpleContentHandler() {
            @Override
            public void headers(Header header) {
                List<Field> fields = header.getFields();
                for (Field field : fields) {
                    if ("Content-Type".equalsIgnoreCase(field.getName())) {
                        System.out.println(field.getName() + "\t" + field.getNameLowerCase() + "\t" + field.getBody() + "\t" + field.getRaw());
                    }
                    //System.out.println(new String(field.getRaw().toByteArray()));
                }
                //System.out.println(fields);
            }
        });

        InputStream instream = new FileInputStream("c:\\eml\\11.eml");
        try {
            parser.parse(instream);
        } finally {
            instream.close();
        }


    }

}
