package cn.chendd.other;

import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.dom.*;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.field.AddressListFieldImpl;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.message.HeaderImpl;
import org.apache.james.mime4j.message.MultipartImpl;
import org.apache.james.mime4j.stream.Field;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.List;
import java.util.TimeZone;

/**
 * 测试正式邮件示例，附件在附件栏
 *
 * @author chendd
 */
@SuppressWarnings("all")
public class TestProdData {

    public static void main(String[] args) throws Exception {
        Message message = Message.Builder.of(new FileInputStream("文件.eml")).build();
        System.out.println("邮件ID：" + message.getMessageId());
        System.out.println("邮件标题：" + message.getSubject());
        System.out.println("邮件编码：" + message.getCharset());
        System.out.println("内容编码：" + message.getContentTransferEncoding());
        System.out.println("DispositionType：" + message.getDispositionType());
        System.out.println("文件名称：" + message.getFilename());
        System.out.println("文档类型：" + message.getMimeType());
        System.out.println("发送人：" + message.getFrom());
        Field from = message.getHeader().getField("From");
        System.out.println(from.getRaw());
        System.out.println(from);
        System.out.println("接收人：" + message.getTo());
        Field fieldTo = message.getHeader().getField("To");
        AddressListField addressField = (AddressListField) fieldTo;
        System.out.println("111--->" + addressField.getRaw());
        System.out.println("222--->" + addressField);
        AddressList to = message.getTo();
        for (Address address : to) {
            Mailbox box = (Mailbox) address;
            System.out.println(box.getAddress() + "----" + box);
        }
        System.out.println("抄送人：" + message.getCc());
        System.out.println("密送人：" + message.getBcc());
        TimeZone timeZone = TimeZone.getTimeZone(ZoneId.of("GMT"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(timeZone);
        System.out.println("邮件时间【格式化格林威治时间】：" + sdf.format(message.getDate()));
        System.out.println("ReplyTo：" + message.getReplyTo());
        System.out.println("Parent：" + message.getParent());
        HeaderImpl header = (HeaderImpl) message.getHeader();
        System.out.println("Header = " + header);
        List<Field> fields = header.getFields();
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            System.out.println((i + 1) + "---" + field + "---" + field.getName() + "---" + field.getRaw() + "---" + field.getBody() + "---" + field.getNameLowerCase());
        }
        MultipartImpl body = (MultipartImpl) message.getBody();
        System.out.println("body = " + body);
        System.out.println("Epilogue = " + body.getEpilogue());
        System.out.println("Preamble = " + body.getPreamble());
        System.out.println("SubType = " + body.getSubType());
        System.out.println("EpilogueRaw = " + body.getEpilogueRaw());
        System.out.println("PreambleRaw = " + body.getPreambleRaw());
        System.out.println("BodyParts = " + body.getBodyParts());

        List<Entity> bodyParts = body.getBodyParts();
        for (int i = 0; i < bodyParts.size(); i++) {
            BodyPart bodyPart = (BodyPart) bodyParts.get(i);
            System.out.println((i + 1) + "----Test.main----begin-----");
            System.out.println(bodyPart.getCharset());
            System.out.println(bodyPart.getContentTransferEncoding());
            System.out.println(bodyPart.getDispositionType());
            System.out.println(bodyPart.getFilename());
            System.out.println(bodyPart.getHeader());
            System.out.println(bodyPart.getMimeType());
            System.out.println(bodyPart.getBody());
            Body bodyContent = bodyPart.getBody();
            System.out.println("bodyContent instanceof " + bodyContent.getClass());
            if (bodyContent instanceof TextBody) {
                TextBody textBody = (TextBody) bodyContent;
                System.out.println("文本内容：" + IOUtils.toString(textBody.getReader()));
            } else if (bodyContent instanceof BinaryBody) {
                BinaryBody binaryBody = (BinaryBody) bodyContent;
                File folder = new File("c:\\eml\\1111");
                if (! folder.exists()) {
                    folder.mkdirs();
                }
                IOUtils.copy(binaryBody.getInputStream() , new FileOutputStream(new File(folder , bodyPart.getFilename())));
            } else if (bodyContent instanceof MultipartImpl) {
                MultipartImpl multipart = (MultipartImpl) bodyContent;
                System.out.println(multipart);
                List<Entity> parts = multipart.getBodyParts();
                for (Entity part : parts) {
                    System.out.println(part.getBody() + "-----------class---" + part.getFilename());
                    Body partBody = part.getBody();
                    if (partBody instanceof TextBody) {
                        TextBody textBody = (TextBody) partBody;
                        System.out.println("文本内容：" + IOUtils.toString(textBody.getReader()));
                    } else if (partBody instanceof BinaryBody) {
                        BinaryBody binaryBody = (BinaryBody) partBody;
                        File folder = new File("c:\\eml\\2222");
                        if (! folder.exists()) {
                            folder.mkdirs();
                        }
                        try {
                            IOUtils.copy(binaryBody.getInputStream() , new FileOutputStream(new File(folder , part.getFilename())));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("TestOnlyComplex.main---------------------------------------->else");
                    }
                }
            } else {
                System.out.println("TestOnlyImage.main-->" + bodyContent.getClass() + "---" + bodyContent);
            }

            System.out.println(bodyPart.getParent());
            System.out.println(bodyPart.isMultipart());
            System.out.println((i + 1) + "----Test.main----end-----");
        }

        System.out.println("ContentTypeParameters = " + body.getContentTypeParameters());
        System.out.println("Count = " + body.getCount());
        System.out.println("Parent = " + body.getParent());


    }

}
