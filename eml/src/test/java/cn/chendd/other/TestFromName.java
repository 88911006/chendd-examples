package cn.chendd.other;

import org.apache.commons.lang3.StringUtils;

import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 解析 发件人From 参数中得昵称和实际邮件名称
 *
 * @author chendd
 */
//@RunWith(JUnit4.class)
public class TestFromName {

//    @Test
    public void testFromSimple() throws Exception {
        String text = "From: abcdefg@qq.com";
        disposeFromName(text);
        testFromEmail(text);
    }

//    @Test
    public void testFromCharsetUtf8() throws Exception {
        String text = "From: =?UTF-8?B?abdef=?=\n" +
                " <abcde02@qq.com>";
        disposeFromName(text);
        testFromEmail(text);
    }

//    @Test
    public void testFromCharsetGb2312() throws Exception {
        String text = "From: =?GB2312?B?文本数据=?=\n" +
                " <abcde02@qq.com>";
        disposeFromName(text);
        testFromEmail(text);
    }

//    @Test
    public void testFromCharsetMoreUtf8() throws Exception {
        String text = "Content-Type: text/plain;\n" +
                "\tname=\"=?utf-8?B?第1段/k?=\n" +
                " =?utf-8?B?第2段+hLnhs?=\n" +
                " =?utf-8?B?cw==?=\"\n" +
                "Content-Disposition: attachment\n" +
                "Content-Transfer-Encoding: quoted-printable\n" +
                "\n";
        System.out.println(getName(text));
    }

    private void testFromEmail(String text) {
        System.out.println(String.format("Email地址：%s" , StringUtils.substringBetween(text , "<" , ">")));

    }

    public static String getName(String text) {
        String regex = "\\?B\\?(.*)\\?=";
        String charsetName = StringUtils.substringBetween(text, "=?", "?B?");
        Matcher matcher = Pattern.compile(regex , Pattern.CASE_INSENSITIVE).matcher(text);
        StringBuilder builder = new StringBuilder();
        while (matcher.find()) {
            builder.append(matcher.group(1));
        }
        try {
            return new String(Base64.getDecoder().decode(builder.toString()) , charsetName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void disposeFromName(String text) throws Exception {
        String regex = "\\?([0-9a-zA-z\\-]+)\\?B\\?";
        Matcher matcher = Pattern.compile(regex , Pattern.CASE_INSENSITIVE).matcher(text);
        String charsetName = null;
        if (matcher.find()) {
            charsetName = matcher.group(1);
        }
        String nikeName = "\\?B\\?(.*)\\?=";
        Matcher nikeNameMatcher = Pattern.compile(nikeName , Pattern.CASE_INSENSITIVE).matcher(text);
        String nikeNameValue = null;
        if (nikeNameMatcher.find()) {
            nikeNameValue = nikeNameMatcher.group(1);
        }
        System.out.println(String.format("编码类型：%s，Base64编码：%s，解码后：%s" , charsetName , nikeNameValue ,
                new String(Base64.getDecoder().decode(nikeNameValue.getBytes()) , charsetName)));
    }

}
