package cn.chendd.other;

import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.message.HeaderImpl;
import org.apache.james.mime4j.stream.Field;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Base64;
import java.util.List;
import java.util.TimeZone;

/**
 * 纯html段落内容的邮件内容获取--测试
 *
 * @author chendd
 */
@SuppressWarnings("all")
public class TestReplyTo {

    public static void main(String[] args) throws Exception {
        Message message = Message.Builder.of(new FileInputStream("文件.eml")).build();
        System.out.println("邮件ID：" + message.getMessageId());
        System.out.println("邮件标题：" + message.getSubject());
        System.out.println("邮件编码：" + message.getCharset());
        System.out.println("内容编码：" + message.getContentTransferEncoding());
        System.out.println("DispositionType：" + message.getDispositionType());
        System.out.println("文件名称：" + message.getFilename());
        System.out.println("文档类型：" + message.getMimeType());
        System.out.println("发送人：" + message.getFrom());

        System.out.println("接收人：" + message.getTo());
        System.out.println("抄送人：" + message.getCc());
        System.out.println("密送人：" + message.getBcc());
        TimeZone timeZone = TimeZone.getTimeZone(ZoneId.of("GMT"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(timeZone);
        System.out.println("邮件时间【格式化格林威治时间】：" + sdf.format(message.getDate()));
        System.out.println("ReplyTo：" + message.getReplyTo());
        System.out.println("Parent：" + message.getParent());
        HeaderImpl header = (HeaderImpl) message.getHeader();
        System.out.println("Header = " + header);
        List<Field> fields = header.getFields();
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            System.out.println((i + 1) + "---" + field + "---" + field.getName() + "---" + field.getRaw() + "---" + field.getBody() + "---" + field.getNameLowerCase());
        }
        System.out.println(message.getBody().getClass());
        TextBody bodyContent = (TextBody) message.getBody();
        if (bodyContent instanceof TextBody) {
            TextBody body = (TextBody) bodyContent;
        }
        System.out.println("body = " + bodyContent);
        System.out.println(bodyContent.getMimeCharset());
        System.out.println(IOUtils.toString(bodyContent.getReader()));
    }

}
