package cn.chendd.beans;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统属性信息
 * @auth chendd
 * @date 2019/11/23 21:48
 */
@Data
@ApiModel
@TableName("Sys_Property")
public class SysProperty {

    @ApiModelProperty(value = "主键ID")
    @TableField("spId")
    private Integer spId;

    @ApiModelProperty(value = "年龄")
    @TableField("age")
    private Integer age;

    @ApiModelProperty(value = "创建日期")
    @TableField("createDate")
    private java.util.Date createDate;

    @ApiModelProperty(value = "性别，男=true，女=false")
    @TableField("sex")
    private Integer sex;

    @ApiModelProperty(value = "数据状态，可用或禁用" , example = "DISABLE" , hidden = true)
    @TableField("dataStatus")
    @TableLogic
    @JsonIgnore
    private String dataStatus;

    @ApiModelProperty(value = "文件类型对象")
    @TableField("file")
    private java.io.InputStream file;

    @ApiModelProperty(value = "用户名")
    @TableField("userName")
    private String userName;


}
