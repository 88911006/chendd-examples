import com.intellij.database.model.DasTable
import com.intellij.database.util.Case
import com.intellij.database.util.DasUtil

import java.text.SimpleDateFormat
import java.util.Map
import java.util.HashMap

/*
 * Available context bindings:
 *   SELECTION   Iterable<DasObject>
 *   PROJECT     project
 *   FILES       files helper
 */

packageName = "cn"//默认的包路径，后续替换
author = System.getProperty("user.name")
pkIdText = "\u4e3b\u952eID";//约定含有“主键ID”的文本注释列为主键
dateTime = getDateTime();
typeMapping = [
        (~/(?i)bigint/)                   : "Long",
        (~/(?i)int/)                      : "Integer",
        (~/(?i)number/)                      : "BigDecimal",
        (~/(?i)float|double|decimal|real/): "Double",
        (~/(?i)tinyint/)                  : "Boolean",
        (~/(?i)datetime|timestamp|date/)  : "Date",
        (~/(?i)blob|binary|bfile|clob|raw|image/): "InputStream",
        (~/(?i)/)                         : "String"
]

def getPackageName(dir) {
    return dir.toString().replaceAll("\\\\", ".").replaceAll("/", ".")
            .replaceAll("^.*src(\\.main\\.java\\.)?", "") + ";"
}

def getDateTime(){
    return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date());
}

def getImportMapping(){
    Map<String , String> map = new HashMap();
    map.put("BigDecimal" , "import java.math.BigDecimal;")
    map.put("Date" , "import java.util.Date;")
    map.put("InputStream" , "import java.io.InputStream;")
    return map;
}


FILES.chooseDirectoryAndSave("Choose directory", "Choose where to store generated files") { dir ->
    SELECTION.filter { it instanceof DasTable }.each { generate(it, dir) }
}

def generate(table, dir) {
    def className = javaName(table.getName(), true)
    def fields = calcFields(table)
    new File(dir, className + ".java").withPrintWriter("UTF-8") { out -> generate(dir , table , out, className, fields) }
}

/**
 * 生成的时候，选择项目的代码路径，从包路径中找src/java/main路径后的路径开始替换
 */
def generate(dir, table, out, className, fields) {
    packageName = getPackageName(dir);
    tableName = table.getName();
    out.println "package $packageName"
    out.println ""
    //约定按照列的描述中含有主键ID的字段备注为主键，某些关系表中的第一个字段不一定就是主键
    existPk(fields , out);
    out.println "import javax.persistence.*;"
    out.println "import io.swagger.annotations.ApiModel;"
    out.println "import io.swagger.annotations.ApiModelProperty;"
    out.println "import lombok.Data;"

    Set<String> types = new HashSet<>();
    fields.each() {
        types.add(it.type)
    }

    Iterator<String> it = types.iterator();
    while(it.hasNext()){
        String next = it.next();
        if(getImportMapping().containsKey(next)){
            out.println getImportMapping().get(next)
        }
    }

    out.println ""
    out.println "/**"
    out.println " * " + table.getComment()
    out.println " * @auth $author"
    out.println " * @date $dateTime"
    out.println " */"
    out.println "@ApiModel"
    out.println "@Entity"
    out.println "@Data"
    out.println "@Table(name=\"$tableName\")"
    out.println "public class $className {"
    out.println ""
    fields.each() {
        if (it.annos != "") out.println "  ${it.annos}"
        if (it.comment != null && it.comment.indexOf(pkIdText) != -1){
            out.println "    @Id"
            out.println "    @GeneratedValue(generator = \"main-uuid\")"
            out.println "    @GenericGenerator(name = \"main-uuid\", strategy = \"uuid\")"
        }
        out.println "    @Column(name = \"${it.column}\")"
        out.println "    @ApiModelProperty(value = \"${it.comment}\")"
        out.println "    private " + it.type + " ${it.name};"
        out.println ""
    }
    //get set 使用lombok
    /*fields.each() {
      out.println ""
      out.println "    public ${it.type} get${it.name.capitalize()}() {"
      out.println "        return ${it.name};"
      out.println "    }"
      out.println ""
      out.println "    public void set${it.name.capitalize()}(${it.type} ${it.name}) {"
      out.println "        this.${it.name} = ${it.name};"
      out.println "    }"
      out.println ""
    }*/
    out.println "}"
}

def calcFields(table) {
    DasUtil.getColumns(table).reduce([]) { fields, col ->
        def spec = Case.LOWER.apply(col.getDataType().getSpecification())
        def typeStr = typeMapping.find { p, t -> p.matcher(spec).find() }.value
        fields += [[
                           name : javaName(col.getName(), false), //表字段名称转为Java命名
                           type : typeStr, //表字段类型
                           comment: col.getComment(), //表字段注释
                           column: col.getName(),
                           annos: ""]]
    }
}

def javaName(str, capitalize) {
    def s = com.intellij.psi.codeStyle.NameUtil.splitNameIntoWords(str)
            .collect { Case.LOWER.apply(it).capitalize() }
            .join("")
            .replaceAll(/[^\p{javaJavaIdentifierPart}[_]]/, "_")
    capitalize || s.length() == 1? s : Case.LOWER.apply(s[0]) + s[1..-1]
}

def existPk(fields , out){
    fields.each() {
        if (it.comment != null && it.comment.indexOf(pkIdText) != -1){
            out.println "import org.hibernate.annotations.GenericGenerator;"
        }
    }
}
