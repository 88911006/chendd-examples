package cn.chendd.example.jpa.user.repository;

import cn.chendd.example.jpa.base.BaseRepository;
import cn.chendd.example.jpa.user.entity.User;

/**
 * Example查询
 *
 * @author chendd
 * @date 2020/4/28 21:33
 */
public interface UserExampleQueryRespository extends BaseRepository<User, String> {


}