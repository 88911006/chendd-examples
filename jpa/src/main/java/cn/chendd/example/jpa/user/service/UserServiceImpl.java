package cn.chendd.example.jpa.user.service;

import cn.chendd.example.jpa.user.entity.User;
import cn.chendd.example.jpa.user.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * UserService接口实现
 *
 * @author chendd
 * @date 2020/4/21 20:16
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

}
