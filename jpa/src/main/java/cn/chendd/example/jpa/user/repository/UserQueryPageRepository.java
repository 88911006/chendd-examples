package cn.chendd.example.jpa.user.repository;

import cn.chendd.example.jpa.base.BaseRepository;
import cn.chendd.example.jpa.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;

/**
 * 查询分页
 *
 * @author chendd
 * @date 2020/5/2 21:22
 */
@RepositoryDefinition(domainClass = User.class , idClass = String.class)
public interface UserQueryPageRepository extends BaseRepository<User, String> {

    @Query(countQuery = "select count(*) from user" ,
           value = "select a.* from user a where 1=1" ,
           nativeQuery = true)
    Page<User> queryUsersPage(Pageable pageable);

}
