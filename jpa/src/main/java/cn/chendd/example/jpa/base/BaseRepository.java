package cn.chendd.example.jpa.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;

/**
 * BaseRepository
 *
 * @author chendd
 * @date 2020/4/21 20:50
 */
public interface BaseRepository<T, ID extends Serializable>
        extends
        JpaRepository<T, ID>,
        JpaSpecificationExecutor<T>,
        PagingAndSortingRepository<T, ID> {

}
