package cn.chendd.example.jpa.nativequery.enums;

import cn.chendd.example.jpa.nativequery.SQLQuery;
import cn.chendd.example.jpa.nativequery.SQLQueryBean;
import cn.chendd.example.jpa.nativequery.SQLQueryMap;

/**
 * @author chendd
 * @date 2020/5/6 22:55
 */
public enum ResultSetType {

    Map(SQLQueryMap.class),
    Bean(SQLQueryBean.class),
    ;

    private Class<? extends SQLQuery> className;

    ResultSetType(Class<? extends SQLQuery> className) {
        this.className = className;
    }

    public Class<? extends SQLQuery> getClassName() {
        return className;
    }

    public void setClassName(Class<? extends SQLQuery> className) {
        this.className = className;
    }
}