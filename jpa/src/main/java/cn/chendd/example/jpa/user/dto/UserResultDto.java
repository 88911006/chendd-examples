package cn.chendd.example.jpa.user.dto;

import cn.chendd.example.jpa.nativequery.annotions.Column;
import lombok.Data;
import org.hibernate.type.StringType;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * UserResultDto
 *
 * @author chendd
 * @date 2020/5/6 0:29
 */
@Data
public class UserResultDto {

    @Column
    private Long id;
    @Column(name = "userName" , type = StringType.class)
    private String userName;
    @Column
    private Timestamp createTime;//或者Date类型
    @Column
    private BigDecimal amount;
    @Column
    private Double pi;
    @Column
    private Boolean status;

}
