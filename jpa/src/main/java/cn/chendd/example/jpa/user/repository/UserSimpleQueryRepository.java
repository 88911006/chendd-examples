package cn.chendd.example.jpa.user.repository;

import cn.chendd.example.jpa.base.BaseRepository;
import cn.chendd.example.jpa.user.entity.User;

import java.util.Date;
import java.util.List;

/**
 * 简单查询
 *
 * @author chendd
 * @date 2020/4/22 21:04
 */
public interface UserSimpleQueryRepository extends BaseRepository<User, String> {

    /**
     * 查询列表：name与email属性，匹配模式：等值匹配
     */
    List<User> queryUsersByNameAndEmail(String name , String email);

    /**
     * 查询条件：createTime与name属性，匹配模式：between and 与 like %%
     */
    List<User> queryUsersByCreateTimeBetweenAndNameLike(Date begin , Date end , String name);

}
