package cn.chendd.example.jpa.user.repository;

import cn.chendd.example.jpa.nativequery.annotions.Query;
import cn.chendd.example.jpa.nativequery.annotions.QueryResult;
import cn.chendd.example.jpa.nativequery.enums.ResultSetType;
import cn.chendd.example.jpa.user.dao.NaiveQueryForMapDao;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

/**
 * @author chendd
 * @date 2020/5/6 23:19
 */
public interface MapResultSqlQuery {

    @Query(sql = "select * from user limit 1" , entityManager = NaiveQueryForMapDao.class ,
            resultSet = @QueryResult(resultSetType = ResultSetType.Map))
    Map<String , Object> queryForMap();

    @Query(sql = "select * from user" , entityManager = NaiveQueryForMapDao.class ,
            resultSet = @QueryResult(resultSetType = ResultSetType.Map))
    List<Map<String , Object>> queryForList();

    @Query(sql = "select * from user where name = :name " , entityManager = NaiveQueryForMapDao.class ,
            resultSet = @QueryResult(resultSetType = ResultSetType.Map))
    List<Map<String , Object>> queryForList(@Param("name") String name);

    @Query(sql = "select * from user where (name = :name or :name is null) " +
            "and (:sexFlag is null or sex in (:sexList))" , entityManager = NaiveQueryForMapDao.class ,
            resultSet = @QueryResult(resultSetType = ResultSetType.Map))
    List<Map<String, Object>> queryForList(@Param("name") String name , @Param("sexFlag") String sexFlag, @Param("sexList") List<String> sexList);
}
