package cn.chendd.example.jpa.nativequery.annotions;

import cn.chendd.example.jpa.nativequery.enums.QueryColumnScope;
import cn.chendd.example.jpa.nativequery.enums.ResultSetType;
import org.springframework.lang.NonNull;

import java.lang.annotation.*;

/**
 * @author chendd
 * @date 2020/5/6
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface QueryResult {

    ResultSetType resultSetType() default ResultSetType.Bean;

    String[] include() default {};

}