package cn.chendd.example.jpa.user.dao;

import cn.chendd.example.jpa.user.repository.MapResultSqlQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author chendd
 * @date 2020/5/6 23:16
 */
@Repository
@Transactional(readOnly = true)
public class NaiveQueryForMapDao extends BaseDao4SqlQuery {

    public Map<String , Object> queryForMap() {
        MapResultSqlQuery mapResultSqlQuery = super.getEntityManager(MapResultSqlQuery.class);
        Map<String, Object> map = mapResultSqlQuery.queryForMap();
        return map;
    }

    public List<Map<String , Object>> queryForList() {
        MapResultSqlQuery mapResultSqlQuery = super.getEntityManager(MapResultSqlQuery.class);
        List<Map<String, Object>> list = mapResultSqlQuery.queryForList();
        return list;
    }

    public List<Map<String , Object>> queryForList(String name) {
        MapResultSqlQuery mapResultSqlQuery = super.getEntityManager(MapResultSqlQuery.class);
        List<Map<String, Object>> list = mapResultSqlQuery.queryForList(name);
        return list;
    }

    public List<Map<String, Object>> queryForList(String name, List<String> sexList) {
        MapResultSqlQuery mapResultSqlQuery = super.getEntityManager(MapResultSqlQuery.class);
        List<Map<String, Object>> list = mapResultSqlQuery.queryForList(name , "sex" , sexList);
        return list;
    }
}
