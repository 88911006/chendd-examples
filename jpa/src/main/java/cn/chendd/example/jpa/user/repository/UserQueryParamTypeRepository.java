package cn.chendd.example.jpa.user.repository;

import cn.chendd.example.jpa.base.BaseRepository;
import cn.chendd.example.jpa.user.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 查询参数类型传值方式
 *
 * @author chendd
 * @date 2020/4/30 23:26
 */
public interface UserQueryParamTypeRepository extends BaseRepository<User, String> {

    /**
     * 第一种参数传递，使用 ? 占位符
     * 传统预编译参数传值方式
     */
    @Query(value = "select * from user where name = ? " +
            "and email like concat(concat('%',?),'%')" , nativeQuery = true)
    List<User> queryUsersByNameAndEmailForOneType(String name , String email);

    /**
     * 第二种参数传递，使用 ?数字 占位符
     * ?数字释义：?为占位符，数字为获取的参数下标
     */
    @Query(value = "select * from user where (name = ?1 or ?1 is null) " +
            "and email like concat(concat('%',?2),'%')" , nativeQuery = true)
    List<User> queryUsersByNameAndEmailForTwoType(String name , String email);

    /**
     * 第三种参数传递，使用 :属性名 占位符
     * 用于参数个数为动态长度时，支持数组与集合
     */
    @Query(value = "select * from user where name in (:nameList) and name in (:nameArray) " +
            "and email like concat(concat('%', :email ),'%')" , nativeQuery = true)
    List<User> queryUsersByNameAndEmailForThreeType(@Param("nameList") List<String> nameList ,
                                                   @Param("nameArray") String nameArray[] ,
                                                   @Param("email") String email);

}
