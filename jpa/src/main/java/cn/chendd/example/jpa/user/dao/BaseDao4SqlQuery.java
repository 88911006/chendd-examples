package cn.chendd.example.jpa.user.dao;

import cn.chendd.example.jpa.nativequery.SQLQueryProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author chendd
 * @date 2020/3/27
 */
public abstract class BaseDao4SqlQuery {

    /**
     * entityManager 属于线程不安全的类，需要使用spring构造的方式获取
     */
    @PersistenceContext
    protected EntityManager entityManager;

    public <T> T getEntityManager(Class<T> sqlQueryClass){
        return SQLQueryProvider.getSQLQueryMapper(sqlQueryClass , this.entityManager);
    }

}
