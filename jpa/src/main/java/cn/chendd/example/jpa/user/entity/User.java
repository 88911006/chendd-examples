package cn.chendd.example.jpa.user.entity;

import cn.chendd.example.jpa.user.dto.UserNamedDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.type.LongType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 用户信息表
 * @auth chendd
 * @date 2020/04/21 22:31
 */
@ApiModel
@Entity
@Data
@Table(name="user")
@NamedNativeQueries(
    value = {
        @NamedNativeQuery(name = "User.query_by_name_email" ,
                query = "select id , name , email , sex , createTime from {h-catalog}user" ,
                resultClass = User.class),
        @NamedNativeQuery(name = "User.query_by_name_custom" ,
                query = "select * from {h-catalog}user a where a.name like ?" ,
                resultClass = User.class),
        @NamedNativeQuery(name = "User.query_id_userName_mapping" ,
            query = "select 88911 id , 'chendd' userName" ,
            resultSetMapping = "UserResultDto.query_id_userName_mapping")
    }
)
@SqlResultSetMapping(
        name = "UserResultDto.query_id_userName_mapping",
        classes = @ConstructorResult(
                targetClass = UserNamedDto.class,
                columns = {
                        @ColumnResult(name = "id" , type = Long.class), //或者LongType.class
                        @ColumnResult(name = "userName")
                }
        )
)
public class User {

    @Id
    @GeneratedValue(generator = "main-uuid")
    @GenericGenerator(name = "main-uuid", strategy = "uuid")
    @Column(name = "id")
    @ApiModelProperty(value = "主键ID")
    private String id;

    @Column(name = "name")
    @ApiModelProperty(value = "姓名")
    private String name;

    @Column(name = "sex")
    @ApiModelProperty(value = "性别枚举类SexEnum")
    private String sex;

    @Column(name = "email")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Column(name = "createTime")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;

    @Transient
    private List<String> statusList;

}
