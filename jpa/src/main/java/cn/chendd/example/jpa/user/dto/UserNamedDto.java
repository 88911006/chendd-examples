package cn.chendd.example.jpa.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chendd
 * @date 2020/5/16 18:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserNamedDto {

    private Long id;
    private String userName;

}
