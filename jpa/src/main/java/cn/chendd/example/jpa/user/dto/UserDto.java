package cn.chendd.example.jpa.user.dto;

/**
 * UserDto
 *
 * @author chendd
 * @date 2020/5/2 20:48
 */
public interface UserDto {

    String getName();

    String getEmail();

    String getRemark();

    default String getDefaultRemark() {
        return this.getName() + "_" + this.getEmail();
    }

}
