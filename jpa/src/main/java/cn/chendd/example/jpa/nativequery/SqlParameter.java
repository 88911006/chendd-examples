package cn.chendd.example.jpa.nativequery;

import java.util.List;

/**
 * @author chendd
 * @date 2020/5/6 22:51
 */
public class SqlParameter {

    private String sql;

    private List<Object> paramList;

    public SqlParameter(String sql) {
        this.sql = sql;
    }

    public SqlParameter(String sql , List<Object> paramList) {
        this.sql = sql;
        this.paramList = paramList;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<Object> getParamList() {
        return paramList;
    }

    public void setParamList(List<Object> paramList) {
        this.paramList = paramList;
    }
}

