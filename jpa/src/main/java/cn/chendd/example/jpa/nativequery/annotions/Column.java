package cn.chendd.example.jpa.nativequery.annotions;

import org.hibernate.type.AbstractStandardBasicType;
import org.hibernate.type.StandardBasicTypeTemplate;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;

import java.lang.annotation.*;

/**
 * @author chendd
 * @date 2020/5/6 22:42
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Column {

    /**
     * 字段类型，默认为动态赋值
     */
    Class<? extends Type> type() default Type.class;

    /**
     * 查询后绑定的属性名称，默认为动态取变量名
     */
    String name() default "";

}

