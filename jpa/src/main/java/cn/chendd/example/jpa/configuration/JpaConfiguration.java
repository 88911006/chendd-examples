package cn.chendd.example.jpa.configuration;

import cn.chendd.example.jpa.base.BaseRepositoryImpl;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * 系统配置文件配置导入
 *
 * @author chendd
 * @date 2020/1/7 22:22
 */
@Configuration
@EnableJpaRepositories(basePackages = "cn.chendd.example.jpa.**.repository" ,
        repositoryBaseClass = BaseRepositoryImpl.class , repositoryFactoryBeanClass = JpaRepositoryFactoryBeanClass.class)
@EntityScan(basePackages = "cn.chendd.example.jpa.**.entity")
public class JpaConfiguration {


}
