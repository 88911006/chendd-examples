package cn.chendd.example.jpa.nativequery;

import javax.persistence.EntityManager;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author chendd
 * @date 2020/3/28
 */
public class SQLQueryProvider {

    @SuppressWarnings("unchecked")
    public static <T> T getSQLQueryMapper(Class<T> sqlQueryClass, EntityManager entityManager) {
        // 创建动态代理对象
        InvocationHandler handler = new SQLQueryHanlder(entityManager);
        Object proxy = Proxy.newProxyInstance(sqlQueryClass.getClassLoader(), new Class[] {sqlQueryClass}, handler);
        return (T) proxy;
    }

}
