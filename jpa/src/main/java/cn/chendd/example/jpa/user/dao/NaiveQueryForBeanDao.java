package cn.chendd.example.jpa.user.dao;

import cn.chendd.example.jpa.user.dto.UserResultDto;
import cn.chendd.example.jpa.user.repository.BeanResultSqlQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author chendd
 * @date 2020/5/6 23:16
 */
@Repository
@Transactional(readOnly = true)
public class NaiveQueryForBeanDao extends BaseDao4SqlQuery {

    public UserResultDto queryForBean() {
        BeanResultSqlQuery beanResultSqlQuery = super.getEntityManager(BeanResultSqlQuery.class);
        UserResultDto bean = beanResultSqlQuery.queryForBean();
        return bean;
    }

    public List<UserResultDto> queryForList() {
        BeanResultSqlQuery beanResultSqlQuery = super.getEntityManager(BeanResultSqlQuery.class);
        List<UserResultDto> beanList = beanResultSqlQuery.queryForList();
        return beanList;
    }

    public List<UserResultDto> queryForListForPart() {
        BeanResultSqlQuery beanResultSqlQuery = super.getEntityManager(BeanResultSqlQuery.class);
        List<UserResultDto> beanList = beanResultSqlQuery.queryForListForPart();
        return beanList;
    }

    public List<UserResultDto> queryForListForParam(Long userId , String userName) {
        BeanResultSqlQuery beanResultSqlQuery = super.getEntityManager(BeanResultSqlQuery.class);
        List<UserResultDto> beanList = beanResultSqlQuery.queryForListForParam(userId , userName);
        return beanList;
    }

    public List<UserResultDto> testForListByVariableParam(Long limit) {
        BeanResultSqlQuery beanResultSqlQuery = super.getEntityManager(BeanResultSqlQuery.class);
        List<UserResultDto> beanList = beanResultSqlQuery.queryForListForVariableParam(limit);
        return beanList;
    }


}
