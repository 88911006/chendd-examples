package cn.chendd.example.jpa.user.repository;

import cn.chendd.example.jpa.base.BaseRepository;
import cn.chendd.example.jpa.user.dto.UserDto;
import cn.chendd.example.jpa.user.entity.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 查询返回接口类型
 *
 * @author chendd
 * @date 2020/5/2 20:44
 */
public interface UserQueryInterfaceResultRepository extends BaseRepository<User, String> {

    @Query(value = "select name , email , concat(concat(name , '_'),email) as remark " +
            "from user where name = ?" , nativeQuery = true)
    List<UserDto> queryUsersByName(String name);

}
