package cn.chendd.example.jpa.user.repository;

import cn.chendd.example.jpa.base.BaseRepository;
import cn.chendd.example.jpa.user.entity.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 用户Repository
 *
 * @author chendd
 * @date 2020/4/21 0:56
 */
public interface UserRepository extends BaseRepository<User, String> {

    @Query(nativeQuery = true , value = "select id , name from User where 1 = 1 and ?#{cdd.chendd} = '666' and 'hello' = ?#{hello()} ")
    List<User> queryAll();

}
