package cn.chendd.example.jpa.user.service;

import cn.chendd.example.jpa.user.entity.User;

import java.util.List;

/**
 * UserService接口定义
 *
 * @author chendd
 * @date 2020/4/21 20:16
 */
public interface UserService {

    /**
     * 保存或修改User
     * @param user user对象
     * @return user对象
     */
    User saveUser(User user);

    /**
     * 查询所有User列表
     * @return users对象
     */
    List<User> findAll();
}
