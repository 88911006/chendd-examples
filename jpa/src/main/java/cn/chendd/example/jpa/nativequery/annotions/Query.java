package cn.chendd.example.jpa.nativequery.annotions;

import java.lang.annotation.*;

/**
 * @author chendd
 * @date 2020/5/6
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Query {

    String sql() default "";

    Class<?> entityManager();

    QueryResult resultSet() default @QueryResult();

}
