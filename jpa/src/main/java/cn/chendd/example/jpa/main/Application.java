package cn.chendd.example.jpa.main;

import cn.chendd.example.jpa.components.SpringBeanFactory;
import cn.chendd.example.jpa.configuration.ContextConfiguration;
import cn.chendd.example.jpa.configuration.JpaConfiguration;
import cn.chendd.example.jpa.user.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * 启动类
 *
 * @author chendd
 * @date 2020/4/19 22:57
 */
@SpringBootApplication
@Import(value = {ContextConfiguration.class , JpaConfiguration.class})
public class Application {

    /**
     * 服务器启动
     *
     * @param args
     * @return void
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        UserRepository userRepository = SpringBeanFactory.getBean(UserRepository.class);
        System.out.println(userRepository);

    }

}



