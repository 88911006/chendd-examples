package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.dao.UserQueryCustomResultDao;
import cn.chendd.example.jpa.user.dto.UserResultDto;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 查询自定义对象类型结果集
 *
 * @author chendd
 * @date 2020/5/5 22:24
 */
public class UserQueryCustomResultTest extends ApplicationTest {

    @Resource
    private UserQueryCustomResultDao dao;

    @Test
    public void testQueryMapResult() {
        List<Map<String, Object>> dataList = dao.nativeSqlQuery2Map();
        Assert.assertNotNull(dataList);
    }

    @Test
    public void testQueryBeanResult() {
        List<UserResultDto> resultList = dao.nativeSqlQuery2Bean();
        Assert.assertNotNull(resultList);
    }

}
