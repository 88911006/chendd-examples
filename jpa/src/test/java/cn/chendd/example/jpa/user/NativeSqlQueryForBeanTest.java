package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.dao.NaiveQueryForBeanDao;
import cn.chendd.example.jpa.user.dao.NaiveQueryForMapDao;
import cn.chendd.example.jpa.user.dto.UserResultDto;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author chendd
 * @date 2020/5/6 23:26
 */
public class NativeSqlQueryForBeanTest extends ApplicationTest {

    @Resource
    private NaiveQueryForBeanDao dao;

    @Test
    public void testForBean() {
        UserResultDto userResult = dao.queryForBean();
        Assert.assertNotNull(userResult);
    }

    @Test
    public void testForList() {
        List<UserResultDto> userResult = dao.queryForList();
        Assert.assertNotNull(userResult);
    }

    @Test
    public void testForListForPart() {
        List<UserResultDto> userResult = dao.queryForListForPart();
        Assert.assertNotNull(userResult);
    }

    @Test
    public void testForListByDynamicParam() {
        Long userId = 2L;
        String userName = "chen";
        List<UserResultDto> userResult = dao.queryForListForParam(userId , userName);
        Assert.assertNotNull(userResult);
    }

    @Test
    public void testForListByVariableParam() {
        Long limit = 2L;
        List<UserResultDto> userResult = dao.testForListByVariableParam(limit);
        System.out.println(userResult);
        Assert.assertNotNull(userResult);
    }

}
