package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.entity.User;
import cn.chendd.example.jpa.user.repository.UserQueryDynamicParamRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * 测试动态查询条件
 *
 * @author chendd
 * @date 2020/5/1 23:47
 */
public class UserQueryDynamicParamTest extends ApplicationTest {

    @Resource
    private UserQueryDynamicParamRepository repository;

    @Test
    public void testQueryDynamicParamGeneral4() {
        List<User> userList = repository.queryUsersByNameAndEmailGeneral(
                "chendd" , "chendd" , "8866" , "8866");
        Assert.assertNotNull(userList);
    }

    @Test
    public void testQueryDynamicParamGeneral2() {
        List<User> userList = repository.queryUsersByNameAndEmailGeneral(
                "chendd" , "8866");
        Assert.assertNotNull(userList);
    }

    @Test
    public void testQueryDynamicParamList() {
        List<String> emailList = Lists.newArrayList("8877" , "8888");
        List<User> userList = repository.queryUsersByNameAndEmailList(
                "chendd" , CollectionUtils.isEmpty(emailList) ? null : "notNull" , emailList);
        Assert.assertNotNull(userList);
    }

    @Test
    public void testQueryDynamicParamArray() {
        String emailArray[] = {"8866" , "8888"};
        List<User> userList = repository.queryUsersByNameAndEmailArray(
                "chendd" , ArrayUtils.isEmpty(emailArray) ? null : "notNull" , emailArray);
        Assert.assertNotNull(userList);
    }

}
