package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.dto.UserDto;
import cn.chendd.example.jpa.user.repository.UserQueryInterfaceResultRepository;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 测试查询返回自定义接口类型
 *
 * @author chendd
 * @date 2020/5/2 20:50
 */
public class UserQueryInterfaceResultTest extends ApplicationTest {

    @Resource
    private UserQueryInterfaceResultRepository repository;

    @Test
    public void testQueryInterfaceResult() {

        List<UserDto> dataList = repository.queryUsersByName("chendd");
        for (UserDto user : dataList) {
            String remark = user.getRemark();
            String defaultRemark = user.getDefaultRemark();
            Assert.assertEquals(remark , defaultRemark);
        }
    }

}
