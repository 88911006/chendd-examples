package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.dao.NaiveQueryForMapDao;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author chendd
 * @date 2020/5/6 23:26
 */
public class NativeSqlQueryForMapTest extends ApplicationTest {

    @Resource
    private NaiveQueryForMapDao dao;

    @Test
    public void testForMap() {
        Map<String, Object> map = dao.queryForMap();
        Assert.assertNotNull(map);
    }

    @Test
    public void testForList() {
        List<Map<String, Object>> list = dao.queryForList();
        Assert.assertNotNull(list);
    }

    @Test
    public void testForListByParam() {
        List<Map<String, Object>> list = dao.queryForList("chendd");
        Assert.assertNotNull(list);
    }

    @Test
    public void testForListByDynamicParam() {
        List<Map<String, Object>> list = dao.queryForList("chendd" , Lists.newArrayList("男" , "女"));
        Assert.assertNotNull(list);
    }



}
