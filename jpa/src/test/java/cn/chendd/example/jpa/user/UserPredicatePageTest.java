package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.dao.UserPageDao;
import cn.chendd.example.jpa.user.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.domain.Page;

import javax.annotation.Resource;

/**
 * Predicate查询分页
 *
 * @author chendd
 * @date 2020/5/2 22:43
 */
public class UserPredicatePageTest extends ApplicationTest {

    @Resource
    private UserPageDao dao;

    @Test
    public void testUserPredicatePage() {
        User user = new User();
        user.setName("chendd"); //等值查询
        user.setEmail("886"); //模糊查询
        Page<User> page = dao.queryUsersPage(user, 0, 5);
        Assert.assertNotNull(page.getContent());
    }

}
