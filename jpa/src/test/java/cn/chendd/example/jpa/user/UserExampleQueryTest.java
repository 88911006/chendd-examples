package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.entity.User;
import cn.chendd.example.jpa.user.repository.UserExampleQueryRespository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.domain.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * Example查询Test
 *
 * @author chendd
 * @date 2020/4/28 21:34
 */
public class UserExampleQueryTest extends ApplicationTest {

    @Resource
    private UserExampleQueryRespository exampleQueryRespository;

    @Test
    public void testExampleById() {
        User user = new User();
        user.setId("1234567890");
        Example<User> userExample = Example.of(user);
        Optional<User> userOptional = exampleQueryRespository.findOne(userExample);
        Assert.assertNotNull(String.valueOf(Boolean.FALSE), userOptional.isPresent());
    }

    @Test
    public void testExampleByNameAndEmailOrders() {
        User user = new User();
        user.setName("chendd");
        user.setEmail("88911006@qq.com");
        Example<User> userExample = Example.of(user);
        Sort orders = Sort.by(Sort.Order.asc("name"), Sort.Order.desc("sex"));
        List<User> userList = exampleQueryRespository.findAll(userExample, orders);
        Assert.assertNotNull(userList);
    }

    @Test
    public void testExampleLikeQueryAndExcludeProperty() {

        User user = new User();
        user.setName("en");
        user.setEmail("88");
        user.setSex("男");
        user.setId("忽略此属性的值作为查询条件");
        ExampleMatcher matcher = ExampleMatcher.matching()
                //默认采用 like %xxx%
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                //忽略大小写
                .withIgnoreCase(true)
                //指定email属性为 like %xxx
                .withMatcher("email", ExampleMatcher.GenericPropertyMatchers.startsWith())
                //指定sex属性为 like xxx%
                .withMatcher("sex", ExampleMatcher.GenericPropertyMatchers.endsWith())
                //设置忽略某些属性不为空的查询条件
                .withIgnorePaths("id");
        Example<User> userExample = Example.of(user, matcher);
        List<User> userList = exampleQueryRespository.findAll(userExample);
        Assert.assertNotNull(userList);
    }

    /**
     * 当查询页号为 0 ，第一页的时候不发送 count 语句 ?
     */
    @Test
    public void testExampleQueryPage() {
        User user = new User();
        user.setName("en");
        Example<User> userExample = Example.of(user, ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING));
        //分页页号从 0 开始
        Pageable pageable = PageRequest.of(0, 2,
                Sort.by(Sort.Direction.ASC, "name", "email"));
        Page<User> userPage = exampleQueryRespository.findAll(userExample, pageable);
        Assert.assertTrue(userPage.getTotalPages() >= 0);
    }

}
