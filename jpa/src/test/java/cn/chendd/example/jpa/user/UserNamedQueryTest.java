package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.entity.User;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * UserTest
 *
 * @author chendd
 * @date 2020/4/29 21:12
 */
@SuppressWarnings("unchecked")
public class UserNamedQueryTest extends ApplicationTest {

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testBasicNamedQuery() {
        List<User> resultList = entityManager
                .createNamedQuery("User.query_by_name_email").getResultList();
        Assert.assertNotNull(resultList);
    }

    @Test
    public void testNamedQueryByParameter() {
        List<User> resultList = entityManager.createNamedQuery("User.query_by_name_custom")
                .setParameter(1 , "%en%").getResultList();
        Assert.assertNotNull(resultList);
    }

    @Test
    public void testNamedQueryByCustomDto() {
        List resultList = entityManager.createNamedQuery("User.query_id_userName_mapping")
                .getResultList();
        System.out.println(resultList);
        Assert.assertNotNull(resultList);
    }

    @Test
    public void testNamedQueryBySqlFromXml() {

        List<User> resultList = entityManager
                .createNamedQuery("XML_User.query_by_name_email").getResultList();
        Assert.assertNotNull(resultList);
    }

}
