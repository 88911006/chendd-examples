package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.entity.User;
import cn.chendd.example.jpa.user.repository.UserSimpleQueryRepository;
import cn.chendd.example.jpa.user.service.UserService;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * UserTest
 *
 * @author chendd
 * @date 2020/4/21 20:14
 */
public class UserSimpleQueryTest extends ApplicationTest {

    @Resource
    private UserSimpleQueryRepository userSimpleQueryRepository;

    @Test
    public void testQueryUserByNameAndEmail() {
        List<User> userList = this.userSimpleQueryRepository
                .queryUsersByNameAndEmail("chendd", "88911006@qq.com");
        Assert.assertNotNull(userList);
    }

    @Test
    public void testQueryUsersByMatch() {
        Date begin = new Date();
        Date end = new Date();
        List<User> userList = this.userSimpleQueryRepository
                .queryUsersByCreateTimeBetweenAndNameLike(begin , end  , "%en%");
        Assert.assertNotNull(userList);
    }

}
