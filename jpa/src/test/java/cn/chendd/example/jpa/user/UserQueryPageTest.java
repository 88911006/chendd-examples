package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.entity.User;
import cn.chendd.example.jpa.user.repository.UserQueryPageRepository;
import com.alibaba.fastjson.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.annotation.Resource;
import java.util.List;

/**
 * 查询分页
 *
 * @author chendd
 * @date 2020/5/2 21:28
 */
public class UserQueryPageTest extends ApplicationTest {

    @Resource
    private UserQueryPageRepository repository;

    @Test
    public void testQueryPage() {
        Pageable pageable = PageRequest.of(0 , 5 , Sort.by(Sort.Order.asc("name")));
        Page<User> page = repository.queryUsersPage(pageable);
        List<User> userList = page.getContent();
        Assert.assertNotNull(userList);
    }

}
