package cn.chendd.example.jpa.main;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * ApplicationTest
 *
 * @author chendd
 * @date 2020/4/21 20:13
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class , webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

}
