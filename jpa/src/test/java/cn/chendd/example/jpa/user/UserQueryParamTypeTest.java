package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.entity.User;
import cn.chendd.example.jpa.user.repository.UserQueryParamTypeRepository;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Query查询参数传值方式
 *
 * @author chendd
 * @date 2020/4/30 23:25
 */
public class UserQueryParamTypeTest extends ApplicationTest {

    @Resource
    private UserQueryParamTypeRepository repository;

    @Test
    public void testQueryByNameAndEmailForOneType() {
        List<User> dataList = repository.queryUsersByNameAndEmailForOneType("chendd", "88");
        Assert.assertNotNull(dataList);
    }

    @Test
    public void testQueryByNameAndEmailForTwoType() {
        List<User> dataList = repository.queryUsersByNameAndEmailForTwoType("chendd" , "88");
        Assert.assertNotNull(dataList);
    }

    @Test
    public void testQueryByNameAndEmailForThreeType() {
        String nameArray[] = {"chendd" , "cdd"};
        List<String> nameList = Lists.newArrayList(nameArray);
        List<User> dataList = repository.queryUsersByNameAndEmailForThreeType(nameList , nameArray , "88");
        System.out.println(dataList);
        Assert.assertNotNull(dataList);
    }

}
