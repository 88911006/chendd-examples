package cn.chendd.example.jpa.user;

import cn.chendd.example.jpa.main.Application;
import cn.chendd.example.jpa.main.ApplicationTest;
import cn.chendd.example.jpa.user.entity.User;
import cn.chendd.example.jpa.user.repository.UserRepository;
import cn.chendd.example.jpa.user.service.UserService;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.Predicate;

/**
 * UserTest
 *
 * @author chendd
 * @date 2020/4/21 20:14
 */
public class UserBasicTest extends ApplicationTest {

    @Resource
    private UserService userService;
    @Resource
    private UserRepository userRepository;

    @Test
    public void testUserInsert() {
        User user = new User();
        user.setName("chendd");
        user.setSex("男");
        user.setEmail("88911006@qq.com");
        user.setCreateTime(new Date());
        User dbUser = userService.saveUser(user);
        Assert.assertEquals(dbUser.getName() , user.getName());
    }

    @Test
    public void testUserAll(){
        List<User> userList = userService.findAll();
        Assert.assertNotNull(userList);
    }

    @Test
    public void testUserPage() {
        User param = new User();
        Specification<User> specification = (root , criteriaQuery , criteriaBuilder) -> {
            List<Predicate> predicateList = Lists.newArrayList();
            //between and查询
            predicateList.add(criteriaBuilder.between(root.get("createTime") , param.getCreateTime() , param.getCreateTime()));
            //in查询
            List<String> statusList = param.getStatusList();
            if (statusList != null) {
                predicateList.add(criteriaBuilder.and(root.get("status").in(statusList)));
            }
            String name = param.getName();
            if (StringUtils.isNotBlank(name)) {
                //like查询
                predicateList.add(criteriaBuilder.like(root.get("email") , "%" + name + "%"));
                //not like查询
                predicateList.add(criteriaBuilder.notLike(root.get("email") , "%" + name + "%"));
            }
            String id = param.getId();
            if (StringUtils.isNotBlank(id)) {
                //等于、不等于查询
                predicateList.add(criteriaBuilder.equal(root.get("id") , id));
                predicateList.add(criteriaBuilder.notEqual(root.get("id") , id));
            }
            //小于等于 predicateList.add(criteriaBuilder.lessThanOrEqualTo());
            //大于等于 predicateList.add(criteriaBuilder.greaterThanOrEqualTo());
            //and (name = 'abc' or email = 'abc')
            predicateList.add(criteriaBuilder.and(
                    criteriaBuilder.or(criteriaBuilder.equal(root.get("name") , name)) ,
                    criteriaBuilder.or(criteriaBuilder.equal(root.get("email") , name))
                )
            );
            Predicate[] predicates = new Predicate[predicateList.size()];
            return criteriaQuery.where(predicateList.toArray(predicates)).getRestriction();
        };
        Pageable pageable = PageRequest.of(1 , 10 , Sort.by(Sort.Order.asc("name") , Sort.Order.desc("email")));
        this.userRepository.findAll(specification , pageable);
    }

    @Test
    public void test() {
        System.out.println(this.userRepository);
        List<User> userList = this.userRepository.queryAll();
        System.out.println(userList);
    }

}
