package cn.chendd.h2console;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author chendd
 * @date 2023/3/25 19:08
 */
@SpringBootApplication
public class H2ConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(H2ConsoleApplication.class , args);
    }

}
