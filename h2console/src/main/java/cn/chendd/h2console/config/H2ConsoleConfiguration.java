package cn.chendd.h2console.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.h2.server.web.WebServlet;


/**
 * H2Console 配置类
 *
 * @author chendd
 * @date 2023/3/25 19:09
 */
@Configuration
public class H2ConsoleConfiguration {

    @Bean
    public ServletRegistrationBean<WebServlet> h2ConsoleServlet() {
        ServletRegistrationBean<WebServlet> servletRegistration = new ServletRegistrationBean<>(new WebServlet());
        servletRegistration.addUrlMappings("/h2-console/*");
        servletRegistration.addInitParameter("-webAdminPassword" , "www.chendd.cn");
        return servletRegistration;
    }

}
