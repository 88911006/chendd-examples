import com.amdelamar.jotp.OTP;
import com.amdelamar.jotp.type.Type;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * 验证 Github 2FA，参考地址：https://blog.csdn.net/dejavu_980323/article/details/132318107
 * @author chendd
 * @date 2023/11/17 17:11
 */
public class Validator {

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        // Generate a Time-based OTP from the secret, using Unix-time
        // rounded down to the nearest 30 seconds.
        String hexTime = OTP.timeInHex(System.currentTimeMillis(), 30);
        String code = OTP.create("XMCQKKBCI63IMF2B", hexTime, 6, Type.TOTP);
        System.out.println(code);
    }

}
