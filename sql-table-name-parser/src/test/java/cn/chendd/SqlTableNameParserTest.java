package cn.chendd;

import com.github.mnadeem.TableNameParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collection;

/**
 * SQL 表名称解析器 测试实践
 *
 * @author chendd
 * @date 2023/7/28 23:21
 */
@RunWith(JUnit4.class)
public class SqlTableNameParserTest {

    /**
     * 1、简单SQL，带查询条件，得到表名称：[User]
     */
    @Test
    public void testA1() {
        String sql = "select * from User where 1=1 and id = '123' and id like '%abc%'";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 2、简单SQL，带查询条件、带schema，得到表名称：[schema.User]
     */
    @Test
    public void testA2() {
        String sql = "select * from schema.User where 1=1 and id = '123' and id like '%abc%'";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 3、简单SQL，带查询条件、带schema、带数据库名称，得到表名称：[database.dbo.User]
     */
    @Test
    public void testA3() {
        String sql = "select * from database.dbo.User where 1=1 and id = '123' and id like '%abc%'";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 4、简单SQL，带查询条件、中文名称、带别名，得到表名称：[User中文表名称]
     */
    @Test
    public void testA4() {
        String sql = "select * from User中文表名称 t1 where 1=1 and id = '123' and id like '%abc%'";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 5、查询多个表，带查询条件、带别名，得到表名称：[Role, User, User_Role]
     */
    @Test
    public void testC1() {
        String sql = "select t1.*, t2.* , t3.* from User t1 , User_Role t2 , Role t3 where t1.userId = t2.userId " +
                "and t2.roleId = t3.roleId and id = ? and t1.id = :id";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 6、交叉连接，带查询条件、带别名，得到表名称：[Role, User, User_Role]
     */
    @Test
    public void testC2() {
        String sql = "select t1.*, t2.* , t3.* from User t1 " +
                "left join User_Role t2 on t1.userId = t2.userId " +
                "right join Role t3 on t2.roleId = t3.roleId " +
                "where t1.userId = t2.userId and  and id = ? and t1.id = :id";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 7、插入查询语句，带交叉连接，带查询条件、带别名，得到表名称：[Role, User, user_temp, User_Role]
     */
    @Test
    public void testC3() {
        String sql = "inser into user_temp as select t1.*, t2.* , t3.* from User t1 " +
                "left join User_Role t2 on t1.userId = t2.userId " +
                "right join Role t3 on t2.roleId = t3.roleId " +
                "where t1.userId = t2.userId and  and id = ? and t1.id = :id";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 8、子查询：[Role, User, User_Role]
     */
    @Test
    public void testC4() {
        String sql = "select t1.*, t2.* , (select max(amount) from Role t3 where t2.roleId = t3.roleId) from User t1 " +
                "left join User_Role t2 on t1.userId = t2.userId " +
                "where t1.userId = t2.userId and  and id = ? and t1.id = :id";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 9、合并查询：[User2, User1, Usert, User3]
     */
    @Test
    public void testC5() {
        String sql = "select t1.* from User1 t1 where t1.id = '1' " +
                "union " +
                "select t2.* from User2 t2 where t2.id = '2' " +
                "union all " +
                "select t3.* from User3 t3 where t3.id = '3' " +
                "minus " +
                "select tt.* from Usert tt where tt.id = 'tt'";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 10、Oracle merge into 合并更新：[User, Usert]
     */
    @Test
    public void testC6() {
        String sql = "MERGE INTO User u " +
                "USING (select * FROM Usert t where t.date >= :date) data" +
                "ON data.name = u.name " +
                "WHEN MATCHED THEN " +
                "UPDATE SET col_1 = value_1, col_2 = value_2 " +
                "WHERE NOT MATCHED THEN " +
                "INSERT (col_1,col_2) " +
                "Values(value_1,value_2) ";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

    /**
     * 11、MySQL 数据库带特殊字符 查询：[`chendd-blog`.sys_operationlog_module]
     */
    @Test
    public void testC7() {
        String sql = "SELECT * FROM `chendd-blog`.sys_operationlog_module ";
        System.out.println(sql);
        Collection<String> tables = new TableNameParser(sql).tables();
        System.out.println(tables);
    }

}
