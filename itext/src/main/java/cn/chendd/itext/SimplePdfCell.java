package cn.chendd.itext;

import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;

/**
 * PdfCell包装类
 *
 * @author chendd
 * @date 2022/7/6 22:21
 */
public class SimplePdfCell extends PdfPCell {

    public SimplePdfCell(String text, Font font, float height) {
        super(new Paragraph(text, font));
        this.setMinimumHeight(height);
    }

    public SimplePdfCell(Paragraph element, Font font, float height) {
        super(element);
        this.setMinimumHeight(height);
    }

    public PdfPCell getCenterCell() {
        this.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        this.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        return this;
    }

    public PdfPCell getLeftCell() {
        this.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        this.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        return this;
    }

    public PdfPCell getRightcell() {
        this.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        this.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        return this;
    }
}
