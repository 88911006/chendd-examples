package cn.chendd.session.listeners;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class SessionAttributeListener implements HttpSessionAttributeListener {

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        System.out.println("SessionAttributeListener.attributeAdded-->" + event.getName() + "," + event.getValue());
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        System.out.println("SessionAttributeListener.attributeRemoved-->" + event.getName() + "," + event.getValue());
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        System.out.println("SessionAttributeListener.attributeReplaced-->" + event.getName() + "," + event.getValue());
    }
}
