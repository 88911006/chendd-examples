package cn.chendd.session.listeners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class SessionBindListener implements HttpSessionBindingListener {

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        String text = String.format("session valueBound：name=%s, value=$s", event.getName() , event.getValue());
        System.out.println(text);
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        String text = String.format("session valueUnbound：name=%s, value=$s", event.getName() , event.getValue());
        System.out.println(text);
    }
}
